import zqdb.container as zqdb
import requests

class MyContainer(zqdb.Container):
    """"""

    # 这里是默认设置，其中input是用户可以通过设置界面修改的入参
    default_setting = {
        'type':'',
        'name':'险资举牌',
        'desc':'我的板块。',
        'input':{
            'id':0
        },
        'menu': [
            {
                'name':'创新高',
                'input':{
                    'id':1
                }
            },
            {
                'name':'创新低',
                'input':{
                    'id':2
                }
            },
            {
                'name':''
            },
            {
                'name':'连续上涨',
                'input':{
                    'id':3
                }
            },
            {
                'name':'连续下跌',
                'input':{
                    'id':4
                }
            },
            {
                'name':''
            },
            {
                'name':'持续放量',
                'input':{
                    'id':5
                }
            },
            {
                'name':'持续缩量',
                'input':{
                    'id':6
                }
            },
            {
                'name':''
            },
            {
                'name':'向上突破',
                'input':{
                    'id':7
                }
            },
            {
                'name':'向下突破',
                'input':{
                    'id':8
                }
            },
            {
                'name':''
            },
            {
                'name':'量价齐升',
                'input':{
                    'id':9
                }
            },
            {
                'name':'量价齐跌',
                'input':{
                    'id':10
                }
            }
        ]
    }

    # 构造函数，外部需要传入setting设置，用户可以在构造函数里关联计算数据/指标等
    def __init__(
        self,
        setting: dict
    ):
        """Constructor"""
        super().__init__(setting)
        input = setting["input"]
        self.id = input["id"]

    # 计算函数，当关联的计算数据/指标等更新时就会被调用
    def on_calc(self):
        """"""
        # self.add_result 添加筛选结果
        url = None
        if(self.id == 1):
            url = "http://127.0.0.1:10809/api/public/stock_rank_cxg_ths"
            pass
        elif(self.id == 2):
            url = "http://127.0.0.1:10809/api/public/stock_rank_cxd_ths"
            pass
        elif(self.id == 3):
            url = "http://127.0.0.1:10809/api/public/stock_rank_lxsz_ths"
            pass
        elif(self.id == 4):
            url = "http://127.0.0.1:10809/api/public/stock_rank_lxxd_ths"
            pass
        elif(self.id == 5):
            url = "http://127.0.0.1:10809/api/public/stock_rank_cxfl_ths"
            pass
        elif(self.id == 6):
            url = "http://127.0.0.1:10809/api/public/stock_rank_cxsl_ths"
            pass
        elif(self.id == 7):
            url = "http://127.0.0.1:10809/api/public/stock_rank_xstp_ths"
            pass
        elif(self.id == 8):
            url = "http://127.0.0.1:10809/api/public/stock_rank_xxtp_ths"
            pass
        elif(self.id == 9):
            url = "http://127.0.0.1:10809/api/public/stock_rank_ljqs_ths"
            pass
        elif(self.id == 10):
            url = "http://127.0.0.1:10809/api/public/stock_rank_ljqd_ths"
            pass
        else:
            url = "http://127.0.0.1:10809/api/public/stock_rank_xzjp_ths"

        if(url is None):
            return

        res = requests.get(url)
        res.encoding = "utf-8"
        arr = res.json()
        for row in arr:
            szse_code = zqdb.Code(row['股票代码'], 'SZSE')
            if(szse_code):
                self.add_result(szse_code)
                continue
                
            sse_code = zqdb.Code(row['股票代码'], 'SSE')
            if(sse_code):
                self.add_result(sse_code)
                continue
        pass

