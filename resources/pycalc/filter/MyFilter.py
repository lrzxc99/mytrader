import zqdb.filter as zqdb

class MyFilter(zqdb.Filter):
    """"""

    # 这里是默认设置，其中input是用户可以通过设置界面修改的入参
    default_setting = {
        'type':'',
        'name':'MACD金叉',
        'desc':'我的筛选过滤器。',
        'input':{
            'id':1
        },
        'menu': [
            {
                'name':'KDJ超跌',
                'input':{
                    'id':2
                }
            },
            {
                'name':'MA年线金叉',
                'input':{
                    'id':3
                }
            },
            {
                'name':'放量',
                'input':{
                    'id':4
                }
            }
        ]
    }

    # 构造函数，外部需要传入setting设置，用户可以在构造函数里关联计算数据/指标等
    def __init__(
        self,
        setting: dict
    ):
        """Constructor"""
        super().__init__(setting)
        input = setting["input"]
        self.id = input["id"]

    # 计算函数，当关联的计算数据/指标等更新时就会被调用
    def on_calc(self, code):
        """"""
        # 返回权重值 0表示过滤掉，1-100表示权重值，越大权重越高
        product = zqdb.Product(code)
        if(product.Type != zqdb.PRODUCT_TYPE.STOCK):
            return 0
        stock = zqdb.StkCode(code)
        if(stock.PE_TTM < 0 or stock.PE_TTM > 100):
            return 0
        # if(stock.MarketValue > 10000000000):
        #     return 0
        if(self.id == 1):
            dif = zqdb.MACDBuffer(zqdb.DATA.CLOSE, 12, 26, 9, zqdb.MACDType.DIF, code, zqdb.PERIOD.DAY)
            dea = zqdb.MACDBuffer(zqdb.DATA.CLOSE, 12, 26, 9, zqdb.MACDType.DEA, code, zqdb.PERIOD.DAY)
            cross = zqdb.CROSS(dif, dea)
            if cross > 0:
                return 1
            return 0
        elif(self.id == 2):
            k = zqdb.KDJBuffer(9, 3, 3, zqdb.KDJType.K, code, zqdb.PERIOD.DAY)
            v = float(k)
            if(v < 10):
                return int(-v)
            return 0
        elif(self.id == 3):
            ma5 = zqdb.MABuffer(zqdb.DATA.CLOSE, 5, zqdb.MAType.SMA, code, zqdb.PERIOD.DAY)
            ma240 = zqdb.MABuffer(zqdb.DATA.CLOSE, 240, zqdb.MAType.SMA, code, zqdb.PERIOD.DAY)
            cross = zqdb.CROSS(ma5, ma240,5)
            if cross > 0:
                return int(5 - cross)
            return 0
        elif(self.id == 4):
            data = zqdb.Data(code, zqdb.PERIOD.DAY)
            volume = zqdb.DataBuffer(data, zqdb.DATA.VOLUME)
            n = len(volume)
            if(n > 1):
                if(volume[n - 2] * 3 < volume[n - 1]):
                    return int(volume[n - 1] / volume[n - 2])
            return 0
        else:
            pass
        return 0

