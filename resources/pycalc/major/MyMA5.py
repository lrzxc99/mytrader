import zqdb.indicator as zqdb

class MyMA5(zqdb.MajorIndicator):
    """"""

    # 这里是默认设置，其中input是用户可以通过设置界面修改的入参，result是结果数组，result数组数量代表数组列表数量
    default_setting = {
        'type':'',#趋势指标、超买超卖指标、能量指标、成交量指标、量价指标、压力支撑指标、摆动指标、反趋势指标、其他指标
        'name':'',
        'desc':'MA5彩均线',
        'input':{
        },
        'result':[
            {'name':'MA5', 'type':zqdb.RESULT_TYPE.LINEREF},
            {'name':'MA20', 'type':zqdb.RESULT_TYPE.LINEREF},
            {'name':'MA60', 'type':zqdb.RESULT_TYPE.LINEREF},
            {'name':'MA120', 'type':zqdb.RESULT_TYPE.LINEREF},
            {'name':'MA240', 'type':zqdb.RESULT_TYPE.LINEREF}
        ]
    }

    # 构造函数，外部需要传入setting设置，用户可以在构造函数里关联计算数据/指标等
    def __init__(
        self,
        setting: dict
    ):
        """Constructor"""
        super().__init__(setting)
        self.MA5 = zqdb.ResultBuffer(0,zqdb.MABuffer(zqdb.DATA.CLOSE, 5))  # 引用CLOSE价格的MA指标数组
        self.MA20 = zqdb.ResultBuffer(1,zqdb.MABuffer(zqdb.DATA.CLOSE, 20))  # 引用CLOSE价格的MA指标数组
        self.MA60 = zqdb.ResultBuffer(2,zqdb.MABuffer(zqdb.DATA.CLOSE, 60))  # 引用CLOSE价格的MA指标数组
        self.MA120 = zqdb.ResultBuffer(3,zqdb.MABuffer(zqdb.DATA.CLOSE, 120))  # 引用CLOSE价格的MA指标数组
        self.MA240 = zqdb.ResultBuffer(4,zqdb.MABuffer(zqdb.DATA.CLOSE, 240))  # 引用CLOSE价格的MA指标数组

    # 计算函数，当关联的计算数据/指标等更新时就会被调用
    def on_calc(self):
        """"""
        # 指标计算请在这里编写...
        # self.count CLOSE/LINE的长度
        # self.counted CLOSE/LINE已经计算了多少，这样可以增量计算
        pass

