import zqdb.script as zqdb

class OrderCancel(zqdb.Script):
    """"""

    # 这里是默认设置，其中input是用户可以通过设置界面修改的入参
    default_setting = {
        'type':'',
        'name':'快撤',
        'desc':'快速撤单。',
        'input':{
        }
    }

    # 构造函数，外部需要传入setting设置，用户可以在构造函数里关联计算数据/指标等
    def __init__(
        self,
        setting: dict
    ):
        """Constructor"""
        super().__init__(setting)

    # 计算函数，当关联的计算数据/指标等更新时就会被调用，脚本可以调用交易相关函数
    def on_calc(self, code):
        """"""
        # 脚本算法请在这里编写...
        user = self.user
        if not user:
            zqdb.LogInfo("快速撤单失败，交易账号为空")
            return

        cancel = False
        n = 0
        m = user.order_count
        while(n < m):
            order = zqdb.Order(user, n)
            if(order.Status not in (zqdb.ORDER_STATUS.ALLTRADED, zqdb.ORDER_STATUS.CANCELLED, zqdb.ORDER_STATUS.REJECTED)):
                if(code.TradeCode == order.Code and code.Exchange == order.Exchange):
                    cancel = True
                    r = self.order_cancel(order)
                    zqdb.LogInfo("快速撤单成功" if r == 0 else "快速撤单失败", r, "委托编号=", order.Order, "代码=", order.Code, " 类型=", order.Type, " 状态=", order.Status, " 价格=", order.Price, " 量=", order.Volume)

            n = n + 1
        
        if not cancel:
            zqdb.LogInfo("没有可撤委托")
        
        pass

