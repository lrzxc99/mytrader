import zqdb.sort as zqdb

class MySort(zqdb.Sort):
    """"""

    # 这里是默认设置，其中input是用户可以通过设置界面修改的入参
    default_setting = {
        'type':'',
        'name':'代码',
        'desc':'我的代码排序',
        'trigger':zqdb.TRIGGER_TYPE.NONE,
        'input':{
        }
    }

    # 构造函数，外部需要传入setting设置，用户可以在构造函数里关联计算数据/指标等
    def __init__(
        self,
        setting: dict
    ):
        """Constructor"""
        super().__init__(setting)
        input = setting["input"]

    # 计算函数，当关联的计算数据/指标等更新时就会被调用
    def on_calc(self, x, y):
        """"""
        # print(x.Code, y.Code)
        return x.Code < y.Code

