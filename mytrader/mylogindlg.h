// aboutdlg.h : interface of the CMySmartKBDlg class
//
/////////////////////////////////////////////////////////////////////////////

#ifndef _H_LOGINDLG_H_
#define _H_LOGINDLG_H_

#include "myapp.h"
#include "mymodule.h"

class MyLoginDlg 
	: public wxDialog
	, public zqdb::SkinMap<MyLoginDlg, SkinInfo>
	, public zqdb::NotifyMap<MyLoginDlg>
	//, public zqdb::MsgMap<MyLoginDlg>
{
	typedef wxDialog Base;
	typedef zqdb::SkinMap<MyLoginDlg, SkinInfo> SkinBase;
	typedef zqdb::NotifyMap<MyLoginDlg> NotifyBase;
	//typedef zqdb::MsgMap<MyLoginDlg> MsgBase;
private:
	static MyLoginDlg* s_dlg;
public:
	static MyLoginDlg* Inst() { return s_dlg; }
private:
	wxStaticBitmap* stc_bmp_ = nullptr;
	wxStaticText* stc_type_ = nullptr;
	wxComboBox *cmb_type_ = nullptr;
	std::shared_ptr<MyModule> module_;
	wxStaticText* stc_view_ = nullptr;
	MyLoginView* view_ = nullptr;
	const char* user_ = nullptr;
	wxButton* btn_ok_ = nullptr;
	wxButton* btn_ignore_ = nullptr;
	wxButton* btn_cancel_ = nullptr;
	wxStaticText* stc_tips_ = nullptr;
public:
	MyLoginDlg(wxWindow *parent = nullptr);
	virtual ~MyLoginDlg();
	
	void OnSkinInfoChanged();

	/*void OnNotifyAdd(HZQDB h);
	void OnNotifyUpdate(HZQDB h);*/

protected:
	//
	void UpdateType();
	void OnCmbTypeUpdate(wxCommandEvent& event);

	//void DealUserLogin(HZQDB h);
	//void OnNotifyEvent(wxCommandEvent& event);

	void OnOK(wxCommandEvent& event);
	void OnIgnore(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

#endif//_H_LOGINDLG_H_
