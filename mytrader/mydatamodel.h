#pragma once

#include "myapp.h"
#include "smartkb.h"

#include "wx/dataview.h"
#include "wx/hashmap.h"
#include <wx/vector.h>

///

class SmartKBManager : public SmartKB
{
protected:
	//通知搜索结束
	virtual void NotifyResult(void* data);
public:
	static SmartKBManager& Inst() {
		static SmartKBManager _inst;
		return _inst;
	}

	SmartKBManager();

	void Start();

	void ClearAll();
	void UpdateAll();

	//通知搜索结束
	void NotifyDirect(wxEvtHandler* notify);

	//搜索
	bool Search(wxEvtHandler* notify, const wchar_t* lpszKey, int flag = 0);
};

// ----------------------------------------------------------------------------
// SmartKBListModel
// ----------------------------------------------------------------------------

wxDECLARE_EVENT(SMARTKB_SEARCH_RESULT_EVENT, wxCommandEvent);

class SmartKBListModel : public wxDataViewVirtualListModel
{
private:
	std::vector<SmartKBItem> results_;
public:
	enum
	{
		Col_Code,
		Col_Name,
		Col_Desc,
		Col_Max
	};

	SmartKBListModel();

	//搜索
	bool Search(wxEvtHandler* notify, const wxString& strKey, int flag = 0);
	//显示结果
	virtual void ShowResult();
	//获取数据
	bool GetResult(const wxDataViewItem& item, SmartKBItem& smkbi);

	// implementation of base class virtuals to define model

	virtual unsigned int GetColumnCount() const wxOVERRIDE
	{
		return Col_Max;
	}

	virtual wxString GetColumnType(unsigned int col) const wxOVERRIDE
	{
		/*if (col == Col_Toggle)
			return "bool";

		if (col == Col_IconText)
			return "wxDataViewIconText";*/

		return "string";
	}

	virtual void GetValueByRow(wxVariant &variant,
		unsigned int row, unsigned int col) const wxOVERRIDE;
	virtual bool GetAttrByRow(unsigned int row, unsigned int col,
		wxDataViewItemAttr &attr) const wxOVERRIDE;
	virtual bool SetValueByRow(const wxVariant &variant,
		unsigned int row, unsigned int col) wxOVERRIDE;
};

///
//可以显示代码行情信息
///

enum MY_CODE_SORT_TYPE {
	SORT_ZDF,
	SORT_ZDS,
	SORT_FIELD,
	SORT_CALC_SORT,
};

struct SmartKBItemSort
{
public:
	MY_CODE_SORT_TYPE type = SORT_ZDF;
	int sort = 0; //-1升序，0不排序，1降序
	union {
		size_t secs; //ZDS排序时间，比如1分钟排序
		MDB_FIELD field = { 0 }; //Normalize过的field
	};
private:
	zqdb::Calc::Sort calc_sort; //
public:
	SmartKBItemSort();
	SmartKBItemSort(MY_CODE_SORT_TYPE type, size_t duration, int sort);
	SmartKBItemSort(const MDB_FIELD& field, int sort);
	SmartKBItemSort(const zqdb::Calc::Sort& calc, int sort);

	bool IsDynamic();

	bool operator() (const SmartKBItem& x, const SmartKBItem& y) const;
};

class MyCodeViewListModel : public wxDataViewVirtualListModel
{
private:
	wxString key_; //关键字
	zqdb::Calc::Container container_; //当前容器
	std::vector<SmartKBItem> results_; //结果集
	SmartKBItemSort sort_; //排序函数
public:
	enum
	{
		Col_Code,
		Col_Max
	};

	MyCodeViewListModel();

	//返回当前列表，origin表示是否返回原始列表，因为当前可能显示的是筛选
	zqdb::Calc::Container Cur(bool origin = false);
	//是否显示所有
	bool IsShowAll();
	//是否显示容器
	bool IsShowContainer();
	//是否显示Key
	bool IsShowKey(const wxString& strKey);
	//搜索
	bool Search(wxEvtHandler* notify, const wxString& strKey, int flag = 0);
	void Search(wxEvtHandler* notify, const zqdb::Calc::Container& container);
	//显示结果
	void InnerUpdateOneResult(SmartKBItem& result, bool first);
	void InnerUpdateResult(bool first);
	void UpdateResult(bool first = false);
	//排序
	int IsSort(MY_CODE_SORT_TYPE* type = nullptr, size_t* secs = nullptr);
	void Sort(bool force = false);
	void SortByZD(MY_CODE_SORT_TYPE type, size_t secs, int sort);
	void SortByField(MDB_FIELD& field, int sort);
	void SortByCalc(const zqdb::Calc::Sort& calc, int sort);
	//获取数据
	bool GetResult(const wxDataViewItem& item, SmartKBItem& smkbi);
	bool GetResult(const size_t row, SmartKBItem& smkbi);
	//查找数据,返回数据位置，-1表示没有找到
	int FindResult(HZQDB h);
	//向结构列表添加数据
	void AddResult(HZQDB h);
	//从结果列表删除数据
	int RemoveResult(const wxDataViewItem& item);
	int RemoveResult(const size_t row);

	// implementation of base class virtuals to define model

	virtual unsigned int GetColumnCount() const wxOVERRIDE
	{
		return Col_Max;
	}

	virtual wxString GetColumnType(unsigned int col) const wxOVERRIDE
	{
		return "void*";
	}

	virtual void GetValueByRow(wxVariant &variant,
		unsigned int row, unsigned int col) const wxOVERRIDE;
	virtual bool GetAttrByRow(unsigned int row, unsigned int col,
		wxDataViewItemAttr &attr) const wxOVERRIDE;
	virtual bool SetValueByRow(const wxVariant &variant,
		unsigned int row, unsigned int col) wxOVERRIDE;
};

class MyCodeListModel : public wxDataViewVirtualListModel
{
public:
	enum
	{
		Col_Code,
		Col_Name,
		Col_Desc,
		Col_Max
	};
private:
	std::vector<HZQDB> code_items_;
public:
	MyCodeListModel();

	void Show(const std::vector<HZQDB>& h_list);
	HZQDB GetData(const wxDataViewItem& item);

	// implementation of base class virtuals to define model

	virtual unsigned int GetColumnCount() const wxOVERRIDE
	{
		return Col_Max;
	}

	virtual wxString GetColumnType(unsigned int col) const wxOVERRIDE
	{
		if (col == Col_Code)
		return "void*";

		if (col == Col_Name)
		return "void*";

		return "string";
	}

	virtual void GetValueByRow(wxVariant &variant,
		unsigned int row, unsigned int col) const wxOVERRIDE;
	virtual bool GetAttrByRow(unsigned int row, unsigned int col,
		wxDataViewItemAttr &attr) const wxOVERRIDE;
	virtual bool SetValueByRow(const wxVariant &variant,
		unsigned int row, unsigned int col) wxOVERRIDE;
};
//
//class MyHZQDBListModel : public zqdb::HZQDBListModel
//{
//	typedef zqdb::HZQDBListModel Base;
//private:
//	std::map<std::string, std::vector<ColInfo>> all_col_items_;
//public:
//	MyHZQDBListModel(const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
//
//	void Select(HZQDB user, ZQDB_HANDLE_TYPE type);
//};


// ----------------------------------------------------------------------------
// MyCatModelNode: a node inside MyCatModel
// ----------------------------------------------------------------------------

class MyCatModelNode;
WX_DEFINE_ARRAY_PTR(MyCatModelNode*, MyCatModelNodePtrArray);

class MyCatModelNode
{
public:
	MyCatModelNode(const wxString &title, char* type = nullptr, int count = 0);
	MyCatModelNode(MyCatModelNode* parent, HZQDB h, char* type = nullptr, int count = 0);
	~MyCatModelNode();

	bool IsContainer() const {	return h_ == nullptr || h_->type == ZQDB_HANDLE_TYPE_EXCHANGE; }
	MyCatModelNode* GetParent() { return parent_; }
	MyCatModelNodePtrArray& GetChildren() {	return childs_; }
	size_t GetChildCount() const { return childs_.GetCount(); }

public:
	HZQDB h_ = nullptr;
	wxString title_;
	wxString info_;
	int state_ = 0;
	int progress_ = 0;
private:
	MyCatModelNode *parent_;
	MyCatModelNodePtrArray childs_;
};

class MyCatModel : public wxDataViewModel
{
public:
	MyCatModel();
	~MyCatModel();

	wxDataViewItem AddRoot(const wxString& title, char* type, int count);
	void RemoveRoot(const wxString& title);
	wxDataViewItemArray GetRoots() const;

	bool EnableState(bool enable = true);
	void UpdateProgress(const wxDataViewItem &item, int value);
	void UpdateInfo(const wxDataViewItem &item, const wxString& value);

	HZQDB GetHandle(const wxDataViewItem &item) const;
	wxString GetTitle(const wxDataViewItem &item) const;
	wxString GetInfo(const wxDataViewItem &item) const;
	int GetState(const wxDataViewItem &item) const;
	int GetProgress(const wxDataViewItem &item) const;

	// override sorting to always sort branches ascendingly

	int Compare(const wxDataViewItem &item1, const wxDataViewItem &item2,
		unsigned int column, bool ascending) const wxOVERRIDE;

	// implementation of base class virtuals to define model

	virtual unsigned int GetColumnCount() const wxOVERRIDE
	{
		return 4;
	}

	virtual wxString GetColumnType(unsigned int col) const wxOVERRIDE
	{
		return "string";
	}

	virtual void GetValue(wxVariant &variant,
		const wxDataViewItem &item, unsigned int col) const wxOVERRIDE;
	virtual bool SetValue(const wxVariant &variant,
		const wxDataViewItem &item, unsigned int col) wxOVERRIDE;

	virtual bool IsEnabled(const wxDataViewItem &item,
		unsigned int col) const wxOVERRIDE;

	virtual wxDataViewItem GetParent(const wxDataViewItem &item) const wxOVERRIDE;
	virtual bool IsContainer(const wxDataViewItem &item) const wxOVERRIDE;
	virtual unsigned int GetChildren(const wxDataViewItem &parent,
		wxDataViewItemArray &array) const wxOVERRIDE;

private:
	MyCatModelNodePtrArray roots_;
	bool enable_state_ = true;
};

