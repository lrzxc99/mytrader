#include "mydataview.h"
#include "mytechframe.h"

///

void SetHeaderCustomDraw(wxHeaderCtrl *header, std::function<void(wxDC &dc, const wxRect& rect)> cb)
{
	auto childs = header->GetChildren();
	auto child = childs.empty() ? header : childs[0];
	auto func = [header, child, cb](wxPaintEvent &event) {
		wxPaintDC dc(child);
		dc.Clear();
		dc.SetFont(header->GetFont());
		dc.SetTextForeground(header->GetForegroundColour());
		auto rcClient = header->GetClientRect();
		cb(dc, rcClient);
	};
	if (cb) {
		child->Bind(wxEVT_PAINT, func);
	}
	else {
		child->Unbind(wxEVT_PAINT, func);
	}
}

///

void MyScrollBar::SetScrollTarget(wxScrollHelper* target)
{
	target_ = target;
}

void MyScrollBar::SetVisibility(wxScrollbarVisibility visibility)
{
	visibility_ = visibility;
	switch (visibility_)
	{
	case wxSHOW_SB_NEVER: {
		if (IsShown()) {
			Hide();
		}
	} break;
	case wxSHOW_SB_ALWAYS: {
		if (!IsShown()) {
			Show();
		}
	} break;
	default: {
		//
	} break;
	}
}

void MyScrollBar::OnSkinInfoChanged()
{
	Refresh();
}

bool MyScrollBar::IsCustomDraw() const
{
	return skin_info_ptr_ && skin_info_ptr_->style != SKIN_NONE;
}

int MyScrollBar::GetThumbPosition() const
{
	return IsCustomDraw() ? thumb_pos_ : Base::GetThumbPosition();
}

int MyScrollBar::GetThumbSize() const
{
	return IsCustomDraw() ? thumb_size_ : Base::GetThumbSize();
}

int MyScrollBar::GetPageSize() const
{
	return IsCustomDraw() ? page_size_ : Base::GetPageSize();
}

int MyScrollBar::GetRange() const
{
	return IsCustomDraw() ? range_ : Base::GetRange();
}

bool MyScrollBar::IsVertical() const
{
	return Base::IsVertical();
}

void MyScrollBar::SetThumbPosition(int viewStart)
{
	if (IsCustomDraw()) {
		if (viewStart < 0) {
			viewStart = 0;
		}
		else if (viewStart > (range_ - thumb_size_)) {
			viewStart = (range_ - thumb_size_);
		}
		if (viewStart != thumb_pos_) {
			thumb_pos_ = viewStart;
			Scroll(viewStart);
		}
	}
	else {
		Base::SetThumbPosition(viewStart);
	}
}

void MyScrollBar::SetScrollbar(int position, int thumbSize,
	int range, int pageSize,
	bool refresh)
{
	if (IsCustomDraw()) {
		thumb_pos_ = position;
		thumb_size_ = thumbSize;
		page_size_ = pageSize;
		range_ = range;
	}
	else {
		Base::SetScrollbar(position, thumbSize, range, pageSize, refresh);
	}
	switch (visibility_)
	{
	case wxSHOW_SB_NEVER: {
		ASSERT(!IsShown());
	} break;
	case wxSHOW_SB_ALWAYS: {
		ASSERT(IsShown());
	} break;
	default: {
		if (thumbSize != 0 && pageSize != 0) {
			if (!IsShown()) {
				Show();
				GetParent()->Layout();
			}
		}
		else {
			if (IsShown()) {
				Hide();
				GetParent()->Layout();
			}
		}
	} break;
	}
	if (IsCustomDraw()) {
		rc_thumb_ = {};
		Refresh();
	}
}

wxBEGIN_EVENT_TABLE(MyScrollBar, Base)
EVT_ERASE_BACKGROUND(MyScrollBar::OnErase)
EVT_PAINT(MyScrollBar::OnPaint)
EVT_MOUSE_EVENTS(MyScrollBar::OnMouse)
EVT_MOUSE_CAPTURE_LOST(MyScrollBar::OnMouseLost)
EVT_SCROLL(MyScrollBar::OnScroll)
//EVT_SIZE(MyScrollBar::OnSize)
wxEND_EVENT_TABLE()

void MyScrollBar::Calc()
{
	if (rc_thumb_.IsEmpty()) {
		auto rcClient = GetClientRect();
		auto thumb_pos = GetThumbPosition();
		auto thumb_size = GetThumbSize();
		auto page_size = GetPageSize();
		double range = GetRange();
		if (IsVertical()) {
			auto height = rcClient.height - rcClient.width * 2;
			ratio_ = height / range;
			rc_thumb_ = wxRect(0, rcClient.width + height * (thumb_pos / range)
				, rcClient.width, thumb_size * ratio_);
		}
		else {
			auto width = rcClient.width - rcClient.height * 2;
			ratio_ = width / range;
			rc_thumb_ = wxRect(rcClient.height + width * (thumb_pos / range), 0
				, thumb_size * ratio_, rcClient.height);
		}
	}
}

void MyScrollBar::Scroll(int position)
{
	if (target_) {
		if (IsVertical()) {
			int x = 0;
			auto win = target_->GetTargetWindow();
			if (win)
				x = win->GetScrollPos(wxSB_HORIZONTAL);
			target_->Scroll(x, GetThumbPosition());
		}
		else {
			int y = 0;
			auto win = target_->GetTargetWindow();
			if (win)
				y = win->GetScrollPos(wxSB_VERTICAL);
			target_->Scroll(GetThumbPosition(), y);
		}
	}
	if (IsCustomDraw()) {
		rc_thumb_ = {};
		Refresh();
	}
}

void MyScrollBar::OnErase(wxEraseEvent &event)
{
	if (IsCustomDraw()) {

	}
	else {
		event.Skip();
	}
}

void MyScrollBar::OnPaint(wxPaintEvent &event)
{
	if (IsCustomDraw()) {
		wxPaintDC dc(this);
		dc.Clear();

		Calc();

		auto rcClient = GetClientRect();
		//dc.FloodFill(0, 0, *wxBLUE);
		dc.GradientFillLinear(rcClient, *wxBLUE, *wxBLACK, wxEAST);

		dc.DrawRectangle(rc_thumb_);
	}
	else {
		event.Skip();
	}
}

void MyScrollBar::OnMouse(wxMouseEvent &event)
{
	if (IsCustomDraw()) {
		Calc();
		wxPoint pt = event.GetPosition();
		if (HasCapture()) {
			if (event.Dragging()) {
				if (IsVertical()) {
					int pos = (pt.y - track_) / ratio_;// +(pt.y > track_ ? _4_5_ctrl_ : -_4_5_ctrl_);
					if (pos != 0) {
						track_ = pt.y;
						SetThumbPosition(GetThumbPosition() + pos);
					}
				}
				else {
					int pos = (pt.x - track_) / ratio_;// +(pt.x > track_ ? _4_5_ctrl_ : -_4_5_ctrl_);
					if (pos != 0) {
						track_ = pt.x;
						SetThumbPosition(GetThumbPosition() + pos);
					}
				}
			}
			else if (event.LeftUp()) {
				ReleaseCapture();
			}
		}
		else {
			if (event.LeftDown()) {
				if (rc_thumb_.Contains(pt)) {
					if (IsVertical()) {
						track_ = pt.y;
					}
					else {
						track_ = pt.x;
					}
					CaptureMouse();
				}
			}
		}
	}
	else {
		event.Skip();
	}
}

void MyScrollBar::OnMouseLost(wxMouseCaptureLostEvent& event)
{
	if (IsCustomDraw()) {
		if (HasCapture()) {
			ReleaseMouse();
		}
	}
	else {
		event.Skip();
	}
}

void MyScrollBar::OnScroll(wxScrollEvent &event)
{
	event.Skip();
	Scroll(GetThumbPosition());
}

///

// a simple renderer that wraps each word on a new line
MyCodeViewListRenderer::MyCodeViewListRenderer()
	: wxDataViewCustomRenderer("void*", wxDATAVIEW_CELL_INERT, 0), view_(nullptr)
{
	wxASSERT(0);
}

MyCodeViewListRenderer::MyCodeViewListRenderer(MyCodeView* view)
	: wxDataViewCustomRenderer("void*", wxDATAVIEW_CELL_INERT, 0), view_(view)
{
}

void MyCodeViewListRenderer::OnSkinInfoChanged()
{
	fontName_ = skin_info_ptr_->artProvider->GetFont(wxRIBBON_ART_TAB_LABEL_FONT);
	fontCode_ = skin_info_ptr_->artProvider->GetFont(wxRIBBON_ART_BUTTON_BAR_LABEL_FONT);
	fontPrice_ = skin_info_ptr_->artProvider->GetFont(wxRIBBON_ART_PANEL_LABEL_FONT);

	crName_ = skin_info_ptr_->artProvider->GetColor(wxRIBBON_ART_TAB_LABEL_COLOUR);
	crCode_ = skin_info_ptr_->artProvider->GetColor(wxRIBBON_ART_BUTTON_BAR_LABEL_COLOUR);
}

void MyCodeViewListRenderer::RenderBackground(wxDC* dc, const wxRect& rect)
{
	//这里可以改变非选中行背景
	//dc->SetBrush(*wxLIGHT_GREY_BRUSH);
	//dc->DrawRectangle(rect);
}

bool MyCodeViewListRenderer::Render(wxRect rect, wxDC *dc, int state)
{
	if (!val_) {
		return false;
	}

	//选中状态颜色像这样修改
	if (state & wxDATAVIEW_CELL_SELECTED) {
		//dc->SetBrush(*wxLIGHT_GREY_BRUSH);
		//dc->DrawRectangle(rect);
	}
	if (val_->Value != val_->OldValue) {
		//dc->SetBrush(skin_info_ptr_->GetBursh(val_->Value - val_->OldValue));
		//dc->DrawRectangle(rect);
		dc->GradientFillLinear(rect, wxColor(255, 255, 255)
			, skin_info_ptr_->GetColor(val_->Value - val_->OldValue));
	}

	rect.Deflate(skin_info_ptr_->xySpace);

	//Name
	dc->SetFont(fontName_);
	dc->SetTextForeground(crName_);
	dc->DrawLabel(val_->Name().c_str(), rect, wxALIGN_LEFT | wxALIGN_TOP);
	//Code
	dc->SetFont(fontCode_);
	dc->SetTextForeground(crCode_);
	dc->DrawLabel(val_->Code().c_str(), rect, wxALIGN_LEFT | wxALIGN_BOTTOM);
	//RenderText(m_value, 0, rect, dc, state);

	zqdb::Code code((HZQDB)val_->Data);
	wxString strClose(wxT("——")), strZD(wxT("——")), strZDF(wxT("——"));
	double zd = 0., zdf = 0.;
	bool invalid_flag = true;
	/*if (!ZQDBIsSubscribeMarketDataAll(code)) {
		if (code.IsSubscribe()) {
			invalid_flag = !code.IsMarketValid();
		}
		else {
			strZDF = wxT("未订阅");
		}
	}
	else*/ {
		invalid_flag = !code.IsMarketValid(false);
	}
	if (!invalid_flag) {
		auto digits = (int)ZQDBGetPriceDigits(code);
		auto close = code->Close, yclose = ZQDBGetYClose(code, true);
		strClose = wxString::Format("%.*f", digits, close);
		zd = val_->Value;
		zdf = val_->ValueF * 100;
		strZD = wxString::Format("%+.2f", zd);
		strZDF = wxString::Format("%+.2f%%", zdf);
	}

	dc->SetFont(fontPrice_);
	dc->SetTextForeground(skin_info_ptr_->GetCtrlColor(zd));
	dc->DrawLabel(strClose, rect, wxALIGN_RIGHT | wxALIGN_TOP);

	dc->SetFont(fontPrice_);
	dc->SetTextForeground(skin_info_ptr_->GetCtrlColor(zd));
	dc->DrawLabel(strZD, rect, wxALIGN_CENTER | wxALIGN_BOTTOM);

	dc->SetFont(fontPrice_);
	dc->SetTextForeground(skin_info_ptr_->GetCtrlColor(zd));
	dc->DrawLabel(strZDF, rect, wxALIGN_RIGHT | wxALIGN_BOTTOM);

	return true;
}

wxSize MyCodeViewListRenderer::GetSize() const
{
	wxSize txtSize;
	txtSize.x = -1;//skin_info_ptr_->xySpace.x + val_->Name.size() * skin_info_ptr_->xyName.x + skin_info_ptr_->xySpace.x;
	txtSize.y = skin_info_ptr_->xySpace.y + skin_info_ptr_->xyText.y + skin_info_ptr_->xySpace.y + skin_info_ptr_->xyText.y + skin_info_ptr_->xySpace.y;
	return txtSize;
}

bool MyCodeViewListRenderer::SetValue(const wxVariant &value)
{
	val_ = (SmartKBItem*)value.GetVoidPtr();
	return true;
}

bool MyCodeViewListRenderer::GetValue(wxVariant &WXUNUSED(value)) const
{
	return true;
}

#if wxUSE_ACCESSIBILITY
wxString MyCodeViewListRenderer::GetAccessibleDescription() const
{
	return val_->Name().c_str();
}
#endif // wxUSE_ACCESSIBILITY

///

// a simple renderer that wraps each word on a new line
MyCodeListCodeRenderer::MyCodeListCodeRenderer()
	: wxDataViewCustomRenderer("void*", wxDATAVIEW_CELL_INERT, 0)
{ 
}

bool MyCodeListCodeRenderer::Render(wxRect rect, wxDC *dc, int state)
{
	//Name
	dc->SetFont(skin_info_ptr_->fontText);
	dc->SetTextForeground(skin_info_ptr_->crCtrlForgnd);
	dc->DrawLabel(code_, rect, wxALIGN_LEFT | wxALIGN_TOP);
	//Code
	dc->SetFont(skin_info_ptr_->fontTitle);
	dc->SetTextForeground(skin_info_ptr_->crCtrlForgnd);
	dc->DrawLabel(name_, rect, wxALIGN_LEFT | wxALIGN_BOTTOM);
	//RenderText(m_value, 0, rect, dc, state);
	return true;
}

wxSize MyCodeListCodeRenderer::GetSize() const
{
	wxSize txtSize;
	txtSize.x = name_.size() * skin_info_ptr_->xyText.x;
	txtSize.y = skin_info_ptr_->xyText.y + skin_info_ptr_->xyTitle.y;
	return txtSize;
}

bool MyCodeListCodeRenderer::SetValue(const wxVariant &value)
{
	wxCSConv utf8cv(wxFONTENCODING_UTF8);

	h_ = (HZQDB)value.GetVoidPtr();
	auto info = (CODEINFO*)ZQDBGetValue(h_);
	name_ = utf8cv.cMB2WX(info->Name);//value.GetString();
	code_ = utf8cv.cMB2WX(info->Code);
	return true;
}

bool MyCodeListCodeRenderer::GetValue(wxVariant &WXUNUSED(value)) const 
{ 
	return true; 
}

#if wxUSE_ACCESSIBILITY
wxString MyCodeListCodeRenderer::GetAccessibleDescription() const
{
	return name_;
}
#endif // wxUSE_ACCESSIBILITY

///

MyCodeListNameRenderer::MyCodeListNameRenderer()
	: wxDataViewCustomRenderer("void*", wxDATAVIEW_CELL_INERT, 0)
{ 

}

bool MyCodeListNameRenderer::Render(wxRect rect, wxDC *dc, int state)
{
	if (!info_ || ZQDBIsInvalidValue(info_->Close) || ZQDBIsInvalidValue(info_->YClose)) {
		return false;
	}
	//
	//Price
	//change zdf
	//
	double price = info_->Close;
	double change = info_->Close - info_->YClose;
	double zdf = change / info_->YClose * 100;
	//Price
	wxString strPrice = wxString::Format("%.2f", price);
	dc->SetFont(skin_info_ptr_->fontText);
	dc->SetTextForeground(skin_info_ptr_->GetColor(change));
	dc->DrawLabel(strPrice, rect, wxALIGN_LEFT | wxALIGN_TOP);
	//change
	wxString strChange = wxString::Format("%.2f", change);
	dc->SetFont(skin_info_ptr_->fontTitle);
	dc->SetTextForeground(skin_info_ptr_->GetColor(change));
	dc->DrawLabel(strChange, rect, wxALIGN_LEFT | wxALIGN_BOTTOM);
	//zdf
	wxString strZDF = wxString::Format("%.2f", change);
	dc->SetFont(skin_info_ptr_->fontTitle);
	dc->SetTextForeground(skin_info_ptr_->GetColor(change));
	dc->DrawLabel(strZDF, rect, wxALIGN_RIGHT | wxALIGN_BOTTOM);
	//RenderText(m_value, 0, rect, dc, state);
	return true;
}

wxSize MyCodeListNameRenderer::GetSize() const
{
	wxSize txtSize;
	txtSize.x = -1;
	txtSize.y = skin_info_ptr_->xyText.y + skin_info_ptr_->xyTitle.y;
	return txtSize;
}

bool MyCodeListNameRenderer::SetValue(const wxVariant &value)
{
	wxCSConv utf8cv(wxFONTENCODING_UTF8);

	h_ = (HZQDB)value.GetVoidPtr();
	info_ = (CODEINFO*)ZQDBGetValue(h_);
	return true;
}

bool MyCodeListNameRenderer::GetValue(wxVariant &WXUNUSED(value)) const 
{ 
	return true;
}

#if wxUSE_ACCESSIBILITY
wxString MyCodeListNameRenderer::GetAccessibleDescription() const
{
	return "";
}
#endif // wxUSE_ACCESSIBILITY
