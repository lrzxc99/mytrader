#pragma once

#include "base.h"
#include "mytraderdef.h"
#include "settings.h"
#include "smartkb.h"
#include <wx/snglinst.h>
#include <wx/debugrpt.h>

extern bool g_wait;
extern bool g_clear_settings;
extern bool g_clear_data;
extern bool g_test;
extern bool g_full;

class MySmartKBDlg;
class MyFrame;
class MyTechFrame;
class MyCalcFrame;
class MyStrategyFrame;
class MyDataFrame;
class MyWebFrame;

enum {
	EXIT_FLAG_NONE = 0,
	EXIT_FLAG_NORMAL = 1, //正常退出
	EXIT_FLAG_CLOSE = 2, //收盘退出
};

// -- application --

class MyEventFilter : public wxEventFilter
{
public:
	MyEventFilter();
	~MyEventFilter();
	virtual int FilterEvent(wxEvent& event);
private:
	wxDECLARE_NO_COPY_CLASS(MyEventFilter);
};

class MyApp : public zqdb::App
	, public MyEventFilter
	, public SettingsMap<MyApp>
	, public zqdb::NotifyImplT<MyApp>
	//, public zqdb::MsgImplT<MyApp>
{
	typedef zqdb::App Base;
	typedef SettingsMap<MyApp> SettingsBase;
	typedef zqdb::NotifyImplT<MyApp> NotifyBase;
	//typedef zqdb::MsgImplT<MyApp> MsgBase;
protected:
	std::shared_ptr<wxSingleInstanceChecker> snglinst_checker_;
	wxTaskBarIcon *taskbaricon_ = nullptr; //系统托盘
#if defined(__WXOSX__) && wxOSX_USE_COCOA
	MyTaskBarIcon *dockicon_ = nullptr;
#endif
	std::vector<wxDialog*> modaldlgs_;
	boost::property_tree::ptree cfg_; //配置信息
	MySmartKBDlg *smartkbdlg_ = nullptr; //键盘精灵
	MyFrame* frame_ = nullptr; //主框架
	std::vector<MyTechFrame *> tech_frames_; //技术框架列表
	std::vector<MyCalcFrame*> calc_frames_; //公式框架
	std::vector<MyStrategyFrame *> strategy_frames_; //策略框架列表
	MyDataFrame* data_frame_ = nullptr;
	//MyWebFrame* web_frame_ = nullptr;
	bool init_flag_ = false; //初始化标志
	wxString version_; //版本号
	size_t run_flag_ = 0; //运行标志
	size_t suspend_flag_ = 0; //挂起标志
	size_t exit_flag_ = 0; //退出标志
	wxTimer timer_;
	wxArrayString selfsels_; //未识别的订阅，需要等待对应市场初始化完成才能识别
	std::map<HZQDB,int> subs_; //订阅列表
	int last_level_ = 0;
	wxString last_tips_;
protected:
	void LoadSkinInfo();
	void DecorateSplashScreen(wxBitmap& bmp, bool about = false);
public:
	MyApp();

#if wxUSE_CMDLINE_PARSER
	virtual bool OnCmdLineParsed(wxCmdLineParser& parser) wxOVERRIDE;
	virtual void OnInitCmdLine(wxCmdLineParser& parser) wxOVERRIDE;
#endif

    bool OnInit() wxOVERRIDE;
    int OnExit() wxOVERRIDE;

#ifndef _DEBUG
	// called when a crash occurs in this application
	virtual void OnFatalException() wxOVERRIDE;
#endif//
	// this is where we really generate the debug report
	void GenerateReport(wxDebugReport::Context ctx);

	//inline const wxIcon& GetIcon() { return icon_; }

	void SetSkinStyle(SkinStyle style);
	void OnSkinInfoChanged();

	void ShowBusyInfo(const wxString& msg);
	void UpdateTaskBarInfo(const wxString& info);
	void PlaySound(const wxString& file);

	void LoadSettings();
	inline bool IsInitFlag() { return init_flag_; }
	void Init();
	void Start();

	bool PreRun();
	enum {
		POST_EVERYDAY_EXEC_IF_TIMEOUT = 0X0001,
		POST_EVERYDAY_EXEC_NEXTDAY = 0X0002,
	};
	void PostEveryDay(const uint32_t time, std::function<void()> && task, size_t flags = 0);
	
	inline size_t IsRunFlag() { return run_flag_; }
	bool Run();
	void FinalRelease();
	void FinalExit();
	void Exit(size_t flag = EXIT_FLAG_NORMAL);
	void DoExit(size_t flag = EXIT_FLAG_NORMAL);
	void PostExit(size_t flag = EXIT_FLAG_NORMAL);
	inline size_t IsExitFlag() { return exit_flag_; }
	
	wxString GetAppMode();
	wxString GetAppTitle(bool ex = false);
	wxString GetAppStatus(bool ex = false);

	wxDialog* AnyModalDlg();
	bool RemoveModalDlg(wxDialog* dlg);
	void AddModalDlg(wxDialog* dlg);

	inline MySmartKBDlg* GetSmartKBDlg() { return smartkbdlg_; }
	inline void ResetSmartKBDlg() { smartkbdlg_ = nullptr; }

	void DoHide();
	wxFrame* GetFrame();
	bool FindTechFrame(wxFrame * frame);
	void ShowFrame(wxFrame* frame = nullptr);
	void ResetFrame(wxFrame * frame);

	void Goto(HZQDB h, wxWindow* top = nullptr);

	int ShowCalcFuncDlg(wxWindow* parent, CALC_TYPE type, const char* name);

	void ResetCalcFrame(MyCalcFrame* frame);
	void ShowCalcFrame(std::shared_ptr<zqdb::FuncContainerInfo> info_ptr);

	void ResetDataFrame();

	//运行策略一般需要两个部分一个是筛选，一个是策略
	//筛选可以是一组代码、品种、市场、条件的组合，最简单的筛选就是一个代码
	//策略就是实时处理筛选出的代码并给出交易判断
	//void RunStrategy(const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
	//void ShowStrategyFrame(const wxString& name);
	void ResetStrategyFrame(MyStrategyFrame * frame);

	void Resettings(bool showMsg = true);
	void DownloadData();
	void ExportData();
	void SaveClose();
	void DoClearData();
	void ClearData(bool showMsg = true);
	void DoRestart(const wxString& args);
	void Restart(bool showMsg = true);
	void Test(size_t speed);

	void ShowAbout(wxWindow* parent = nullptr);

	void LoadSelfSel();
	void SaveSelfSel();
	bool IsSelfSel(HZQDB h);
	void AddSelfSel(HZQDB h, bool save = true);
	void RemoveSelfSel(HZQDB h, bool save = true);
	void Subscribe(HZQDB h);
	void UnSubscribe(HZQDB h);
	bool IsSubscribe(HZQDB h);

	void OnTimer(wxTimerEvent& evt);

	void OnNotifyStatus(HZQDB h);
	void OnNotifyAdd(HZQDB h);
	void OnNotifyRemove(HZQDB h);
	void OnNotifyUpdate(HZQDB h);
	void HandleNotify(HZQDB h, ZQDB_NOTIFY_TYPE notify);

	int HandleNetMsg(HNMSG hmsg, size_t* flags);

	void ShowTips(int level, const char* xml, size_t xmlflag);
	void ShowTips(int level, wxString&& str);
	inline const wxString& GetLastTips(int* level = nullptr) { 
		if (level) {
			*level = last_level_;
		}
		return last_tips_; 
	}
};

wxDECLARE_APP(MyApp);
