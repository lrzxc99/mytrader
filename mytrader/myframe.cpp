#include "myapp.h"
#include "myframe.h"
#include "mylogdlg.h"
#include "mysettingsdlg.h"
#include <techdlg.h>
#include <techedit.h>

///

wxBEGIN_EVENT_TABLE(MyFrame, Base)
EVT_RIBBONBUTTONBAR_CLICKED(ID_DATA_LISTEN, MyFrame::OnDataListen)
EVT_RIBBONBUTTONBAR_CLICKED(ID_DATA_RECORD, MyFrame::OnDataRecord)
EVT_RIBBONBUTTONBAR_CLICKED(ID_DATA_TEST, MyFrame::OnDataTest)
EVT_RIBBONBUTTONBAR_DROPDOWN_CLICKED(ID_DATA_TEST, MyFrame::OnDataTestDropdown)
EVT_MENU(ID_DATA_TEST, MyFrame::OnDataTestTest)
EVT_MENU(ID_DATA_TRADE_TEST, MyFrame::OnDataTestTradeTest)
EVT_MENU(ID_DATA_DEPTH_RECORD, MyFrame::OnDataTestDepthRecord)
EVT_MENU(ID_DATA_DEPTH_TEST, MyFrame::OnDataTestDepthTest)
//EVT_MENU_RANGE(ID_DATA_TEST_RECORD, ID_DATA_TEST_RECORD_MAX, MyFrame::OnDataTestRecord)
EVT_RIBBONBUTTONBAR_CLICKED(ID_CALC_EDIT, MyFrame::OnCalcEdit)
EVT_RIBBONBUTTONBAR_DROPDOWN_CLICKED(ID_CALC_EDIT, MyFrame::OnCalcEditDropdown)
EVT_MENU_RANGE(ID_SYS_BEGIN, ID_SYS_END, MyFrame::OnSysGoto)
EVT_RIBBONBUTTONBAR_CLICKED(ID_CALC_TRADEFLAG, MyFrame::OnCalcTradeFlag)
EVT_CLOSE(MyFrame::OnCloseEvent)
wxEND_EVENT_TABLE()

MyFrame::MyFrame(const char* xml, size_t xmlflag)
    : Base(xml, xmlflag)
	, all_strategy_func_(CALC_STRATEGY)
{
	CFG_FROM_XML(cfg, xml, xmlflag);

	test_timer_.Bind(wxEVT_TIMER, &MyFrame::OnTestTimer, this);
	Bind(wxEVT_RIBBONBUTTONBAR_CLICKED, &MyFrame::OnStrategy, this, ID_STRATEGY, ID_STRATEGY_MAX);
}

MyFrame::~MyFrame()
{
	/*zqdb::Calc::Container container;
	auto key = code_view_->GetKey(container);
	if (container) {
		auto name = key;
		key.clear();
		if (container.GetCalcType() == CALC_CONTAINER) {
			auto func_name = container.GetCalcName();
			if (strcmp(func_name, "StdContainer") == 0) {
				auto h = container.Data();
				if (h) {
					switch (h->type)
					{
					case ZQDB_HANDLE_TYPE_EXCHANGE: {
						zqdb::Exchange exchange(h);
						key = wxString::Format(wxT("%s.%s"), exchange->Exchange, func_name);
					} break;
					case ZQDB_HANDLE_TYPE_PRODUCT: {
						zqdb::Product product(h);
						key = wxString::Format(wxT("%s.%s.%s"), product->Product, product->Exchange, func_name);
					} break;
					default: {
						key = name + wxT(".") + func_name;
					} break;
					}
				}
			}
			else {
				key = name + wxT(".") + func_name;
			}
		}
	}
	wxGetApp().SetFrameLastKey(key);

	MY_CODE_SORT_TYPE sort_type = SORT_ZDF;
	int sort = IsSort(&sort_type);
	wxGetApp().SetFrameLastSortType(sort_type);
	wxGetApp().SetFrameLastSort(sort);*/
}

void MyFrame::OnCloseEvent(wxCloseEvent& evt)
{
	evt.Veto();
	int action = wxCANCEL;
	if (ZQDBIsTest()) {
		action = wxOK;
	}
	else {
		if (wxGetApp().GetFrameOnCloseRember()) {
			action = wxGetApp().GetFrameOnCloseAction();
#ifdef _DEBUG
			wxGetApp().SetFrameOnCloseRember(false);
#endif//
		}
		else {
			wxString tips;
			long style = 0;
			if (ZQDBIsTest()) {
				tips = wxT("确定要退出超级回测吗？\n")
					wxT("点击“取消”按钮，超级回测将会隐藏在后台继续运行。");
				style = wxOK | wxCANCEL;
			}
			else {
				if (ZQDBIsClient()) {
					tips = wxT("确定要退出程序吗？\n")
						wxT("点击“取消”按钮，程序将会隐藏在后台继续运行。");
					style = wxOK | wxCANCEL;
				}
				else {
					tips = wxT("退出程序后将无法收集行情数据，将导致历史行情数据缺失。\n")
						wxT("默认“取消”按钮，程序将会隐藏在后台继续收集行情数据。\n")
						wxT("确定要退出程序吗？");
					style = wxOK | wxCANCEL | wxCANCEL_DEFAULT;
				}
			}
#if 0
			MyCheckDlg dlg(this, tips
				, wxT("下次不再提示"), false
				, wxT("提示"), style);
			action = dlg.ShowModal();
			if (dlg.IsCheck()) {
				wxGetApp().SetFrameOnCloseAction(action);
				wxGetApp().SetFrameOnCloseRember(true);
			}
#else
			wxRichMessageDialog dlg(this, tips, wxT("提示"), wxOK | wxCANCEL | wxICON_INFORMATION);
			//dlg.SetYesNoLabels(m_labels[Btn_Yes]->GetValue(),
			//	m_labels[Btn_No]->GetValue());
			dlg.ShowCheckBox(wxT("下次不再提示"), false);
			//dlg.ShowDetailedText(wxT("Detaile"));
			//dlg.SetFooterText(wxT("Footer"));
			//dlg.SetFooterIcon(wxICON_INFORMATION);
			switch (dlg.ShowModal())
			{
			case wxID_YES:
			case wxID_OK:
				action = wxOK;
				break;
			case wxID_CANCEL:
			case wxID_NO:
			default:
				action = wxCANCEL;
				break;
			}
			if (dlg.IsCheckBoxChecked()) {
				wxGetApp().SetFrameOnCloseAction(action);
				wxGetApp().SetFrameOnCloseRember(true);
			}
#endif//
		}
	}
	if (wxOK == action) {
		//wxGetApp().Post(100, []() {
		wxGetApp().DoExit();
		//});
	}
	else {
		wxGetApp().DoHide();
	}
}

void MyFrame::AddFirstPages()
{
	wxRibbonPage* home = new wxRibbonPage(m_ribbon, wxID_ANY, wxT("开始"));

	auto navigate_panel = new wxRibbonPanel(home, wxID_ANY, wxT("导航"), wxNullBitmap, wxDefaultPosition, wxDefaultSize
		, wxRIBBON_PANEL_NO_AUTO_MINIMISE | wxRIBBON_PANEL_EXT_BUTTON);
	navigate_panel->Bind(wxEVT_RIBBONPANEL_EXTBUTTON_ACTIVATED, &MyFrame::OnNavigateSetting, this);
	navigate_bar_ = new wxRibbonButtonBar(navigate_panel);
	InnerUpdateNavigate();

	/*auto fast_access_panel = new wxRibbonPanel(home, wxID_ANY, "快速访问");
	wxRibbonButtonBar* fast_access_bar = new wxRibbonButtonBar(fast_access_panel);
	fast_access_bar->AddButton(ID_MARKET_ALL, "全部", skin_info_ptr_->GetBitmap32(wxEmptyString));
	fast_access_bar->SetButtonMaxSizeClass(ID_MARKET_ALL, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->SetButtonMinSizeClass(ID_MARKET_ALL, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->AddButton(ID_MARKET_MAIN, "主力", skin_info_ptr_->GetBitmap32(wxEmptyString));
	fast_access_bar->SetButtonMaxSizeClass(ID_MARKET_MAIN, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->SetButtonMinSizeClass(ID_MARKET_MAIN, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->AddButton(ID_MARKET_SELF, "自选", skin_info_ptr_->GetBitmap32(wxEmptyString), wxEmptyString);
	fast_access_bar->SetButtonMaxSizeClass(ID_MARKET_SELF, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->SetButtonMinSizeClass(ID_MARKET_SELF, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->AddButton(ID_TRADE_POSITION, "持仓", skin_info_ptr_->GetBitmap32(wxEmptyString));
	fast_access_bar->SetButtonMaxSizeClass(ID_TRADE_POSITION, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->SetButtonMinSizeClass(ID_TRADE_POSITION, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->AddButton(ID_TRADE_ORDER, "委托", skin_info_ptr_->GetBitmap32(wxEmptyString));
	fast_access_bar->SetButtonMaxSizeClass(ID_TRADE_ORDER, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->SetButtonMinSizeClass(ID_TRADE_ORDER, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->AddButton(ID_TRADE_TRADE, "成交", skin_info_ptr_->GetBitmap32(wxEmptyString));
	fast_access_bar->SetButtonMaxSizeClass(ID_TRADE_TRADE, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	fast_access_bar->SetButtonMinSizeClass(ID_TRADE_TRADE, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);*/

	//auto market_panel = new wxRibbonPanel(home, wxID_ANY, "快速导航");
	//wxRibbonButtonBar* market_bar = new wxRibbonButtonBar(market_panel);
	//int product_id = ID_MARKET_PRODUCT;
	//for (size_t i = 0; i < allexchange.size(); i++)
	//{
	//	zqdb::Exchange exchange(allexchange[i]);
	//	//auto item = market_bar->AddButton(ID_MARKET_EXCHANGE + i, utf2wxString(exchange->Exchange), skin_info_ptr_->GetBitmap32(wxEmptyString));
	//	//market_bar->SetItemClientData(item, allexchange[i]);
	//	//market_bar->SetButtonMaxSizeClass(ID_MARKET_EXCHANGE + i, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	//	//market_bar->SetButtonMinSizeClass(ID_MARKET_EXCHANGE + i, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	//	/*auto market_panel = new wxRibbonPanel(home, ID_MARKET_EXCHANGE + i, utf2wxString(exchange->Exchange));
	//	market_panel->SetToolTip(utf2wxString(exchange->Name));
	//	wxRibbonButtonBar* market_bar = new wxRibbonButtonBar(market_panel);*/
	//	////这里可以显示最活跃品种
	//	//zqdb::AllProduct allproduct(allexchange[i]);
	//	///*std::sort(allproduct.begin(), allproduct.end(), [](HZQDB x, HZQDB y) {
	//	//	zqdb::Product xproduct(x);
	//	//	zqdb::Product yproduct(y);
	//	//	return strcmp(xproduct->Name, yproduct->Name) < 0;
	//	//});*/
	//	//for (size_t k = 0; k < allproduct.size() && k < 2; k++)
	//	//{
	//	//	zqdb::Product product(allproduct[k]);
	//	//	auto item = market_bar->AddButton(product_id, utf2wxString(product->Name), skin_info_ptr_->GetBitmap32(wxEmptyString));
	//	//	market_bar->SetItemClientData(item, allproduct[k]);
	//	//	market_bar->SetButtonMaxSizeClass(product_id, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	//	//	market_bar->SetButtonMinSizeClass(product_id, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	//	//	++product_id;
	//	//}
	//	//这里显示市场下拉菜单，可以查看所有品种
	//	auto item = market_bar->AddHybridButton(ID_MARKET_EXCHANGE + i
	//		, utf2wxString(exchange->Exchange), skin_info_ptr_->GetBitmap32(wxEmptyString));
	//	market_bar->SetItemClientData(item, allexchange[i]);
	//	market_bar->SetButtonMaxSizeClass(ID_MARKET_EXCHANGE + i, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	//	market_bar->SetButtonMinSizeClass(ID_MARKET_EXCHANGE + i, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	//}
	//ASSERT(product_id < ID_MARKET_PRODUCT_MAX);

	/*auto filter_panel = new wxRibbonPanel(home, wxID_ANY, wxT("筛选"), wxNullBitmap, wxDefaultPosition, wxDefaultSize
		, wxRIBBON_PANEL_NO_AUTO_MINIMISE | wxRIBBON_PANEL_EXT_BUTTON);
	filter_panel->Bind(wxEVT_RIBBONPANEL_EXTBUTTON_ACTIVATED, &MyFrame::OnFilterSetting, this);
	filter_bar_ = new wxRibbonButtonBar(filter_panel);
	InnerUpdateFilter();*/

#ifdef _SORT
	//点击一次降序、二次升序、三次不排序
	auto sort_panel = new wxRibbonPanel(home, wxID_ANY, wxT("排序"), wxNullBitmap, wxDefaultPosition, wxDefaultSize
		, wxRIBBON_PANEL_NO_AUTO_MINIMISE | wxRIBBON_PANEL_EXT_BUTTON);
	sort_panel->Bind(wxEVT_RIBBONPANEL_EXTBUTTON_ACTIVATED, &MyFrame::OnSortSetting, this);
	sort_bar_ = new wxRibbonButtonBar(sort_panel);
	InnerUpdateSort();
#endif

	wxRibbonPanel *analysis_panel = new wxRibbonPanel(home, wxID_ANY, "技术分析",
		wxNullBitmap, wxDefaultPosition, wxDefaultSize,
		wxRIBBON_PANEL_NO_AUTO_MINIMISE |
		wxRIBBON_PANEL_EXT_BUTTON);
	analysis_panel->Bind(wxEVT_RIBBONPANEL_EXTBUTTON_ACTIVATED, &MyFrame::OnTechSetting, this);
	tech_bar_ = new wxRibbonToolBar(analysis_panel);
	InnerUpdateTechBar();

	/*auto template_panel = new wxRibbonPanel(home, wxID_ANY, "模板");
	wxRibbonButtonBar* template_bar = new wxRibbonButtonBar(template_panel);
	template_bar->AddButton(ID_TEMPLATE_SAVE, "保存", skin_info_ptr_->GetBitmap32(wxEmptyString));
	template_bar->SetButtonMaxSizeClass(ID_TEMPLATE_SAVE, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	template_bar->SetButtonMinSizeClass(ID_TEMPLATE_SAVE, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	template_bar->AddButton(ID_TEMPLATE_NEW_WINDOW, "新窗口", skin_info_ptr_->GetBitmap32(wxEmptyString));
	template_bar->SetButtonMaxSizeClass(ID_TEMPLATE_NEW_WINDOW, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	template_bar->SetButtonMinSizeClass(ID_TEMPLATE_NEW_WINDOW, wxRIBBON_BUTTONBAR_BUTTON_LARGE);*/

	auto sys_panel = new wxRibbonPanel(home, wxID_ANY, wxT("系统"), wxNullBitmap, wxDefaultPosition, wxDefaultSize
		, wxRIBBON_PANEL_NO_AUTO_MINIMISE | wxRIBBON_PANEL_EXT_BUTTON);
	sys_panel->Bind(wxEVT_RIBBONPANEL_EXTBUTTON_ACTIVATED, &MyFrame::OnCalcSetting, this);
	sys_bar_ = new wxRibbonButtonBar(sys_panel);
	/*if (ZQDBIsClient()) {
		sys_bar_->AddButton(ID_DATA_SYNC_DATA, wxT("下载数据"), skin_info_ptr_->GetBitmap32(wxT("下载")));
	} else */if (ZQDBIsServer()) {
		sys_bar_->AddToggleButton(ID_DATA_LISTEN, wxT("云服务"), skin_info_ptr_->GetBitmap32(wxT("服务")));
		sys_bar_->ToggleButton(ID_DATA_LISTEN, ZQDBIsListen());
	}
	if (ZQDBIsTest()) {
		sys_bar_->AddToggleButton(ID_DATA_TEST, wxT("开始回测"), skin_info_ptr_->GetBitmap32(wxT("回放")));
		//, wxArtProvider::GetBitmap(wxART_ADD_BOOKMARK, wxART_OTHER, wxSize(32, 32)));
		//sys_bar_->SetButtonTextMinWidth(ID_DATA_TEST, wxT("回放录制的全市场数据"));
		sys_bar_->SetButtonMaxSizeClass(ID_DATA_TEST, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
		sys_bar_->SetButtonMinSizeClass(ID_DATA_TEST, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	}
	else {
#if 0
		if (ZQDBIsRecording()) {
			sys_bar_->AddToggleButton(ID_DATA_RECORD, wxT("录制中"), skin_info_ptr_->GetBitmap32(wxT("录制")));
			sys_bar_->ToggleButton(ID_DATA_RECORD, true);
		}
		else {
			sys_bar_->AddToggleButton(ID_DATA_RECORD, wxT("录制"), skin_info_ptr_->GetBitmap32(wxT("录制")));
		}
		//, wxArtProvider::GetBitmap(wxART_ADD_BOOKMARK, wxART_OTHER, wxSize(32, 32)));
		//sys_bar_->SetButtonTextMinWidth(ID_DATA_RECORD, wxT("开始/停止录制全市场数据"));
		sys_bar_->SetButtonMaxSizeClass(ID_DATA_RECORD, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
		sys_bar_->SetButtonMinSizeClass(ID_DATA_RECORD, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
#endif
		sys_bar_->AddHybridButton(ID_DATA_TEST, wxT("快速回放"), skin_info_ptr_->GetBitmap32(wxT("回放")));
		//, wxArtProvider::GetBitmap(wxART_ADD_BOOKMARK, wxART_OTHER, wxSize(32, 32)));
		//sys_bar_->SetButtonTextMinWidth(ID_DATA_TEST, wxT("回放录制的全市场数据"));
		sys_bar_->SetButtonMaxSizeClass(ID_DATA_TEST, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
		sys_bar_->SetButtonMinSizeClass(ID_DATA_TEST, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	}
	if (!ZQDBIsUserObserver()) {
		sys_bar_->AddHybridButton(ID_CALC_EDIT, wxT("系统定制"), skin_info_ptr_->GetBitmap32(wxT("公式管理")));
		//, wxArtProvider::GetBitmap(wxART_HELP_BOOK, wxART_OTHER, wxSize(32, 32)));
		//sys_bar_->SetButtonTextMinWidth(ID_CALC_EDIT, wxT("管理指标、过滤器、脚本、策略"));
		sys_bar_->SetButtonMaxSizeClass(ID_CALC_EDIT, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
		sys_bar_->SetButtonMinSizeClass(ID_CALC_EDIT, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	}
	//sys_bar_->AddHybridButton(ID_CALC_MONITOR, wxT("监控中心"), skin_info_ptr_->GetBitmap32(wxT("监控中心")));
	////sys_bar_->SetButtonTextMinWidth(ID_CALC_MONITOR, wxT("监控全市场市场动态变化"));
	//sys_bar_->SetButtonMaxSizeClass(ID_CALC_MONITOR, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	//sys_bar_->SetButtonMinSizeClass(ID_CALC_MONITOR, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	//sys_bar_->AddHybridButton(ID_CALC_MONITOR, wxT("监控中心"), skin_info_ptr_->GetBitmap32(wxT("涨跌速")));

#ifdef _TRADE

	sys_bar_->AddToggleButton(ID_CALC_TRADEFLAG, wxT("智能交易"), skin_info_ptr_->GetBitmap32(wxT("智能交易")));
		//, wxArtProvider::GetBitmap(wxART_ADD_BOOKMARK, wxART_OTHER, wxSize(32, 32)));
	//sys_bar_->SetButtonTextMinWidth(ID_CALC_TRADEFLAG, wxT("允许或禁止算法交易"));
	sys_bar_->SetButtonMaxSizeClass(ID_CALC_TRADEFLAG, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	sys_bar_->SetButtonMinSizeClass(ID_CALC_TRADEFLAG, wxRIBBON_BUTTONBAR_BUTTON_LARGE);

	//脚本
	auto script_panel = new wxRibbonPanel(home, wxID_ANY, wxT("快速脚本"), wxNullBitmap, wxDefaultPosition, wxDefaultSize
		, wxRIBBON_PANEL_NO_AUTO_MINIMISE | wxRIBBON_PANEL_EXT_BUTTON);
	script_panel->Bind(wxEVT_RIBBONPANEL_EXTBUTTON_ACTIVATED, &MyFrame::OnScriptSetting, this);
	script_bar_ = new wxRibbonButtonBar(script_panel);
	InnerUpdateScript();

	//策略
	auto strategy_panel = new wxRibbonPanel(home, wxID_ANY, wxT("策略算法"), wxNullBitmap, wxDefaultPosition, wxDefaultSize
		, wxRIBBON_PANEL_NO_AUTO_MINIMISE | wxRIBBON_PANEL_EXT_BUTTON);
	strategy_panel->Bind(wxEVT_RIBBONPANEL_EXTBUTTON_ACTIVATED, &MyFrame::OnStrategySetting, this);
	strategy_bar_ = new wxRibbonButtonBar(strategy_panel);
	InnerUpdateStrategy();

#endif//_TRADE

	/*auto other_panel = new wxRibbonPanel(home, wxID_ANY, "其他", wxNullBitmap, wxDefaultPosition, wxDefaultSize
		, wxRIBBON_PANEL_NO_AUTO_MINIMISE | wxRIBBON_PANEL_EXT_BUTTON);
	wxRibbonButtonBar* other_bar = new wxRibbonButtonBar(other_panel);
	other_bar->AddButton(ID_CLEAR_SETTINGS, "重新设置", skin_info_ptr_->GetBitmap32(wxEmptyString));
	other_bar->SetButtonMaxSizeClass(ID_CLEAR_SETTINGS, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	other_bar->SetButtonMinSizeClass(ID_CLEAR_SETTINGS, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	other_bar->AddButton(ID_CLEAR_DATA, "清理数据", skin_info_ptr_->GetBitmap32(wxEmptyString));
	other_bar->SetButtonMaxSizeClass(ID_CLEAR_DATA, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	other_bar->SetButtonMinSizeClass(ID_CLEAR_DATA, wxRIBBON_BUTTONBAR_BUTTON_LARGE);*/
	//other_bar->AddButton(ID_HELP_ABOUT, "关于", skin_info_ptr_->GetBitmap32(wxEmptyString));
	//other_bar->SetButtonMaxSizeClass(ID_HELP_ABOUT, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
	//other_bar->SetButtonMinSizeClass(ID_HELP_ABOUT, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
}

void MyFrame::InnerUpdateStrategy()
{
	strategy_bar_->ClearButtons();
	/*strategy_bar->AddHybridButton(ID_STRATEGY, "趋势开仓A", skin_info_ptr_->GetBitmap32(wxEmptyString));
	strategy_bar->SetButtonMaxSizeClass(ID_STRATEGY, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	strategy_bar->SetButtonMinSizeClass(ID_STRATEGY, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	strategy_bar->AddHybridButton(ID_STRATEGY + 1, "策略平仓A", skin_info_ptr_->GetBitmap32(wxEmptyString));
	strategy_bar->SetButtonMaxSizeClass(ID_STRATEGY + 1, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	strategy_bar->SetButtonMinSizeClass(ID_STRATEGY + 1, wxRIBBON_BUTTONBAR_BUTTON_LARGE);*/
	bool test = false;
	for (size_t i = 0, j = all_strategy_func_.size(); i < j; i++)
	{
		zqdb::Calc::Func func(all_strategy_func_[i]);
		char buf[260] = { 0 };
		auto type = func.GetAttrAsStr("type", buf, 260);
		if (strcmp(type, "test") == 0) {
			test = true;
			continue;
		}
		auto name = func.GetAttrAsStr("name", buf, 260);
		auto calc_name = func.GetCalcName();
		wxString label = utf2wxString(name[0] ? name : calc_name);
		if (strstr(calc_name, "Algo")) {
			strategy_bar_->AddToggleButton(ID_STRATEGY + i, label, skin_info_ptr_->GetBitmap32(wxT("算法")));
		}
		else {
			strategy_bar_->AddToggleButton(ID_STRATEGY + i, label, skin_info_ptr_->GetBitmap32(wxT("策略")));
		}
		strategy_bar_->SetButtonMaxSizeClass(ID_STRATEGY + i, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
		strategy_bar_->SetButtonMinSizeClass(ID_STRATEGY + i, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
	}
	if (test) {
		for (size_t i = 0, j = all_strategy_func_.size(); i < j; i++)
		{
			zqdb::Calc::Func func(all_strategy_func_[i]);
			char buf[260] = { 0 };
			auto type = func.GetAttrAsStr("type", buf, 260);
			if (strcmp(type, "test") != 0) {
				continue;
			}
			auto name = func.GetAttrAsStr("name", buf, 260);
			auto calc_name = func.GetCalcName();
			wxString label = utf2wxString(name[0] ? name : calc_name);
			if (strstr(calc_name, "Algo")) {
				strategy_bar_->AddToggleButton(ID_STRATEGY + i, label, skin_info_ptr_->GetBitmap32(wxT("算法")));
			}
			else {
				strategy_bar_->AddToggleButton(ID_STRATEGY + i, label, skin_info_ptr_->GetBitmap32(wxT("策略")));
			}
			strategy_bar_->SetButtonMaxSizeClass(ID_STRATEGY + i, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
			strategy_bar_->SetButtonMinSizeClass(ID_STRATEGY + i, wxRIBBON_BUTTONBAR_BUTTON_LARGE);
		}
	}
}

void MyFrame::OnSkinInfoChanged()
{
	Base::OnSkinInfoChanged();
}

void MyFrame::OnInfoChanged()
{
	if (ZQDBIsTest()) {
		//提示运行策略，然后点击开始回测按钮
		ShowMessage(wxT("欢迎使用深度回测功能，请先添加交易测试账号、选择策略算法，然后再点击\"开始回测\"按钮进行回测！！！"));
	}

	auto h = Get();
	auto user = GetUser();
	if (!h) {
		/*auto key = wxGetApp().GetFrameLastKey();
		if (key.IsEmpty()) {
			key = wxT(SMARTKB_KEY_ALL);
		}
		auto keys = wxSplit(key, wxT('.'));
		if (keys.size() > 1) {
			if (keys.back() == wxT("StdContainer")) {
				bool show = false;
				switch (keys.size())
				{
				case 2: {
					auto h = ZQDBGetExchange(keys[0]);
					if (h) {
						zqdb::Exchange exchange(h);
						ShowContainer(utf2wxString(exchange->Name), zqdb::Calc::StdContainer(h));
						show = true;
					}
				} break;
				case 3: {
					auto h = ZQDBGetProduct(keys[0], keys[1]);
					if (h) {
						zqdb::Product product(h);
						ShowContainer(utf2wxString(product->Name), zqdb::Calc::StdContainer(h));
						show = true;
					}
				} break;
				}
				if (!show) {
					zqdb::Category cat;
					auto it = ++keys.rbegin();
					for (; it != keys.rend(); ++it)
					{
						cat = zqdb::Category(wxString2utf(*it).c_str(), cat);
						if (!cat) {
							break;
						}
					}
					if (cat && it == keys.rend()) {
						ShowCategory(cat);
					}
				}
			}
			else {
				zqdb::Calc::Func func(wxString2utf(keys.back()).c_str(), CALC_CONTAINER);
				if (func) {
					auto& utf_name = wxString2utf(keys[0]);
					char buf[260] = { 0 };
					auto name = func.GetAttrAsStr("name", buf, 260);
					if (strcmp(utf_name.c_str(), name) == 0 || strcmp(utf_name.c_str(), func.GetCalcName()) == 0) {
						zqdb::Calc::Container container(func.GetCalcName(), nullptr);
						ShowContainer(keys[0], container);
					}
					else {
						char buf[4096] = { 0 };
						boost::property_tree::ptree cfg;
						XUtil::json_from_str(func.GetAttrAsStrEx("", buf, 4096), cfg);
						auto opt_menu = cfg.get_child_optional("menu");
						if (opt_menu) {
							auto& cfg_menu = opt_menu.get();
							for (auto it = cfg_menu.begin(); it != cfg_menu.end(); ++it)
							{
								if (utf_name == it->second.get<std::string>("name")) {
									std::string str;
									XUtil::json_to_str(str, it->second.get_child("input"));
									zqdb::Calc::InputAttr input(str.c_str(), XUtil::XML_FLAG_JSON_STRING);
									zqdb::Calc::Container container(func.GetCalcName(), input);
									ShowContainer(keys[0], container);
									break;
								}
							}
						}
					}
				}
			}
		}
		else*/ {
			ShowKey(wxT(SMARTKB_KEY_ALL)/*key*/);
		}
	}

	/*auto sort_type = (MY_CODE_SORT_TYPE)wxGetApp().GetFrameLastSortType();
	auto sort = wxGetApp().GetFrameLastSort();
	if (sort) {
		switch (sort_type)
		{
		case SORT_ZDF:
			SortByZD(sort_type, 0, sort);
			break;
		case SORT_ZDS:
			SortByZD(sort_type, wxGetApp().GetSortQuick(), sort);
			break;
		case SORT_CALC_SORT:
			if (cur_sort_func_ < all_sort_func_.size()) {
				zqdb::Calc::Func func(all_sort_func_[cur_sort_func_]);
				if (wxGetApp().GetSortCalcFunc() == func.GetCalcName()) {
					zqdb::Calc::Sort calc(func.GetCalcName(), nullptr);
					SortByCalc(calc, sort);
				}
			}
			break;
		}
	}
	InnerUpdateSort();*/

	if (sys_bar_) {
		sys_bar_->ToggleButton(ID_CALC_TRADEFLAG, ZQDBGetCalcTradeFlag());
	}

	m_ribbon->Realize();

	if (h) {
		Goto(h);
		OnHandleChanged();
	}
	if (user) {
		GotoUser(user);
		OnUserChanged();
	}
}

void MyFrame::OnNotifyStatus(HZQDB h)
{
	switch (h->type)
	{
	case ZQDB_HANDLE_TYPE_SERVER: {
		if (sys_bar_)
			sys_bar_->ToggleButton(ID_DATA_LISTEN, ZQDBIsIPCServer() && ZQDBIsRPCServer());
	} break;
	case ZQDB_HANDLE_TYPE_MODULE: {
		if (ZQDBIsDisable(h)) {
			if (IsTest()) {
				StopTest();
			}
		}
		else {
			
		}
	} break;
	case ZQDB_HANDLE_TYPE_CALCULATOR: {
		zqdb::Calc::CalcBase calc(h);
		UpdateStrategy(wxGetApp().FindStrategy(calc.GetCalcName()));
	} break;
	}
	Base::OnNotifyStatus(h);
}

void MyFrame::OnNotifyUpdate(HZQDB h)
{
	if (h->type == ZQDB_HANDLE_TYPE_CALCFUNC) {
		zqdb::Calc::Func func(h);
		switch (func.GetCalcType())
		{
		case CALC_MAJOR: {

		} break;
		case CALC_MINOR: {

		} break;
		case CALC_CONTAINER: {
		} break;
		case CALC_FILTER: {
		} break;
		case CALC_SORT: {
		} break;
		case CALC_SCRIPT: {
		} break;
		case CALC_STRATEGY: {
			all_strategy_func_.Update();
			InnerUpdateStrategy();
			m_ribbon->Realize();
		} break;
		}
	}

	Base::OnNotifyUpdate(h);
}

void MyFrame::OnNavigateSetting(wxRibbonPanelEvent& evt)
{
	zqdb::TechDlg dlg(this);
	dlg.ShowModal();
}

void MyFrame::OnDataListen(wxRibbonButtonBarEvent& evt)
{
	if (ZQDBIsIPCServer() && ZQDBIsRPCServer()) {
		//ZQDBStopListen();
		ZQDBPauseServer(true, true);
	}
	else {
		//ZQDBStartListen();
		ZQDBRestartServer(true, true);
	}
}

void MyFrame::OnDataRecord(wxRibbonButtonBarEvent& WXUNUSED(evt))
{
	wxWindowDisabler disableAll;
	if (ZQDBIsRecording()) {
		wxBusyInfo info(wxT("停止录制中，请稍后..."), this);
		ZQDBStopRecord();
	}
	else {
		bool record = false;
		/*zqdb::AllExchange allexchange;
		if (allexchange.empty()) {
			if (wxNO == wxMessageBox(wxT("没有任何市场可用，建议等所有市场都可用时再录制数据。\n")
				wxT("取消录制吗？"), wxT("提示"), wxYES | wxNO)) {
				record = true;
			}
		}
		else if (ZQDBGetCalcAnyDisabledModule()) {
			if (wxNO == wxMessageBox(wxT("有模块不可用，建议等所有模块都可用时再录制数据。\n")
				wxT("取消录制吗？"), wxT("提示"), wxYES | wxNO)) {
				record = true;
			}
		}
		else */{
			record = true;
		}
		if (record) {
			wxBusyInfo info(wxT("准备录制中，请稍后..."), this);
			ZQDBStartRecord();
		}
	}
	if (sys_bar_) {
		sys_bar_->ToggleButton(ID_DATA_RECORD, ZQDBIsRecording() ? true : false);
		sys_bar_->SetButtonText(ID_DATA_RECORD, ZQDBIsRecording() ? wxT("录制中") : wxT("录制"));
		m_ribbon->Realize();
	}
}

void  MyFrame::UpdateTestInfo()
{
	if (ZQDBIsTest()) {
		if (sys_bar_) {
			sys_bar_->ToggleButton(ID_DATA_TEST, ZQDBIsTesting());
			sys_bar_->SetButtonText(ID_DATA_TEST, ZQDBIsTesting() ? wxT("回测中") : wxT("继续回测"));
			m_ribbon->Realize();
		}
		SetTitle(wxGetApp().GetAppTitle() + wxString(ZQDBIsTesting() ? wxT("") : wxT("[暂停]")));
	}
	else {
		if (sys_bar_) {
			wxString label = wxT("快速回放");
			switch (tech_view_->IsTest())
			{
			case ZQDB_CALC_TEST_DATA: {
				label = wxT("停止回放");
			} break;
			case ZQDB_CALC_TEST_TRADE: {
				label = wxT("停止回测");
			} break;
			default: {
			} break;
			}
			sys_bar_->SetButtonText(ID_DATA_TEST, label);
			m_ribbon->Realize();
		}
	}
}

void MyFrame::OnStartTest()
{
	tech_view_->StartTest(test_);
	switch (test_)
	{
	case ZQDB_CALC_TEST_DATA: {
	} break;
	case ZQDB_CALC_TEST_TRADE: {
		user_view_->StartTest(test_users_);
	} break;
	}
	test_timer_.Start(test_speed_);
	UpdateTestInfo();
}

void MyFrame::OnStopTest()
{
	if (tech_view_->IsTest())
		tech_view_->StopTest();
	if(user_view_->IsTest())
		user_view_->StopTest();
	test_timer_.Stop();
	UpdateTestInfo();
}

void MyFrame::OnTest(uint32_t date, uint32_t time)
{
	tech_view_->Test(date, time);
	switch (test_)
	{
	case ZQDB_CALC_TEST_DATA: {
	} break;
	case ZQDB_CALC_TEST_TRADE: {
		user_view_->Test(date, time);
	} break;
	}
	test_timer_.StartOnce();
}

void MyFrame::OnTestTimer(wxTimerEvent& event)
{
	auto type = IsTest();
	if (!type) {
		return;
	}
	for (auto huser : test_users_)
	{
		auto module = wxGetApp().FindModule(ZQDBGetModule(huser));
		module->ReqTestUserUpdate(huser, test_date_, test_time_);
	}
	if (!TestBase::Test()) {
		if (type != ZQDB_CALC_TEST_TRADE) {
			StopTest(); //交易回测不主动停止，等用户点击停止按钮
		}
	}
}

void MyFrame::Test(size_t type, bool showDlg)
{
	if (!ZQDBCalcIsTest(nullptr)) {
		switch (type)
		{
		case ZQDB_CALC_TEST_DATA: {
			auto calc_data = tech_view_->GetCalcData();
			if (calc_data) {
				auto pDate = (uint32_t*)calc_data->GetData(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, DATE));
				auto pTime = (uint32_t*)calc_data->GetData(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, TIME));
				if (showDlg) {
					MyTestDlg dlg(this, wxT("快速回放"), *pDate, *pTime);
					if (wxOK == dlg.ShowModal()) {
						auto date = dlg.GetDate();
						auto time = dlg.GetTime();
						auto speed = 1000.0 / dlg.GetSpeed();
						StartTest(ZQDB_CALC_TEST_DATA, date, time, speed);
					}
				}
				else {
					StartTest(ZQDB_CALC_TEST_DATA, *pDate, *pTime, 200);
				}
			}
		} break;
		case ZQDB_CALC_TEST_TRADE: {
			auto calc_data = tech_view_->GetCalcData();
			if (calc_data) {
				auto pDate = (uint32_t*)calc_data->GetData(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, DATE));
				auto pTime = (uint32_t*)calc_data->GetData(MDB_FIELD_TABLE_FIELD(ZQDB, KDATA, TIME));
				MyTradeTestDlg dlg(this, wxT("快速回测"), *pDate, *pTime);
				if (wxOK == dlg.ShowModal()) {
					auto date = dlg.GetDate();
					auto time = dlg.GetTime();
					auto speed = 1000.0 / dlg.GetSpeed();
					//初始化测试账户
					std::vector<HZQDB> users;
					double amount = dlg.GetAmount();
					wxGetApp().BroadcastModule([&users, amount](std::shared_ptr<zqdb::Module> module) -> bool {
						auto huser = module->ReqTestUser(amount);
						if (huser) {
							users.push_back(huser);
						}
						return false;
					});
					//创建策略
					std::vector<zqdb::Calc::Strategy> strategys;
					for (const auto& name : dlg.GetStrategy())
					{
						zqdb::Calc::Func func(wxString2utf(name).c_str(), CALC_STRATEGY);
						auto input = zqdb::GetCalcInputAttr(func);
						size_t target = zqdb::GetCalcTarget(func);
						HZQDB data = nullptr;
						PERIODTYPE cycle = zqdb::GetCalcCycle(func);
						size_t cycleex = 0;
						if (cycle == 0) {
							cycle = tech_view_->GetCycle();
							cycleex = tech_view_->GetCycleEx();
						}
						else {
							cycleex = zqdb::GetCalcCycleEx(func);
						}
						switch (target)
						{
						case 0: {
							//当前代码
							//data = Get();
							data = *tech_view_->GetCalcData();
						} break;
						case 1: {
							//当前列表
							data = code_view_->Cur();
						} break;
						case 2: {
							//策略指定代码
						} break;
						}
						strategys.emplace_back(name.c_str(), CALC_STRATEGY
							, data, cycle, cycleex, (HZQDB)input
							, users.data(), users.size()
							, CALCULATOR_FLAG_TEST);
					}
					//开始回测
					StartTest(ZQDB_CALC_TEST_TRADE, date, time, speed
						, users, strategys);
				}
			}
		} break;
		}
	}
	else if (IsTest()) {
		StopTest();
	}
	else {
		//
	}
}

void MyFrame::OnDataTest(wxRibbonButtonBarEvent& WXUNUSED(evt))
{
	if (ZQDBIsTest()) {
		if (ZQDBIsTesting()) {
			ZQDBPauseTesting();
		}
		else {
			HideMessage();
			ZQDBContinueTesting();
		}
		UpdateTestInfo();
	}
	else {
#if 1
		if (IsTest()) {
			Test(IsTest(), false);
		}
		else {
			Test(ZQDB_CALC_TEST_DATA, false);
		}
#else
		auto count = ZQDBGetRecordCount();
		if (!count) {
			wxMessageBox(wxT("请先录制数据!!!"), wxT("提示"), wxOK);
			return;
		}
		else if (count == 1 && ZQDBIsRecording()) {
			wxMessageBox(wxT("请先停止录制!!!"), wxT("提示"), wxOK);
			return;
		}
		MyDepthTestDlg dlg(this);
		if (wxOK == dlg.ShowModal()) {
			auto begin = dlg.GetBegin();
			auto end = dlg.GetEnd();
			auto speed = 1000.0 / dlg.GetSpeed();
			wxGetApp().Test(begin, end, speed);
		}
#endif//
	}
}

void MyFrame::OnDataTestDropdown(wxRibbonButtonBarEvent& evt)
{
	wxMenu menu;
	menu.Append(ID_DATA_TEST, wxT("快速回放"));
	menu.Append(ID_DATA_TRADE_TEST, wxT("快速回测"));
#ifndef mytraderpro
	menu.AppendSeparator();
	menu.AppendCheckItem(ID_DATA_DEPTH_RECORD, wxT("深度录制"));
	menu.Check(ID_DATA_DEPTH_RECORD, ZQDBIsRecording());
	menu.Append(ID_DATA_DEPTH_TEST, wxT("深度回测"));
#endif//
	evt.PopupMenu(&menu);
}

void MyFrame::OnDataTestTest(wxCommandEvent& evt)
{
	Test(ZQDB_CALC_TEST_DATA, true);
}

void MyFrame::OnDataTestTradeTest(wxCommandEvent& evt)
{
	Test(ZQDB_CALC_TEST_TRADE, true);
}

void MyFrame::OnDataTestDepthRecord(wxCommandEvent& evt)
{
	if (ZQDBIsRecording()) {
		wxBusyInfo info(wxT("停止录制中，请稍后..."), this);
		ZQDBStopRecord();
		wxSleep(1);
	}
	else {
		wxBusyInfo info(wxT("准备录制中，请稍后..."), this);
		ZQDBStartRecord();
		wxSleep(1);
	}
}

void MyFrame::OnDataTestDepthTest(wxCommandEvent& evt)
{
	MyDepthTestDlg dlg(this);
	if (wxOK == dlg.ShowModal()) {
		wxGetApp().Test(dlg.GetSpeed());
	}
}
//
//void MyFrame::OnDataTestRecord(wxCommandEvent& evt)
//{
//	size_t pos = evt.GetId() - ID_DATA_TEST_RECORD;
//	size_t date = ZQDBGetRecordDate(pos);
//	if (date) {
//		wxGetApp().Test(date, 0, 100);
//	}
//}

void MyFrame::OnCalcEdit(wxRibbonButtonBarEvent& WXUNUSED(evt))
{
	zqdb::TechDlg dlg(this);
	dlg.ShowModal();
	//wxGetApp().ShowCalcFrame();
}

void MyFrame::OnCalcEditDropdown(wxRibbonButtonBarEvent& evt)
{
	wxRibbonButtonBar* pBar = evt.GetBar();
	if (pBar) {
		wxMenu menu;
		menu.Append(ID_HELP_HOME, wxT("我要定制..."));
		wxMenu* sub_menu = new wxMenu;
		if (ZQDBIsRPC()) {
			sub_menu->Append(ID_DOWNLOAD_DATA, wxT("盘后数据下载"));
		}
#ifndef _DEBUG
		else
#endif
		{
			sub_menu->Append(ID_CLOSE_DATA, "强制收盘");
		}
		sub_menu->Append(ID_SAVE_DATA, wxT("保存数据"));
		sub_menu->Append(ID_CLEAR_DATA, wxT("清理数据"));
		if (!g_full) {
			sub_menu->Append(ID_CLEAR_SETTINGS, wxT("重新设置"));
		}
		sub_menu->AppendSeparator();
		sub_menu->Append(ID_RESTART, wxT("重启"));
		menu.AppendSubMenu(sub_menu, wxT("系统管理"));
		menu.AppendSeparator();
		zqdb::Calc::AllFunc all_major_func_(CALC_MAJOR);
		zqdb::Calc::AllFunc all_minor_func_(CALC_MINOR);
		sub_menu = new wxMenu;
		sub_menu->Append(ID_CALC_EDIT_NEW + CALC_MAJOR, wxT("新建"));
		sub_menu->AppendSeparator();
		for (size_t i = 0, j = 0; i < all_major_func_.size(); i++)
		{
			zqdb::Calc::Func func(all_major_func_[i]);
			if (func.GetCalcLang() != CALC_LANG_C_CPP) {
				auto sub_menu_item = sub_menu->Append(ID_CALC_EDIT_MAJOR + i, func.GetCalcName());
				zqdb::SetMenuItemClientData(sub_menu_item, func);
			}
		}
		menu.AppendSubMenu(sub_menu, wxT("主图指标"));
		sub_menu = new wxMenu;
		sub_menu->Append(ID_CALC_EDIT_NEW + CALC_MINOR, wxT("新建"));
		sub_menu->AppendSeparator();
		for (size_t i = 0, j = 0; i < all_minor_func_.size(); i++)
		{
			zqdb::Calc::Func func(all_minor_func_[i]);
			if (func.GetCalcLang() != CALC_LANG_C_CPP) {
				auto sub_menu_item = sub_menu->Append(ID_CALC_EDIT_MINOR + i, func.GetCalcName());
				zqdb::SetMenuItemClientData(sub_menu_item, func);
			}
		}
		menu.AppendSubMenu(sub_menu, wxT("副图指标"));
		sub_menu = new wxMenu;
		sub_menu->Append(ID_CALC_EDIT_NEW + CALC_CONTAINER, wxT("新建"));
		sub_menu->AppendSeparator();
		for (size_t i = 0, j = 0; i < all_container_func_.size(); i++)
		{
			zqdb::Calc::Func func(all_container_func_[i]);
			if (func.GetCalcLang() != CALC_LANG_C_CPP) {
				auto sub_menu_item = sub_menu->Append(ID_CALC_EDIT_CONTAINER + i, func.GetCalcName());
				zqdb::SetMenuItemClientData(sub_menu_item, func);
			}
		}
		menu.AppendSubMenu(sub_menu, wxT("板块算法"));
		sub_menu = new wxMenu;
		sub_menu->Append(ID_CALC_EDIT_NEW + CALC_FILTER, wxT("新建"));
		sub_menu->AppendSeparator();
		for (size_t i = 0, j = 0; i < all_filter_func_.size(); i++)
		{
			zqdb::Calc::Func func(all_filter_func_[i]);
			if (func.GetCalcLang() != CALC_LANG_C_CPP) {
				auto sub_menu_item = sub_menu->Append(ID_CALC_EDIT_FILTER + i, func.GetCalcName());
				zqdb::SetMenuItemClientData(sub_menu_item, func);
			}
		}
		menu.AppendSubMenu(sub_menu, wxT("筛选算法"));
#ifdef _SORT
		sub_menu = new wxMenu;
		sub_menu->Append(ID_CALC_EDIT_NEW + CALC_SORT, wxT("新建"));
		sub_menu->AppendSeparator();
		for (size_t i = 0, j = 0; i < all_sort_func_.size(); i++)
		{
			zqdb::Calc::Func func(all_sort_func_[i]);
			if (func.GetCalcLang() != CALC_LANG_C_CPP) {
				auto sub_menu_item = sub_menu->Append(ID_CALC_EDIT_SORT + i, func.GetCalcName());
				zqdb::SetMenuItemClientData(sub_menu_item, func);
			}
		}
		menu.AppendSubMenu(sub_menu, wxT("排序算法"));
#endif
#ifdef _TRADE
		sub_menu = new wxMenu;
		sub_menu->Append(ID_CALC_EDIT_NEW + CALC_SCRIPT, wxT("新建"));
		sub_menu->AppendSeparator();
		for (size_t i = 0, j = 0; i < all_script_func_.size(); i++)
		{
			zqdb::Calc::Func func(all_script_func_[i]);
			if (func.GetCalcLang() != CALC_LANG_C_CPP) {
				auto sub_menu_item = sub_menu->Append(ID_CALC_EDIT_SCRIPT + i, func.GetCalcName());
				zqdb::SetMenuItemClientData(sub_menu_item, func);
			}
		}
		menu.AppendSubMenu(sub_menu, wxT("快速脚本"));
		sub_menu = new wxMenu;
		sub_menu->Append(ID_CALC_EDIT_NEW + CALC_STRATEGY, wxT("新建"));
		sub_menu->AppendSeparator();
		for (size_t i = 0, j = 0; i < all_strategy_func_.size(); i++)
		{
			zqdb::Calc::Func func(all_strategy_func_[i]);
			if (func.GetCalcLang() != CALC_LANG_C_CPP) {
				auto sub_menu_item = sub_menu->Append(ID_CALC_EDIT_STRATEGY + i, func.GetCalcName());
				zqdb::SetMenuItemClientData(sub_menu_item, func);
			}
		}
		menu.AppendSubMenu(sub_menu, wxT("策略算法"));
#endif//
		evt.PopupMenu(&menu);
	}
}

void MyFrame::OnSysGoto(wxCommandEvent& evt)
{
	switch (evt.GetId())
	{
	case ID_HELP_HOME: {
		wxLaunchDefaultBrowser(wxT("https://gitee.com/7thTool/mytrader/blob/master/book/zh-cn/contact.md"));
	} break;
	case ID_CLEAR_SETTINGS: {
		wxGetApp().Resettings();
	} break;
	case ID_CLEAR_DATA: {
		wxGetApp().ClearData();
	} break;
	case ID_CLOSE_DATA: {
		wxGetApp().SaveClose();
	} break;
	case ID_SAVE_DATA: {
		wxGetApp().ExportData();
	} break;
	case ID_DOWNLOAD_DATA: {
		wxGetApp().DownloadData();
	} break;
	case ID_RESTART: {
		wxGetApp().Restart();
	} break;
	}
}

void MyFrame::OnCalcTradeFlag(wxRibbonButtonBarEvent& WXUNUSED(evt))
{
	ZQDBSetCalcTradeFlag(!ZQDBGetCalcTradeFlag());
}

void MyFrame::OnCalcSetting(wxRibbonPanelEvent& evt)
{
	zqdb::TechDlg dlg(this);
	dlg.ShowModal();
}

void MyFrame::UpdateStrategy(std::shared_ptr<zqdb::StrategyInfo> strategy)
{
	if (strategy) {
		if (!strategy->IsRun()) {
			wxGetApp().StopStrategy(strategy);
		}
		for (size_t pos = 0; pos < all_strategy_func_.size(); pos++) 
		{
			HZQDB h = all_strategy_func_[pos];
			zqdb::Calc::Func func(h); 
			std::string name = func.GetCalcName();
			if (name == strategy->name) {
				strategy_bar_->ToggleButton(ID_STRATEGY + pos, strategy->IsRun());
				break;
			}
		}
	}
}

void MyFrame::OnStrategy(wxRibbonButtonBarEvent& evt)
{
	auto pBar = evt.GetBar();
	size_t pos = evt.GetId() - ID_STRATEGY;
	if (pos < all_strategy_func_.size()) {
		HZQDB h = all_strategy_func_[pos];
		zqdb::Calc::Func func(h);
		std::string name = func.GetCalcName();
		auto strategy = wxGetApp().FindStrategy(name);
		if (!strategy) {
			auto huser = GetUser();
			if (!huser) {
				if (wxOK != wxMessageBox(wxT("没有登录交易账户!!!\n")
					wxT("确定继续运行策略算法吗？"), wxT("提示"), wxOK | wxCANCEL)) {
					pBar->ToggleButton(evt.GetId(), false);
					return;
				}
			}
			MyStrategyDlg dlg(this, CALC_STRATEGY, name.c_str());
			if (wxID_OK == dlg.ShowModal()) {
				auto input = zqdb::GetCalcInputAttr(h);
				size_t target = zqdb::GetCalcTarget(h);
				HZQDB data = nullptr;
				PERIODTYPE cycle = zqdb::GetCalcCycle(func);
				size_t cycleex = 0;
				if (cycle == 0) {
					cycle = tech_view_->GetCycle();
					cycleex = tech_view_->GetCycleEx();
				}
				else {
					cycleex = zqdb::GetCalcCycleEx(func);
				}
				switch (target) 
				{
				case 0: {
					//当前代码
					//data = Get();
					data = *tech_view_->GetCalcData();
				} break;
				case 1: {
					//当前列表
					data = code_view_->Cur();
				} break;
				case 2: {
					//策略指定代码
				} break;
				}
				strategy = wxGetApp().StartStrategy(h, input, data, cycle, cycleex, huser ? &huser : nullptr, huser ? 1 : 0);
			}
			else {
				pBar->ToggleButton(evt.GetId(), false);
			}
		}
		else {
			if (!strategy->IsRun()) {
				//strategy->Start();
			}
			else {
				if (wxOK == wxMessageBox(wxT("策略正在运行中...\n")
					wxT("确定停止策略吗？"), wxT("提示"), wxOK | wxCANCEL)) {
					wxGetApp().StopStrategy(strategy);
				}
			}
			pBar->ToggleButton(evt.GetId(), strategy->IsRun());
		}
	}
}

void MyFrame::OnStrategySetting(wxRibbonPanelEvent& evt)
{
	zqdb::TechDlg dlg(this, CALC_STRATEGY);
	dlg.ShowModal();
}