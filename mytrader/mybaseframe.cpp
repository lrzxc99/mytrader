#include "myapp.h"
#include "mybaseframe.h"
#include "mylogdlg.h"
#include "mysettingsdlg.h"
#include "wx/sysopt.h"
#include "wx/display.h"

///

BEGIN_EVENT_TABLE(MyTitleBar, wxPanel)
EVT_ERASE_BACKGROUND(MyTitleBar::OnErase)
EVT_PAINT(MyTitleBar::OnPaint)
EVT_SIZE(MyTitleBar::OnSize)
EVT_ENTER_WINDOW(MyTitleBar::OnEnterWindow)
EVT_LEAVE_WINDOW(MyTitleBar::OnLeaveWindow)
EVT_LEFT_DOWN(MyTitleBar::OnLeftDown)
EVT_LEFT_UP(MyTitleBar::OnLeftUp)
EVT_MOUSE_CAPTURE_LOST(MyTitleBar::OnMouseLost)
EVT_MOTION(MyTitleBar::OnMotion)
END_EVENT_TABLE()

MyTitleBar::MyTitleBar(wxWindow* parent) :wxPanel(parent, wxID_ANY
	, wxDefaultPosition, wxSize(30, 30), wxNO_BORDER)
{
}

MyTitleBar::~MyTitleBar()
{
}

void MyTitleBar::OnSkinInfoChanged()
{
	SetBackgroundColour(wxColor(25, 27, 31)/*skin_info_ptr_->crBackgnd*/);
	m_close_bmps[0] = skin_info_ptr_->GetBitmap(wxT("关闭_默认"));
	m_close_bmps[1] = skin_info_ptr_->GetBitmap(wxT("关闭_Hover"));
	Refresh();
}

void MyTitleBar::OnPaint(wxPaintEvent& e)
{
	int w, h;
	GetSize(&w, &h);
	if (w == 0 || h == 0)
	{
		return;
	}
	wxBufferedPaintDC dc(this);
	Paint(dc);
}

void MyTitleBar::Paint(wxDC& dc)
{
	int w, h;
	GetSize(&w, &h);
	if (w == 0 || h == 0)
	{
		return;
	}
	if (!IsDispOk()) {
		return;
	}
	dc.Clear();
	dc.DrawBitmap(m_close_bmps[m_close_status], wxPoint(m_close_rect.x, m_close_rect.y));

}

void MyTitleBar::OnSize(wxSizeEvent& e)
{
	auto sz = GetClientSize();
	m_close_rect = wxRect(sz.x - 30, 0, 30, 30);
}

void MyTitleBar::OnEnterWindow(wxMouseEvent& event)
{
	m_bInside = true;
}

void MyTitleBar::OnLeaveWindow(wxMouseEvent& event)
{
	m_bInside = false;
	m_close_status = 0;
	Refresh();
	//
	bool isDown = event.LeftIsDown();
	bool isDragging = event.Dragging();
	if (isDown && isDragging) {
		wxCommandEvent ev(wxEVT_MOTION, GetId());
		ev.SetEventObject(this);
		GetEventHandler()->ProcessEvent(ev);
	}
}

void MyTitleBar::OnMotion(wxMouseEvent& event)
{
	wxPoint pt = event.GetPosition();
	bool isDown = event.LeftIsDown();

	if (isDown && event.Dragging() && HasCapture()) {
		wxPoint mouse_pos = ClientToScreen(pt);
		if (m_offset.x != -1) {
			wxPoint dp = mouse_pos - m_offset;
			GetParent()->Move(dp);
			return;
		}
	}
	auto old_close_status = m_close_status;
	m_close_status = m_close_rect.Contains(pt) ? 1 : 0;
	if (old_close_status != m_close_status) {
		RefreshRect(m_close_rect);
	}
}

void MyTitleBar::OnLeftDown(wxMouseEvent& event)
{
	wxPoint pt = event.GetPosition();
	if (m_close_rect.Contains(pt)) {
		m_close_status = 1;
		RefreshRect(m_close_rect);
		return;
	}

	if (!HasCapture()) {
		CaptureMouse();
	}

	wxPoint mouse_pos = ClientToScreen(pt);
	wxPoint wnd_pos = ClientToScreen(this->GetPosition());
	m_offset.x = mouse_pos.x - wnd_pos.x;
	m_offset.y = mouse_pos.y - wnd_pos.y;
}

void MyTitleBar::OnLeftUp(wxMouseEvent& event)
{
	m_offset = wxPoint(-1, -1);
	if (HasCapture()) {
		ReleaseMouse();
	}

	wxPoint pt = event.GetPosition();
	if (m_close_rect.Contains(pt)) {
		m_close_status = 0;
		wxGetTopLevelParent(this)->Close();
	}
}

void MyTitleBar::OnMouseLost(wxMouseCaptureLostEvent& event)
{
	m_offset = wxPoint(-1, -1);
	if (HasCapture()) {
		ReleaseMouse();
	}
}

///

MyRibbonMSWArtProvider::MyRibbonMSWArtProvider(SkinStyle style)
	:Base(style == SKIN_NONE), style_(style)
{
	switch (style)
	{
	case SKIN_BLACK: {
		SetColourScheme(
			wxColour(49, 56, 74),
			wxColour(78, 83, 94),
			wxColour(255, 255, 255));
		/*skin_info_ptr->crPrimary.Set(49, 56, 74);
		skin_info_ptr->crSecondary.Set(78, 83, 94);
		skin_info_ptr->crTertiary.Set(159, 166, 187);
		skin_info_ptr->crViewBkgnd.Set(201, 217, 237);
		skin_info_ptr->crCtrlBkgnd.Set(39, 42, 53);*/
		SetColor(wxRIBBON_ART_TAB_CTRL_BACKGROUND_COLOUR
			, m_primary_scheme_colour);
		SetColor(wxRIBBON_ART_TAB_CTRL_BACKGROUND_GRADIENT_COLOUR
			, m_primary_scheme_colour);
		//SetColor(wxRIBBON_ART_TAB_SEPARATOR_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TAB_SEPARATOR_GRADIENT_COLOUR
			, GetColor(wxRIBBON_ART_TAB_SEPARATOR_COLOUR));
		//SetColor(wxRIBBON_ART_TAB_HOVER_BACKGROUND_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TAB_HOVER_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TAB_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TAB_HOVER_BACKGROUND_TOP_COLOUR, GetColor(wxRIBBON_ART_TAB_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TAB_HOVER_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TAB_HOVER_BACKGROUND_COLOUR));
		//SetColor(wxRIBBON_ART_TAB_ACTIVE_BACKGROUND_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TAB_ACTIVE_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TAB_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TAB_ACTIVE_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TAB_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TAB_ACTIVE_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TAB_ACTIVE_BACKGROUND_COLOUR));
		//SetColor(wxRIBBON_ART_TAB_BORDER_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));

		//SetColor(wxRIBBON_ART_PAGE_BORDER_COLOUR, GetColor(wxRIBBON_ART_TAB_SEPARATOR_COLOUR));
		SetColor(wxRIBBON_ART_PAGE_BACKGROUND_TOP_COLOUR
			, m_secondary_scheme_colour);
		SetColor(wxRIBBON_ART_PAGE_BACKGROUND_TOP_GRADIENT_COLOUR
			, m_secondary_scheme_colour);
		SetColor(wxRIBBON_ART_PAGE_HOVER_BACKGROUND_TOP_GRADIENT_COLOUR
			, GetColor(wxRIBBON_ART_PAGE_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR
			, m_primary_scheme_colour);
		SetColor(wxRIBBON_ART_PAGE_BACKGROUND_GRADIENT_COLOUR
			, m_primary_scheme_colour);
		SetColor(wxRIBBON_ART_PAGE_HOVER_BACKGROUND_GRADIENT_COLOUR
			, GetColor(wxRIBBON_ART_PAGE_HOVER_BACKGROUND_COLOUR));

		SetColor(wxRIBBON_ART_PANEL_BORDER_COLOUR
			, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_BORDER_GRADIENT_COLOUR
			, GetColor(wxRIBBON_ART_PANEL_BORDER_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_HOVER_BORDER_GRADIENT_COLOUR
			, GetColor(wxRIBBON_ART_PANEL_HOVER_BORDER_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_MINIMISED_BORDER_GRADIENT_COLOUR
			, GetColor(wxRIBBON_ART_PANEL_MINIMISED_BORDER_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_LABEL_BACKGROUND_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_LABEL_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_PANEL_LABEL_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_HOVER_LABEL_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_PANEL_HOVER_LABEL_BACKGROUND_COLOUR));
		//SetColor(wxRIBBON_ART_PANEL_ACTIVE_BACKGROUND_COLOUR, GetColor(wxRIBBON_ART_PANEL_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_ACTIVE_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_PANEL_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_ACTIVE_BACKGROUND_TOP_COLOUR, GetColor(wxRIBBON_ART_PANEL_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_PANEL_ACTIVE_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_PANEL_ACTIVE_BACKGROUND_COLOUR));
		//SetColor(wxRIBBON_ART_PANEL_BUTTON_FACE_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));

		SetColor(wxRIBBON_ART_BUTTON_BAR_HOVER_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_BUTTON_BAR_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_BUTTON_BAR_HOVER_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_BUTTON_BAR_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_BUTTON_BAR_ACTIVE_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_BUTTON_BAR_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_BUTTON_BAR_ACTIVE_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_BUTTON_BAR_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_BUTTON_BAR_LABEL_HIGHLIGHT_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_BUTTON_BAR_LABEL_HIGHLIGHT_COLOUR));
		SetColor(wxRIBBON_ART_BUTTON_BAR_LABEL_HIGHLIGHT_TOP_COLOUR, GetColor(wxRIBBON_ART_BUTTON_BAR_LABEL_HIGHLIGHT_COLOUR));
		SetColor(wxRIBBON_ART_BUTTON_BAR_LABEL_HIGHLIGHT_GRADIENT_TOP_COLOUR, GetColor(wxRIBBON_ART_BUTTON_BAR_LABEL_HIGHLIGHT_COLOUR));

		SetColor(wxRIBBON_ART_TOOLBAR_BORDER_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));
		//SetColor(wxRIBBON_ART_TOOLBAR_FACE_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_BACKGROUND_COLOUR, GetColor(wxRIBBON_ART_PAGE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TOOL_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_BACKGROUND_TOP_COLOUR, GetColor(wxRIBBON_ART_TOOL_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TOOL_BACKGROUND_COLOUR));
		//SetColor(wxRIBBON_ART_TOOL_HOVER_BACKGROUND_COLOUR, GetColor(wxRIBBON_ART_TOOL_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_HOVER_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TOOL_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_HOVER_BACKGROUND_TOP_COLOUR, GetColor(wxRIBBON_ART_TOOL_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_HOVER_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TOOL_HOVER_BACKGROUND_COLOUR));
		//SetColor(wxRIBBON_ART_TOOL_ACTIVE_BACKGROUND_COLOUR, GetColor(wxRIBBON_ART_TOOL_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_ACTIVE_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TOOL_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_ACTIVE_BACKGROUND_TOP_COLOUR, GetColor(wxRIBBON_ART_TOOL_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_TOOL_ACTIVE_BACKGROUND_TOP_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_TOOL_ACTIVE_BACKGROUND_COLOUR));

		SetColor(wxRIBBON_ART_GALLERY_BUTTON_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_GALLERY_BUTTON_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_GALLERY_BUTTON_HOVER_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_GALLERY_BUTTON_HOVER_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_GALLERY_BUTTON_ACTIVE_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_GALLERY_BUTTON_ACTIVE_BACKGROUND_COLOUR));
		SetColor(wxRIBBON_ART_GALLERY_BUTTON_DISABLED_BACKGROUND_GRADIENT_COLOUR, GetColor(wxRIBBON_ART_GALLERY_BUTTON_DISABLED_BACKGROUND_COLOUR));

	} break;
	case SKIN_WHITE: {
		SetColourScheme(
			wxColour(194, 216, 241),
			wxColour(255, 223, 114),
			wxColour(0, 0, 0));
	} break;
	}
}

wxRibbonArtProvider* MyRibbonMSWArtProvider::Clone() const
{
	MyRibbonMSWArtProvider *copy = new MyRibbonMSWArtProvider;
	CloneTo(copy);
	return copy;
}

void MyRibbonMSWArtProvider::CloneTo(MyRibbonMSWArtProvider* copy) const
{
	Base::CloneTo(copy);
	//
	copy->m_tool_face_colour = m_tool_face_colour;
	copy->m_tool_background_top_colour = m_tool_background_top_colour;
	copy->m_tool_background_top_gradient_colour = m_tool_background_top_gradient_colour;
	copy->m_tool_background_colour = m_tool_background_colour;
	copy->m_tool_background_gradient_colour = m_tool_background_gradient_colour;
	copy->m_tool_hover_background_top_colour = m_tool_hover_background_top_colour;
	copy->m_tool_hover_background_top_gradient_colour = m_tool_hover_background_top_gradient_colour;
	copy->m_tool_hover_background_colour = m_tool_hover_background_colour;
	copy->m_tool_hover_background_gradient_colour = m_tool_hover_background_gradient_colour;
	copy->m_tool_active_background_top_colour = m_tool_active_background_top_colour;
	copy->m_tool_active_background_top_gradient_colour = m_tool_active_background_top_gradient_colour;
	copy->m_tool_active_background_colour = m_tool_active_background_colour;
	copy->m_tool_active_background_gradient_colour = m_tool_active_background_gradient_colour;
	//
	copy->style_ = style_;
}

void MyRibbonMSWArtProvider::SetColourScheme(const wxColour& primary,
	const wxColour& secondary,
	const wxColour& tertiary)
{
	Base::SetColourScheme(primary, secondary, tertiary);
}

void MyRibbonMSWArtProvider::DrawTool(
	wxDC& dc,
	wxWindow* WXUNUSED(wnd),
	const wxRect& rect,
	const wxBitmap& bitmap,
	wxRibbonButtonKind kind,
	long state)
{
	if (kind == wxRIBBON_BUTTON_TOGGLE)
	{
		if (state & wxRIBBON_TOOLBAR_TOOL_TOGGLED)
			state ^= wxRIBBON_TOOLBAR_TOOL_ACTIVE_MASK;
	}

	wxRect bg_rect(rect);
	bg_rect.Deflate(1);
	if ((state & wxRIBBON_TOOLBAR_TOOL_LAST) == 0)
		bg_rect.width++;
	bool is_split_hybrid = (kind == wxRIBBON_BUTTON_HYBRID && (state &
		(wxRIBBON_TOOLBAR_TOOL_HOVER_MASK | wxRIBBON_TOOLBAR_TOOL_ACTIVE_MASK)));

	// Background
	wxRect bg_rect_top(bg_rect);
	bg_rect_top.height = (bg_rect_top.height * 2) / 5;
	wxRect bg_rect_btm(bg_rect);
	bg_rect_btm.y += bg_rect_top.height;
	bg_rect_btm.height -= bg_rect_top.height;
	wxColour bg_top_colour = m_tool_background_top_colour;
	wxColour bg_top_grad_colour = m_tool_background_top_gradient_colour;
	wxColour bg_colour = m_tool_background_colour;
	wxColour bg_grad_colour = m_tool_background_gradient_colour;
	if (state & wxRIBBON_TOOLBAR_TOOL_ACTIVE_MASK)
	{
		bg_top_colour = m_tool_active_background_top_colour;
		bg_top_grad_colour = m_tool_active_background_top_gradient_colour;
		bg_colour = m_tool_active_background_colour;
		bg_grad_colour = m_tool_active_background_gradient_colour;
	}
	else if (state & wxRIBBON_TOOLBAR_TOOL_HOVER_MASK)
	{
		bg_top_colour = m_tool_hover_background_top_colour;
		bg_top_grad_colour = m_tool_hover_background_top_gradient_colour;
		bg_colour = m_tool_hover_background_colour;
		bg_grad_colour = m_tool_hover_background_gradient_colour;
	}
	dc.GradientFillLinear(bg_rect_top, bg_top_colour, bg_top_grad_colour, wxSOUTH);
	dc.GradientFillLinear(bg_rect_btm, bg_colour, bg_grad_colour, wxSOUTH);
	if (is_split_hybrid)
	{
		wxRect nonrect(bg_rect);
		if (state & (wxRIBBON_TOOLBAR_TOOL_DROPDOWN_HOVERED |
			wxRIBBON_TOOLBAR_TOOL_DROPDOWN_ACTIVE))
		{
			nonrect.width -= 8;
		}
		else
		{
			nonrect.x += nonrect.width - 8;
			nonrect.width = 8;
		}
		wxBrush B(m_tool_hover_background_top_colour);
		dc.SetPen(*wxTRANSPARENT_PEN);
		dc.SetBrush(B);
		dc.DrawRectangle(nonrect.x, nonrect.y, nonrect.width, nonrect.height);
	}

	// Border
	dc.SetPen(m_toolbar_border_pen);
	if (state & wxRIBBON_TOOLBAR_TOOL_FIRST)
	{
		dc.DrawPoint(rect.x + 1, rect.y + 1);
		dc.DrawPoint(rect.x + 1, rect.y + rect.height - 2);
	}
	else
		dc.DrawLine(rect.x, rect.y + 1, rect.x, rect.y + rect.height - 1);

	if (state & wxRIBBON_TOOLBAR_TOOL_LAST)
	{
		dc.DrawPoint(rect.x + rect.width - 2, rect.y + 1);
		dc.DrawPoint(rect.x + rect.width - 2, rect.y + rect.height - 2);
	}

	// Foreground
	int avail_width = bg_rect.GetWidth();
	if (kind & wxRIBBON_BUTTON_DROPDOWN)
	{
		avail_width -= 8;
		if (is_split_hybrid)
		{
			dc.DrawLine(rect.x + avail_width + 1, rect.y,
				rect.x + avail_width + 1, rect.y + rect.height);
		}
		dc.DrawBitmap(m_toolbar_drop_bitmap, bg_rect.x + avail_width + 2,
			bg_rect.y + (bg_rect.height / 2) - 2, true);
	}
	auto bmp_size = bitmap.GetScaledSize();
	if (avail_width > bmp_size.x) {
		dc.DrawBitmap(bitmap, bg_rect.x + (avail_width - bmp_size.x) / 2,
			bg_rect.y + (bg_rect.height - bmp_size.y) / 2, true);
	}
	else {
		dc.DrawBitmap(bitmap, bg_rect.x,
			bg_rect.y + (bg_rect.height - bmp_size.y) / 2, true);
	}
}

///

MyRibbonBar::MyRibbonBar(wxWindow* parent,
	wxWindowID id,
	const wxPoint& pos,
	const wxSize& size,
	long style) 
	: Base(parent, id, pos, size, style)
	, icon_(wxT("mytrader"), wxBITMAP_TYPE_ICO_RESOURCE, 16, 16)
{
	m_tab_margin_left = 300;
	m_tab_margin_right += 32 * 3 + 1;
}

wxBEGIN_EVENT_TABLE(MyRibbonBar, Base)
EVT_PAINT(MyRibbonBar::OnPaint)
EVT_SIZE(MyRibbonBar::OnSize)
EVT_MOTION(MyRibbonBar::OnMouseMove)
EVT_LEAVE_WINDOW(MyRibbonBar::OnMouseLeave)
EVT_MOUSE_CAPTURE_LOST(MyRibbonBar::OnMouseLost)
EVT_LEFT_DOWN(MyRibbonBar::OnMouseLeftDown)
EVT_LEFT_UP(MyRibbonBar::OnMouseLeftUp)
//EVT_MIDDLE_DOWN(MyRibbonBar::OnMouseMiddleDown)
//EVT_MIDDLE_UP(MyRibbonBar::OnMouseMiddleUp)
//EVT_RIGHT_DOWN(MyRibbonBar::OnMouseRightDown)
//EVT_RIGHT_UP(MyRibbonBar::OnMouseRightUp)
EVT_LEFT_DCLICK(MyRibbonBar::OnMouseDoubleClick)
wxEND_EVENT_TABLE()

void MyRibbonBar::OnPaint(wxPaintEvent& WXUNUSED(evt))
{
	wxAutoBufferedPaintDC dc(this);

	if (GetUpdateRegion().Contains(0, 0, GetClientSize().GetWidth(), m_tab_height) == wxOutRegion)
	{
		// Nothing to do in the tab area, and the page area is handled by the active page
		return;
	}

	DoEraseBackground(dc);

	wxRendererNative& renderer = wxRendererNative::GetDefault();
	auto frame = (wxFrame*)wxGetTopLevelParent(this);
	dc.DrawIcon(icon_, 1, 4);
	dc.DrawText(frame->GetTitle(), 1 + 16 + 1, 3);
	
	close_button_rect_ = wxRect(GetClientSize().GetWidth() - 32 - 1, 4, 30, 16);
	max_button_rect_ = close_button_rect_; max_button_rect_.Offset(-32, 0);
	min_button_rect_ = max_button_rect_; min_button_rect_.Offset(-32, 0);
	if (m_flags & wxRIBBON_BAR_SHOW_HELP_BUTTON) {
		m_help_button_rect = m_art->GetRibbonHelpButtonArea(GetSize());
		m_help_button_rect.Offset(-32 * 3, 0);
	}
	if (m_flags & wxRIBBON_BAR_SHOW_TOGGLE_BUTTON) {
		m_toggle_button_rect = m_art->GetBarToggleButtonArea(GetSize());
		m_toggle_button_rect.Offset(-32 * 3, 0);
	}

	size_t numtabs = m_pages.GetCount();
	double sep_visibility = 0.0;
	bool draw_sep = false;
	wxRect tabs_rect(m_tab_margin_left, 0, GetClientSize().GetWidth() - m_tab_margin_left - m_tab_margin_right, m_tab_height);
	if (m_tab_scroll_buttons_shown)
	{
		tabs_rect.x += m_tab_scroll_left_button_rect.GetWidth();
		tabs_rect.width -= m_tab_scroll_left_button_rect.GetWidth() + m_tab_scroll_right_button_rect.GetWidth();
	}
	size_t i;
	for (i = 0; i < numtabs; ++i)
	{
		wxRibbonPageTabInfo& info = m_pages.Item(i);
		if (!info.shown)
			continue;

		dc.DestroyClippingRegion();
		if (m_tab_scroll_buttons_shown)
		{
			if (!tabs_rect.Intersects(info.rect))
				continue;
			dc.SetClippingRegion(tabs_rect);
		}
		dc.SetClippingRegion(info.rect);
		m_art->DrawTab(dc, this, info);

		if (info.rect.width < info.small_begin_need_separator_width)
		{
			draw_sep = true;
			if (info.rect.width < info.small_must_have_separator_width)
			{
				sep_visibility += 1.0;
			}
			else
			{
				sep_visibility += (double)(info.small_begin_need_separator_width - info.rect.width) / (double)(info.small_begin_need_separator_width - info.small_must_have_separator_width);
			}
		}
	}
	if (draw_sep)
	{
		wxRect rect = m_pages.Item(0).rect;
		rect.width = m_art->GetMetric(wxRIBBON_ART_TAB_SEPARATION_SIZE);
		sep_visibility /= (double)numtabs;
		for (i = 0; i < numtabs - 1; ++i)
		{
			wxRibbonPageTabInfo& info = m_pages.Item(i);
			if (!info.shown)
				continue;
			rect.x = info.rect.x + info.rect.width;

			if (m_tab_scroll_buttons_shown && !tabs_rect.Intersects(rect))
			{
				continue;
			}

			dc.DestroyClippingRegion();
			dc.SetClippingRegion(rect);
			m_art->DrawTabSeparator(dc, this, rect, sep_visibility);
		}
	}
	if (m_tab_scroll_buttons_shown)
	{
		if (m_tab_scroll_left_button_rect.GetWidth() != 0)
		{
			dc.DestroyClippingRegion();
			dc.SetClippingRegion(m_tab_scroll_left_button_rect);
			m_art->DrawScrollButton(dc, this, m_tab_scroll_left_button_rect, wxRIBBON_SCROLL_BTN_LEFT | m_tab_scroll_left_button_state | wxRIBBON_SCROLL_BTN_FOR_TABS);
		}
		if (m_tab_scroll_right_button_rect.GetWidth() != 0)
		{
			dc.DestroyClippingRegion();
			dc.SetClippingRegion(m_tab_scroll_right_button_rect);
			m_art->DrawScrollButton(dc, this, m_tab_scroll_right_button_rect, wxRIBBON_SCROLL_BTN_RIGHT | m_tab_scroll_right_button_state | wxRIBBON_SCROLL_BTN_FOR_TABS);
		}
	}

	if (m_flags & wxRIBBON_BAR_SHOW_HELP_BUTTON)
		m_art->DrawHelpButton(dc, this, m_help_button_rect);
	if (m_flags & wxRIBBON_BAR_SHOW_TOGGLE_BUTTON)
		m_art->DrawToggleButton(dc, this, m_toggle_button_rect, m_ribbon_state);
	
	dc.DestroyClippingRegion();
	dc.SetClippingRegion(close_button_rect_);
	renderer.DrawTitleBarBitmap(this, dc, close_button_rect_
		, wxTITLEBAR_BUTTON_CLOSE
		, close_button_hovered_ ? wxCONTROL_CURRENT : wxCONTROL_NONE);

	dc.DestroyClippingRegion();
	dc.SetClippingRegion(max_button_rect_);
	renderer.DrawTitleBarBitmap(this, dc, max_button_rect_
		, frame->IsMaximized() ? wxTITLEBAR_BUTTON_RESTORE : wxTITLEBAR_BUTTON_MAXIMIZE
		, max_button_hovered_ ? wxCONTROL_CURRENT : wxCONTROL_NONE);
	
	dc.DestroyClippingRegion();
	dc.SetClippingRegion(min_button_rect_);
	renderer.DrawTitleBarBitmap(this, dc, min_button_rect_
		, wxTITLEBAR_BUTTON_ICONIZE
		, min_button_hovered_ ? wxCONTROL_CURRENT : wxCONTROL_NONE);
}

void MyRibbonBar::OnSize(wxSizeEvent& evt)
{
	evt.Skip();
	/*if (titlebar_) {
		auto rcClient = GetClientRect();
		auto szTitleBar = titlebar_->GetSize();
		titlebar_->Move(rcClient.GetRight() - szTitleBar.x, rcClient.y);
	}*/
}

void MyRibbonBar::OnMouseMove(wxMouseEvent& evt)
{
	if (evt.LeftIsDown() && evt.Dragging() && HasCapture()) {
		wxPoint mouse_pos = ClientToScreen(evt.GetPosition());
		if (offset_.x != -1) {
			wxPoint dp = mouse_pos - offset_;
			wxGetTopLevelParent(this)->Move(dp);
			return;
		}
	}

	int x = evt.GetX();
	int y = evt.GetY();
	int hovered_page = -1;
	bool refresh_tabs = false;
	if (y < m_tab_height)
	{
		// It is quite likely that the mouse moved a small amount and is still over the same tab
		if (m_current_hovered_page != -1 && m_pages.Item((size_t)m_current_hovered_page).rect.Contains(x, y))
		{
			hovered_page = m_current_hovered_page;
			// But be careful, if tabs can be scrolled, then parts of the tab rect may not be valid
			if (m_tab_scroll_buttons_shown)
			{
				if (x >= m_tab_scroll_right_button_rect.GetX() || x < m_tab_scroll_left_button_rect.GetRight())
				{
					hovered_page = -1;
				}
			}
		}
		else
		{
			HitTestTabs(evt.GetPosition(), &hovered_page);
		}
	}
	if (hovered_page != m_current_hovered_page)
	{
		if (m_current_hovered_page != -1)
		{
			m_pages.Item((int)m_current_hovered_page).hovered = false;
		}
		m_current_hovered_page = hovered_page;
		if (m_current_hovered_page != -1)
		{
			m_pages.Item((int)m_current_hovered_page).hovered = true;
		}
		refresh_tabs = true;
	}
	if (m_tab_scroll_buttons_shown)
	{
#define SET_FLAG(variable, flag) \
    { if(((variable) & (flag)) != (flag)) { variable |= (flag); refresh_tabs = true; }}
#define UNSET_FLAG(variable, flag) \
    { if((variable) & (flag)) { variable &= ~(flag); refresh_tabs = true; }}

		if (m_tab_scroll_left_button_rect.Contains(x, y))
			SET_FLAG(m_tab_scroll_left_button_state, wxRIBBON_SCROLL_BTN_HOVERED)
		else
			UNSET_FLAG(m_tab_scroll_left_button_state, wxRIBBON_SCROLL_BTN_HOVERED)

			if (m_tab_scroll_right_button_rect.Contains(x, y))
				SET_FLAG(m_tab_scroll_right_button_state, wxRIBBON_SCROLL_BTN_HOVERED)
			else
				UNSET_FLAG(m_tab_scroll_right_button_state, wxRIBBON_SCROLL_BTN_HOVERED)
#undef SET_FLAG
#undef UNSET_FLAG
	}
	if (refresh_tabs)
	{
		RefreshTabBar();
	}
	if (m_flags & wxRIBBON_BAR_SHOW_TOGGLE_BUTTON)
		HitTestRibbonButton(m_toggle_button_rect, evt.GetPosition(), m_toggle_button_hovered);
	if (m_flags & wxRIBBON_BAR_SHOW_HELP_BUTTON)
		HitTestRibbonButton(m_help_button_rect, evt.GetPosition(), m_help_button_hovered);

	if (m_bar_hovered) {
		if (m_toggle_button_hovered || m_help_button_hovered) {
			if (min_button_hovered_ || max_button_hovered_ || close_button_hovered_) {
				min_button_hovered_ = false;
				max_button_hovered_ = false;
				close_button_hovered_ = false;
				Refresh(false);
			}
		}
		else {
			auto pt = evt.GetPosition();
			if (min_button_rect_.Contains(pt)) {
				if (!min_button_hovered_) {
					min_button_hovered_ = true;
					max_button_hovered_ = false;
					close_button_hovered_ = false;
					Refresh(false);
				}
			}
			else if (max_button_rect_.Contains(pt)) {
				if (!max_button_hovered_) {
					min_button_hovered_ = false;
					max_button_hovered_ = true;
					close_button_hovered_ = false;
					Refresh(false);
				}
			}
			else if (close_button_rect_.Contains(pt)) {
				if (!close_button_hovered_) {
					min_button_hovered_ = false;
					max_button_hovered_ = false;
					close_button_hovered_ = true;
					Refresh(false);
				}
			} else if (min_button_hovered_ || max_button_hovered_ || close_button_hovered_) {
				min_button_hovered_ = false;
				max_button_hovered_ = false;
				close_button_hovered_ = false;
				Refresh(false);
			}
		}
	}
}

void MyRibbonBar::OnMouseLost(wxMouseCaptureLostEvent& evt)
{
	offset_ = wxPoint(-1, -1);
	if (HasCapture()) {
		ReleaseMouse();
	}
}

void MyRibbonBar::OnMouseLeave(wxMouseEvent& WXUNUSED(evt))
{
	// The ribbon bar is (usually) at the top of a window, and at least on MSW, the mouse
	// can leave the window quickly and leave a tab in the hovered state.
	bool refresh_tabs = false;
	if (m_current_hovered_page != -1)
	{
		m_pages.Item((int)m_current_hovered_page).hovered = false;
		m_current_hovered_page = -1;
		refresh_tabs = true;
	}
	if (m_tab_scroll_left_button_state & wxRIBBON_SCROLL_BTN_HOVERED)
	{
		m_tab_scroll_left_button_state &= ~wxRIBBON_SCROLL_BTN_HOVERED;
		refresh_tabs = true;
	}
	if (m_tab_scroll_right_button_state & wxRIBBON_SCROLL_BTN_HOVERED)
	{
		m_tab_scroll_right_button_state &= ~wxRIBBON_SCROLL_BTN_HOVERED;
		refresh_tabs = true;
	}
	if (refresh_tabs)
	{
		RefreshTabBar();
	}
	if (m_toggle_button_hovered || m_help_button_hovered
		|| min_button_hovered_ || max_button_hovered_ || close_button_hovered_
		) {
		m_bar_hovered = false;
		m_toggle_button_hovered = false;
		m_help_button_hovered = false;
		min_button_hovered_ = false;
		max_button_hovered_ = false;
		close_button_hovered_ = false;
		Refresh(false);
	}
}

void MyRibbonBar::OnMouseLeftDown(wxMouseEvent& evt)
{
	wxRibbonPageTabInfo *tab = HitTestTabs(evt.GetPosition());
	SetFocus();
	if (tab)
	{
		if (m_ribbon_state == wxRIBBON_BAR_MINIMIZED)
		{
			ShowPanels(wxRIBBON_BAR_EXPANDED);
		}
		else if ((tab == &m_pages.Item(m_current_page)) && (m_ribbon_state == wxRIBBON_BAR_EXPANDED))
		{
			HidePanels();
		}
	}
	else
	{
		if (m_ribbon_state == wxRIBBON_BAR_EXPANDED)
		{
			HidePanels();
		}
	}
	if (tab && tab != &m_pages.Item(m_current_page))
	{
		wxRibbonBarEvent query(wxEVT_RIBBONBAR_PAGE_CHANGING, GetId(), tab->page);
		query.SetEventObject(this);
		ProcessWindowEvent(query);
		if (query.IsAllowed())
		{
			SetActivePage(query.GetPage());

			wxRibbonBarEvent notification(wxEVT_RIBBONBAR_PAGE_CHANGED, GetId(), m_pages.Item(m_current_page).page);
			notification.SetEventObject(this);
			ProcessWindowEvent(notification);
		}
		return;
	}
	else if (tab == NULL)
	{
		if (m_tab_scroll_left_button_rect.Contains(evt.GetPosition()))
		{
			m_tab_scroll_left_button_state |= wxRIBBON_SCROLL_BTN_ACTIVE | wxRIBBON_SCROLL_BTN_HOVERED;
			RefreshTabBar();
			return;
		}
		else if (m_tab_scroll_right_button_rect.Contains(evt.GetPosition()))
		{
			m_tab_scroll_right_button_state |= wxRIBBON_SCROLL_BTN_ACTIVE | wxRIBBON_SCROLL_BTN_HOVERED;
			RefreshTabBar();
			return;
		}
	}

	wxPoint position = evt.GetPosition();

	if (position.x >= 0 && position.y >= 0)
	{
		wxSize size = GetSize();
		if (position.x < size.GetWidth() && position.y < size.GetHeight())
		{
			if (m_toggle_button_rect.Contains(position))
			{
				ShowPanels(ArePanelsShown() ? wxRIBBON_BAR_MINIMIZED : wxRIBBON_BAR_PINNED);
				wxRibbonBarEvent event(wxEVT_RIBBONBAR_TOGGLED, GetId());
				event.SetEventObject(this);
				ProcessWindowEvent(event);
			}
			else if (m_help_button_rect.Contains(position))
			{
				wxRibbonBarEvent event(wxEVT_RIBBONBAR_HELP_CLICK, GetId());
				event.SetEventObject(this);
				ProcessWindowEvent(event);
			}
			else if (min_button_rect_.Contains(position))
			{
				((wxFrame*)wxGetTopLevelParent(this))->Iconize();
			}
			else if (max_button_rect_.Contains(position))
			{
				auto frame = (wxFrame*)wxGetTopLevelParent(this);
				if (!frame->IsMaximized()) {
					frame->Maximize();
				}
				else {
					frame->Restore();
				}
			}
			else if (close_button_rect_.Contains(position))
			{
				((wxFrame*)wxGetTopLevelParent(this))->Close();
			} 
			else if(!((wxFrame*)wxGetTopLevelParent(this))->IsMaximized()) {
				if (!HasCapture()) {
					CaptureMouse();
				}
				wxPoint mouse_pos = ClientToScreen(position);
				wxPoint wnd_pos = ClientToScreen(this->GetPosition());
				offset_.x = mouse_pos.x - wnd_pos.x;
				offset_.y = mouse_pos.y - wnd_pos.y;
			}
		}
	}
}

void MyRibbonBar::OnMouseLeftUp(wxMouseEvent& WXUNUSED(evt))
{
	offset_ = wxPoint(-1, -1);
	if (HasCapture()) {
		ReleaseMouse();
		return;
	}

	if (!m_tab_scroll_buttons_shown)
	{
		return;
	}

	int amount = 0;
	if (m_tab_scroll_left_button_state & wxRIBBON_SCROLL_BTN_ACTIVE)
	{
		amount = -1;
	}
	else if (m_tab_scroll_right_button_state & wxRIBBON_SCROLL_BTN_ACTIVE)
	{
		amount = 1;
	}
	if (amount != 0)
	{
		m_tab_scroll_left_button_state &= ~wxRIBBON_SCROLL_BTN_ACTIVE;
		m_tab_scroll_right_button_state &= ~wxRIBBON_SCROLL_BTN_ACTIVE;
		ScrollTabBar(amount * 8);
	}
}

void MyRibbonBar::OnMouseMiddleDown(wxMouseEvent& evt)
{
	DoMouseButtonCommon(evt, wxEVT_RIBBONBAR_TAB_MIDDLE_DOWN);
}

void MyRibbonBar::OnMouseMiddleUp(wxMouseEvent& evt)
{
	DoMouseButtonCommon(evt, wxEVT_RIBBONBAR_TAB_MIDDLE_UP);
}

void MyRibbonBar::OnMouseRightDown(wxMouseEvent& evt)
{
	DoMouseButtonCommon(evt, wxEVT_RIBBONBAR_TAB_RIGHT_DOWN);
}

void MyRibbonBar::OnMouseRightUp(wxMouseEvent& evt)
{
	DoMouseButtonCommon(evt, wxEVT_RIBBONBAR_TAB_RIGHT_UP);
}

void MyRibbonBar::OnMouseDoubleClick(wxMouseEvent& evt)
{
	wxRibbonPageTabInfo *tab = HitTestTabs(evt.GetPosition());
	//SetFocus();
	if (tab) {
		if (tab == &m_pages.Item(m_current_page)) {
			if (m_ribbon_state == wxRIBBON_BAR_PINNED)
			{
				HidePanels();
			}
			else
			{
				ShowPanels(wxRIBBON_BAR_PINNED);
			}
		}
	}
	else {
		auto frame = (wxFrame*)wxGetTopLevelParent(this);
		frame->Maximize(!frame->IsMaximized());
	}
}

///

wxBEGIN_EVENT_TABLE(MyStatusBarBase, Base)
EVT_SET_CURSOR(MyStatusBarBase::OnSetCursor)
EVT_MOTION(MyStatusBarBase::OnMotion)
EVT_LEFT_DOWN(MyStatusBarBase::OnLeftDown)
EVT_LEFT_UP(MyStatusBarBase::OnLeftUp)
EVT_MOUSE_CAPTURE_LOST(MyStatusBarBase::OnMouseLost)
wxEND_EVENT_TABLE()

void MyStatusBarBase::OnSetCursor(wxSetCursorEvent& event)
{
	auto frame = (wxFrame*)wxGetTopLevelParent(this);
	if (!frame->IsMaximized()) {
		wxPoint pt(event.GetX(), event.GetY());
		wxRect rcClient = GetClientRect();
		wxRect rcDrag(rcClient.GetRight() - 10, rcClient.GetBottom() - 10, 10, 10);
		if (rcDrag.Contains(pt)) {
			event.SetCursor(wxCursor(wxCURSOR_SIZENWSE));
			return;
		}
	}
	event.Skip();
}

void MyStatusBarBase::OnMotion(wxMouseEvent& event)
{
	wxPoint pt = event.GetPosition();
	if (event.LeftIsDown() && event.Dragging() && HasCapture()) {
		wxPoint mouse_pos = ClientToScreen(pt);
		wxPoint dp = mouse_pos - m_offset;
		if (dp.x > 10 || dp.x < -10 || dp.y > 10 || dp.y < -10) {
			m_offset = mouse_pos;
			auto frame = (wxFrame*)wxGetTopLevelParent(this);
			auto sz = frame->GetSize();
			frame->SetSize(sz.x + dp.x, sz.y + dp.y);
		}
		return;
	}
	else {
	}
}

void MyStatusBarBase::OnLeftDown(wxMouseEvent& event)
{
	event.Skip();
	auto frame = (wxFrame*)wxGetTopLevelParent(this);
	if (frame->IsMaximized()) {
		return;
	}

	wxPoint pt = event.GetPosition();
	wxRect rcClient = GetClientRect();
	wxRect rcDrag(rcClient.GetRight() - 10, rcClient.GetBottom() - 10, 10, 10);
	if (rcDrag.Contains(pt)) {
		if (!HasCapture()) {
			CaptureMouse();
		}
		m_offset = ClientToScreen(pt);
	}
}

void MyStatusBarBase::OnLeftUp(wxMouseEvent& event)
{
	if (HasCapture()) {
		ReleaseMouse();
		return;
	}
}

void MyStatusBarBase::OnMouseLost(wxMouseCaptureLostEvent& event)
{
	if (HasCapture()) {
		ReleaseMouse();
	}
}

///

LRESULT CALLBACK MyBaseFrame::MyWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	auto pThis = (MyBaseFrame*)GetProp(hwnd, _T("MyBaseFrame"));
	switch (uMsg)
	{
	/*case WM_NCHITTEST: {
		if (!pThis->IsMaximized())
		{
			POINT pt;
			pt.x = GET_X_LPARAM(lParam);
			pt.y = GET_Y_LPARAM(lParam);
			::ScreenToClient(hwnd, &pt);
			RECT rcClient;
			::GetClientRect(hwnd, &rcClient);
			if (pt.x < rcClient.left + 20 && pt.y < rcClient.top + 20)//左上角,判断是不是在左上角，就是看当前坐标是不是即在左边拖动的范围内，又在上边拖动的范围内，其它角判断方法类似
			{
				return HTTOPLEFT;
			}
			else if (pt.x > rcClient.right - 20 && pt.y < rcClient.top + 20)//右上角
			{
				return HTTOPRIGHT;
			}
			else if (pt.x<rcClient.left + 20 && pt.y>rcClient.bottom - 20)//左下角
			{
				return HTBOTTOMLEFT;
			}
			else if (pt.x > rcClient.right - 20 && pt.y > rcClient.bottom - 20)//右下角
			{
				return HTBOTTOMRIGHT;
			}
			else if (pt.x < rcClient.left + 20)
			{
				return HTLEFT;
			}
			else if (pt.x > rcClient.right - 20)
			{
				return HTRIGHT;
			}
			else if (pt.y < rcClient.top + 20)
			{
				return HTTOP;
			}if (pt.y > rcClient.bottom - 20)
			{
				return HTBOTTOM; //以上这四个是上、下、左、右四个边
			}
			else
			{
				//return HTCAPTION;
			}
			return HTCLIENT;
		}
	} break;*/
	case WM_NCCALCSIZE: {
		if (wParam == TRUE) {
			auto& params = *reinterpret_cast<NCCALCSIZE_PARAMS*>(lParam);
			RECT& rect = params.rgrc[0];
			//rect.left += 1;
			rect.top += 1;
			//rect.right -= 1;
			//rect.bottom -= 1;
		}
		return 0;
	} break;
	case WM_NCACTIVATE: {
		return TRUE;
	} break;
	}
	return CallWindowProc(pThis->old_wndproc_, hwnd, uMsg, wParam, lParam);
}

wxBEGIN_EVENT_TABLE(MyBaseFrame, wxFrame)
EVT_RIBBONBAR_PAGE_CHANGING(wxID_ANY, MyBaseFrame::OnPageChanging)
EVT_RIBBONBAR_HELP_CLICK(wxID_ANY, MyBaseFrame::OnHelpClicked)
//EVT_ERASE_BACKGROUND(MyBaseFrame::OnErase)
//EVT_PAINT(MyBaseFrame::OnPaint)
EVT_MAXIMIZE(MyBaseFrame::OnMaximizeEvent)
EVT_SIZE(MyBaseFrame::OnSizeEvent)
EVT_CLOSE(MyBaseFrame::OnCloseEvent)
EVT_TIMER(wxID_ANY, MyBaseFrame::OnTimer)
wxEND_EVENT_TABLE()

#include "transparent.xpm"
//#include "zoom_in.xpm"
//#include "zoom_out.xpm"

MyBaseFrame::MyBaseFrame(const char* xml, size_t xmlflag)
    : wxFrame(NULL, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(1024,600)
		,  0 | 
		wxSYSTEM_MENU |
		wxRESIZE_BORDER |
		wxMINIMIZE_BOX |
		wxMAXIMIZE_BOX |
		wxCLOSE_BOX |
		//wxCAPTION |
		//wxNO_BORDER |
		wxCLIP_CHILDREN
	)
{
	//SetBackgroundStyle(wxBG_STYLE_CUSTOM);
	//wxConfigBase *pConfig = wxConfigBase::Get();
	
	SetIcon(wxICON(mytrader));
	//SetIcon(wxIcon(wxT("mytrader"), wxBITMAP_TYPE_ICO_RESOURCE, 16, 16));
	//SetSize(pConfig->ReadLong(wxT("frame/width"), 800), pConfig->ReadLong(wxT("frame/height"), 600));

	//SetMaxSize(wxGetClientDisplayRect().GetSize());
	/*wxDisplay display(wxDisplay::GetFromWindow(this));
	wxRect rcDisplay(display.GetClientArea());
	SetMaxSize(rcDisplay.GetSize());*/
	/*auto frame = wxGetApp().GetFrame();
	wxDisplay display(frame ? wxDisplay::GetFromWindow(frame) : 0);
	SetSize(display.GetClientArea());*/

	//SetWindowLong(GetHandle(), GWL_STYLE, GetWindowLong(GetHandle(), GWL_STYLE) & ~WS_CAPTION);

    m_ribbon = new MyRibbonBar(this,-1,wxDefaultPosition, wxDefaultSize, wxRIBBON_BAR_FLOW_HORIZONTAL
		| wxRIBBON_BAR_SHOW_PAGE_LABELS
		| wxRIBBON_BAR_SHOW_PANEL_EXT_BUTTONS
		| wxRIBBON_BAR_SHOW_PANEL_MINIMISE_BUTTONS
		| wxRIBBON_BAR_ALWAYS_SHOW_TABS
		| wxRIBBON_BAR_SHOW_TOGGLE_BUTTON
		| wxRIBBON_BAR_SHOW_HELP_BUTTON
	);

    //m_ribbon->Realize();

#if wxUSE_INFOBAR
	infobar_ = new wxInfoBar(this);
#endif

	HWND hwnd = GetHandle();
	::SetProp(hwnd, _T("MyBaseFrame"), (HANDLE)this);
	old_wndproc_ = (WNDPROC)SetWindowLongPtr(hwnd, GWLP_WNDPROC, (LONG_PTR)MyWndProc);
}

MyBaseFrame::~MyBaseFrame()
{
	HWND hwnd = GetHandle();
	if (hwnd) {
		SetWindowLongPtr(hwnd, GWLP_WNDPROC, (LONG_PTR)old_wndproc_);
		old_wndproc_ = nullptr;
	}
}

void MyBaseFrame::SetTitle(const wxString& title)
{
	Base::SetTitle(title);
	m_ribbon->Refresh(false);
}

void MyBaseFrame::AddFrame(MyBaseFrame* frame)
{
	RemoveLastPages();
	auto page = new wxRibbonPage(m_ribbon, wxID_ANY, frame->GetTitle());
	page->SetClientData(frame);
	frames_[frame] = page;
	AddLastPages();
	m_ribbon->Realize();
	Layout();
}

void MyBaseFrame::RemoveFrame(MyBaseFrame* frame)
{
	auto it = frames_.find(frame);
	if (it != frames_.end()) {
		m_ribbon->DeletePage(m_ribbon->GetPageNumber(it->second));
		frames_.erase(it);
		m_ribbon->Realize();
		Layout();
	}
}

void MyBaseFrame::ActivateFrame(MyBaseFrame* frame)
{
	auto it = frames_.find(frame);
	if (it != frames_.end()) {
		m_ribbon->SetActivePage(it->second);
	}
}

void MyBaseFrame::UpdateAllPages()
{
	m_ribbon->ClearPages();
	AddFirstPages();
	for (auto& pr : frames_)
	{
		auto page = new wxRibbonPage(m_ribbon, wxID_ANY, pr.first->GetTitle());
		page->SetClientData(pr.first);
		pr.second = page;
	}
	AddLastPages();
	m_ribbon->Realize();
	Layout();
}

void MyBaseFrame::ShowMessage(const wxString& msg, int flags)
{
#if wxUSE_INFOBAR
	infobar_->ShowMessage(msg, flags);
#endif
}


void MyBaseFrame::HideMessage()
{
#if wxUSE_INFOBAR
	infobar_->Dismiss();
#endif
}

//
//size_t MyBaseFrame::AddExchangePage(HZQDB h, size_t product_id)
//{
//	zqdb::Exchange exchange(h);
//	auto strExchange = utf2wxString(exchange->Exchange);
//	auto strName = wxT(" ") + utf2wxString(exchange->Name) + wxT(" ");
//	wxRibbonPage* home = new wxRibbonPage(m_ribbon, wxID_ANY, strExchange);
//
//	auto market_panel = new wxRibbonPanel(home, ID_MARKET_EXCHANGE + 0, strName);
//	wxRibbonButtonBar* market_bar = new wxRibbonButtonBar(market_panel);
//	zqdb::AllProduct allproduct(h);
//	/*std::sort(allproduct.begin(), allproduct.end(), [](HZQDB x, HZQDB y) {
//		zqdb::Product xproduct(x);
//		zqdb::Product yproduct(y);
//		return strcmp(xproduct->Name, yproduct->Name) < 0;
//	});*/
//	for (size_t k = 0; k < allproduct.size(); k++)
//	{
//		zqdb::Product product(allproduct[k]);
//		auto item = market_bar->AddButton(product_id, utf2wxString(product->Name), wxBitmap(transparent_xpm));
//		market_bar->SetItemClientData(item, allproduct[k]);
//		market_bar->SetButtonMaxSizeClass(product_id, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
//		market_bar->SetButtonMinSizeClass(product_id, wxRIBBON_BUTTONBAR_BUTTON_MEDIUM);
//		++product_id;
//	}
//
//	return product_id;
//}

void MyBaseFrame::AddLastPages()
{
	wxRibbonPage* scheme = new wxRibbonPage(m_ribbon, wxID_ANY, "帮助");
	last_pages_[scheme] = std::bind(&MyBaseFrame::OnHelpClicked, this, std::placeholders::_1);
}

void MyBaseFrame::RemoveLastPages()
{
	for (auto& pr : last_pages_)
	{
		m_ribbon->DeletePage(m_ribbon->GetPageNumber(pr.first));
	}
	last_pages_.clear();
}

int MyBaseFrame::FilterEvent(wxEvent& event)
{
	// Continue processing the event normally as well.
	return wxEventFilter::Event_Skip;
}

void MyBaseFrame::OnSkinInfoChanged()
{
	Freeze();
	//普通显示信息变化
	SetBackgroundColour(skin_info_ptr_->crXYLine);
	//SetBackgroundColour(skin_info_ptr_->crPrimary);
	SetForegroundColour(skin_info_ptr_->crTertiary);
	//wxColour primary, secondary, tertiary;
	//m_ribbon->SetBackgroundColour();
	/*m_ribbon->GetArtProvider()->GetColourScheme(&primary, &secondary, &tertiary);
#ifdef _DEBUG
	int r, g, b;
	r = primary.Red();
	g = primary.Green();
	b = primary.Blue();
	r = secondary.Red();
	g = secondary.Green();
	b = secondary.Blue();
	r = tertiary.Red();
	g = tertiary.Green();
	b = tertiary.Blue();
#endif*/
	if (skin_info_ptr_->artProvider) {
		m_ribbon->SetArtProvider(skin_info_ptr_->artProvider->Clone());
	}
	else {
		m_ribbon->GetArtProvider()->SetColourScheme(skin_info_ptr_->crPrimary, skin_info_ptr_->crSecondary, skin_info_ptr_->crTertiary);
		skin_info_ptr_->artProvider = m_ribbon->GetArtProvider()->Clone();
	}
	//
	auto statusbar = GetStatusBar();
	if (statusbar) {
		statusbar->SetBackgroundColour(skin_info_ptr_->crPrimary);
		statusbar->SetForegroundColour(skin_info_ptr_->crTertiary);
	}
	Layout();
	Thaw();
}

void MyBaseFrame::OnPageChanging(wxRibbonBarEvent& evt)
{
	evt.Veto();
	auto page = evt.GetPage();
	if (page) {
		auto it = last_pages_.find(page);
		if (it != last_pages_.end()) {
			it->second(evt);
		}
		else {
			auto frame = (MyBaseFrame*)page->GetClientData();
			if (frame) {
				wxGetApp().ShowFrame(frame);
			}
			else {
				wxMessageBox(wxT("敬请期待"), evt.GetPage()->GetLabelText(), wxICON_INFORMATION | wxOK, this);
			}
		}
	}
}

void MyBaseFrame::OnHelpClicked(wxRibbonBarEvent& evt)
{
	/*wxString filename = wxGetTextFromUser(
		"Enter the URL",
		"exec sample",
		s_url,
		this
	);
	if (filename.empty())
		return;
	s_url = filename;*/
	if (!wxLaunchDefaultBrowser(HOME_URL)) {
		wxLogError("Failed to open URL \"%s\"", HOME_URL);
	}
}

void MyBaseFrame::OnErase(wxEraseEvent &event)
{

}

void MyBaseFrame::OnPaint(wxPaintEvent &event)
{
	wxPaintDC dc(this);
	dc.Clear();
}

void MyBaseFrame::OnMaximizeEvent(wxMaximizeEvent& evt)
{
	wxDisplay display(wxDisplay::GetFromWindow(this));
	SetSize(display.GetClientArea());
}

// This shows how to hide ribbon dynamically if there is not enough space.
void MyBaseFrame::OnSizeEvent(wxSizeEvent& evt)
{
   if ( evt.GetSize().GetWidth() < 200 )
        m_ribbon->Hide();
   else {
	   m_ribbon->Show();
	   m_ribbon->Refresh(false);
   }

   evt.Skip();
}

void MyBaseFrame::OnCloseEvent(wxCloseEvent& evt)
{
	evt.Skip();
}

void MyBaseFrame::OnTimer(wxTimerEvent& evt)
{
	bool change = false;
	for (auto& pr : frames_)
	{
		auto title = pr.first->GetTitle();
		if (title != pr.second->GetLabel()) {
			change = true;
			pr.second->SetLabel(title);
		}
	}
	if (change) {
		m_ribbon->Realize();
	}
}

