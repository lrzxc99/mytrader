#pragma once

#include <wx/taskbar.h>

class MyTaskBarIcon : public wxTaskBarIcon
{
public:
#if defined(__WXOSX__) && wxOSX_USE_COCOA
    MyTaskBarIcon(wxTaskBarIconType iconType = wxTBI_DEFAULT_TYPE)
    :   wxTaskBarIcon(iconType)
#else
    MyTaskBarIcon()
#endif
    {}

    void OnLeftButtonDClick(wxTaskBarIconEvent&);
    void OnMenuRestore(wxCommandEvent&);
	void OnAbout(wxCommandEvent&);
	void OnCloseSound(wxCommandEvent&);
	void OnCloseData(wxCommandEvent&);
	void OnDownloadData(wxCommandEvent&);
	void OnExportData(wxCommandEvent&);
	void OnClearData(wxCommandEvent&);
	void OnResettings(wxCommandEvent&);
	void OnReportContext(wxCommandEvent&);
	void OnRestart(wxCommandEvent&);
	void OnMenuCloseExit(wxCommandEvent&);
    void OnMenuExit(wxCommandEvent&);
    virtual wxMenu *CreatePopupMenu() wxOVERRIDE;

    wxDECLARE_EVENT_TABLE();
};
