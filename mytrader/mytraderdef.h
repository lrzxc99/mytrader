#pragma once

#include "version.h"

#if !defined(APP_NAME)
#define APP_NAME "mytrader"
#endif

#if !defined(APP_TITLE)
#define APP_TITLE APP_NAME
#endif

#if !defined(APP_VERSION)
#define APP_VERSION MYTRADER_VERSION
#endif

#define OPTION_TYPE "type" //cef subprocess
#define OPTION_WAIT "wait"
#define OPTION_CLEAR_SETTINGS "clearsettings"
#define OPTION_CLEAR_DATA "cleardata"
#define OPTION_TEST "test"
#define OPTION_FULL "full"

#define HOME_URL "www.mytrader.org.cn"
#define PROJECT_URL "www.gitee.com/7thTool/mytrader"
#define PROJECT_SETUP_URL "https://gitee.com/7thTool/mytrader/blob/master/book/zh-cn/04.md"
#define PROJECT_CONTACT_URL "https://gitee.com/7thTool/mytrader/blob/master/book/zh-cn/contact.md"
