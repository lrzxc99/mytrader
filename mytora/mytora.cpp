#include <mytora.h>
#include <zqtora.h>
#include <zqdb.pb.h>
#include<wx/generic/statbmpg.h>
#include <wx/gbsizer.h>

#include <wx/xy/xyplot.h>
#include <wx/xy/xylinerenderer.h>

#include <wx/axis/numberaxis.h>
#include <wx/axis/dateaxis.h>

#include <wx/xy/timeseriesdataset.h>

#include "../zqapp/circle.xpm"

#ifdef _DEBUG

const std::set<wxString> g_names = {
	
};

#endif

static MyTORAModule* g_module = nullptr;

static const std::array<const char*, 3> const sz_exchanges = {
	EXCHANGE_SSE,EXCHANGE_SZSE,EXCHANGE_BSE
};

// -- MyTORALoginView -- 

MyTORALoginView::MyTORALoginView(wxWindow* parent, const char* xml, size_t xmlflag) :Base(parent, xml, xmlflag)
{
	CFG_FROM_XML(cfg, xml, xmlflag);

	wizard_ = cfg.get<std::string>("wizard","");
	auto Broker = cfg.get<std::string>(STR_MDB_FIELD_INDEX(ZQDB, USER, BROKER),"");

	pg_ = new wxPropertyGrid(this, wxID_ANY, wxDefaultPosition, FromDIP(wxSize(300, -1)),
		wxPG_TOOLTIPS |
		//wxPG_SPLITTER_AUTO_CENTER |
		//wxPG_STATIC_LAYOUT |
		wxPG_BOLD_MODIFIED);

	//pg_->Append(new wxPropertyCategory(_("Base Information"), wxPG_LABEL));
	pg_->Append(new wxArrayStringProperty(_("DepartmentID"), wxPG_LABEL));
	pg_->SetPropertyEditor(_("DepartmentID"), wxPGEditor_ComboBox);
	pg_->Append(new wxArrayStringProperty(_("UserID"), wxPG_LABEL));
	pg_->SetPropertyEditor(_("UserID"), wxPGEditor_ComboBox);
	pg_->SetPropertyAttribute(_("UserID"), wxPG_ATTR_HINT, wxT("必填"));

	if (Broker == ZQDB_USER_BROKER_MOCK) {
		pg_->SetPropertyReadOnly(_("DepartmentID"));
		pg_->SetPropertyValue(_("DepartmentID"), ZQDB_USER_BROKER_MOCK);
		pg_->SetPropertyValue(_("UserID"), ZQDB_USER_BROKER_MOCK);
		//
		pg_->Append(new wxFloatProperty(_("Amount"), wxPG_LABEL, 1000.));
		pg_->SetPropertyAttribute(_("Amount"), wxPG_ATTR_MIN, 1);
		pg_->SetPropertyAttribute(_("Amount"), wxPG_ATTR_MAX, 100000000.);
		pg_->SetPropertyAttribute(_("Amount"), wxPG_ATTR_UNITS, wxT("万"));
	} else if (Broker == ZQDB_USER_BROKER_TEST) {
		pg_->SetPropertyReadOnly(_("DepartmentID"));
		pg_->SetPropertyValue(_("DepartmentID"), ZQDB_USER_BROKER_TEST);
		pg_->SetPropertyValue(_("UserID"), ZQDB_USER_BROKER_TEST);
		//
		pg_->Append(new wxFloatProperty(_("Amount"), wxPG_LABEL, 1000.));
		pg_->SetPropertyAttribute(_("Amount"), wxPG_ATTR_MIN, 1);
		pg_->SetPropertyAttribute(_("Amount"), wxPG_ATTR_MAX, 100000000.);
		pg_->SetPropertyAttribute(_("Amount"), wxPG_ATTR_UNITS, wxT("万"));
	}
	else {
		pg_->Append(new wxStringProperty(_("Password"), wxPG_LABEL));
		pg_->SetPropertyAttribute(_("Password"), wxPG_STRING_PASSWORD, true);
		pg_->SetPropertyAttribute(_("Password"), wxPG_ATTR_HINT, wxT("必填"));
		//pg_->Append(new wxStringProperty(_("AuthCode"), wxPG_LABEL));
		//pg_->Append(new wxStringProperty(_("AppID"), wxPG_LABEL));

		//pg_->Append(new wxPropertyCategory(_("Server"), wxPG_LABEL));
		pg_->Append(new wxArrayStringProperty(_("Trade Server"), wxPG_LABEL));
		pg_->SetPropertyAttribute(_("Trade Server"), wxPG_ATTR_HINT, wxT("必填"));
	}

	if (!wizard_.empty()) {
		pg_->Append(new wxArrayStringProperty(_("Market Server"), wxPG_LABEL));
		pg_->SetPropertyAttribute(_("Market Server"), wxPG_ATTR_HINT, wxT("必填"));

		auto subscribe_all = cfg.get<int>("subscribe_all", 0);

		//
		// wxEnumProperty does not store strings or even list of strings
		// ( so that's why they are static in function ).
		static const wxString enum_SUB_labels[] = { wxT("手动按需订阅"), wxT("自动全市场订阅") };

		// this value array would be optional if values matched string indexes
		static long enum_SUB_values[] = { 0, 1 };

		// note that the initial value (the last argument) is the actual value,
		// not index or anything like that. Thus, our value selects "Another Item".
		pg_->Append(new wxEnumProperty(wxT("行情订阅方式"), wxPG_LABEL,
			wxArrayString(WXSIZEOF(enum_SUB_labels), enum_SUB_labels),
			wxArrayInt(enum_SUB_values, enum_SUB_values + WXSIZEOF(enum_SUB_values)), subscribe_all));
	}
	else {
		//
	}

	pg_->FitColumns();

	wxBoxSizer *sizerAll = new wxBoxSizer(wxHORIZONTAL);

	sizerAll->Add(
		pg_,
		1, // stretching
		wxEXPAND
	);

	SetSizerAndFit(sizerAll);
}

MyTORALoginView::~MyTORALoginView()
{
	CancelLogin(false);
}

wxString MyTORALoginView::GetValue(wxString name, const wxString& def)
{
	wxPGProperty* p = pg_->GetProperty(name);
	if (p) {
		if (name == _("Amount")) {
			return wxString::FromDouble(p->GetValue().GetDouble() * 10000.);
		}
		return p->GetValueAsString(wxPG_FULL_VALUE);
	}
	return def;
}

void MyTORALoginView::SetTypeMode(ZQDB_APP_TYPE type, ZQDB_APP_RUN_MODE mode)
{
	switch (type)
	{
	case ZQDB_APP_SERVER: {
		pg_->SetPropertyValue(wxT("行情订阅方式"), 1);
	} break;
	default: {
		pg_->SetPropertyValue(wxT("行情订阅方式"), 0);
	} break;
	}
	switch (mode)
	{
	case ZQDB_APP_RUN_REAL: {
		wizard_ = u8"奇点实盘";
	} break;
	case ZQDB_APP_RUN_MOCK: {
		wizard_ = u8"奇点仿真";
	} break;
	case ZQDB_APP_RUN_TEST: {
		wizard_ = u8"股票测试";
	} break;
	default: {
		wizard_ = u8"TORA";
	} break;
	}
}

bool MyTORALoginView::IsValid()
{ 
	if (!wizard_.empty()) {
		if (GetValue(_("UserID")).IsEmpty()
			|| GetValue(_("Password")).IsEmpty()
			|| GetValue(_("Trade Server")).IsEmpty()
			|| GetValue(_("Market Server")).IsEmpty()) {
			return false;
		}
	}
	else {
		//
	}
	return true; 
}

void MyTORALoginView::Save(boost::property_tree::ptree& cfg) 
{ 
	wxASSERT(!wizard_.empty());
	auto opt_data = cfg.get_child_optional("data");
	ASSERT(opt_data);
	auto& cfg_data = opt_data.get();
	cfg_data.put("name", wizard_);
	cfg_data.put("subscribe_all", pg_->GetPropertyValueAsInt(wxT("行情订阅方式")));
	auto DepartmentID = GetValue(_("DepartmentID"));
	auto UserID = GetValue(_("UserID"));
	if (!UserID.empty()) {
		auto opt_user_array = cfg_data.get_child_optional("user");
		ASSERT(opt_user_array);
		auto& cfg_user_array = opt_user_array.get();
		boost::property_tree::ptree cfg_user;
		cfg_user.put("DepartmentID", wxString2utf(DepartmentID));
		cfg_user.put("LogInAccount", wxString2utf(UserID));
		if (DepartmentID == ZQDB_USER_BROKER_TEST) {
			cfg_user.put("Amount", wxString2utf(GetValue(_("Amount"))));
		}
		else {
			cfg_user.put("Password", wxString2utf(GetValue(_("Password"))));
			//
			boost::property_tree::ptree cfg_trade_server_array;
			auto trade_server_array = wxSplit(GetValue(_("Trade Server")), wxT(','));
			for (int i = 0, j = trade_server_array.GetCount(); i < j; i++)
			{
				trade_server_array[i].Trim().Trim(false);
				if (trade_server_array[i].IsEmpty()) {
					continue;
				}
				boost::property_tree::ptree cfg_trade_server;
				cfg_trade_server.put("", wxString2utf(trade_server_array[i]));
				cfg_trade_server_array.push_back(std::make_pair("", cfg_trade_server));
			}
			cfg_user.push_back(std::make_pair("FrontAddress", cfg_trade_server_array));
			//
			boost::property_tree::ptree cfg_market_server_array;
			auto market_server_array = wxSplit(GetValue(_("Market Server")), wxT(','));
			for (int i = 0, j = market_server_array.GetCount(); i < j; i++)
			{
				market_server_array[i].Trim().Trim(false);
				if (market_server_array[i].IsEmpty()) {
					continue;
				}
				boost::property_tree::ptree cfg_market_server;
				cfg_market_server.put("", wxString2utf(market_server_array[i]));
				cfg_market_server_array.push_back(std::make_pair("", cfg_market_server));
			}
			cfg_user.push_back(std::make_pair("MdFrontAddress", cfg_market_server_array));
			//
			cfg_user_array.push_back(std::make_pair("", cfg_user));
			//cfg_data.push_back(std::make_pair("user", user_array));
		}
	}
}

const char* MyTORALoginView::StartLogin(std::function<void(const zqdb::Msg& rsp)>&& cb)
{
	cb_ = std::move(cb);
	auto DepartmentID = GetValue(_("DepartmentID"));
	auto UserID = GetValue(_("UserID"));
	auto Password = GetValue(_("Password"));
	if (UserID.empty()) {
		return nullptr;
	}

	userid_ = wxString2utf(UserID);

	/*boost::property_tree::ptree cfg_user;
	cfg_user.put("DepartmentID", wxString2utf(DepartmentID));
	cfg_user.put("LogInAccount", userid_);
	cfg_user.put("Password", wxString2utf(Password));
	//cfg_user.put("AuthCode", wxString2utf(GetValue(_("AuthCode"))));
	//cfg_user.put("AppID", wxString2utf(GetValue(_("AppID"))));
	//
	boost::property_tree::ptree cfg_trade_server_array;
	auto trade_server_array = wxSplit(GetValue(_("Trade Server")), wxT(','));
	for (int i = 0, j = trade_server_array.GetCount(); i < j; i++)
	{
		trade_server_array[i].Trim().Trim(false);
		if (trade_server_array[i].IsEmpty()) {
			continue;
		}
		boost::property_tree::ptree cfg_trade_server;
		cfg_trade_server.put("", wxString2utf(trade_server_array[i]));
		cfg_trade_server_array.push_back(std::make_pair("", cfg_trade_server));
	}
	cfg_user.push_back(std::make_pair("FrontAddress", cfg_trade_server_array));*/
	//
	{
		req_ = zqdb::Msg(ZQDB_MSG_REQUEST_DO);
		req_.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_USER_LOGIN);
		req_.SetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, BROKER), wxString2utf(DepartmentID).c_str());
		req_.SetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, USER), userid_.c_str());
		req_.SetParam("Password", wxString2utf(Password).c_str());
		req_.SetParam("FrontAddress", wxString2utf(GetValue(_("Trade Server"))).c_str());
		if (ZQDB_STATUS_OK != ZQDBRequestCBEx(*g_module, req_, [](ZQDB_MSG* req, ZQDB_MSG* rsp, void* user_data) {
			((MyTORALoginView*)user_data)->OnResponse(rsp);
		}, this)) {
			req_.Reset();
			return nullptr;
		}
	}
	return userid_.c_str();
}

void MyTORALoginView::CancelLogin(bool logout)
{
	if (cb_) {
		cb_ = nullptr;
	}
	if (req_) {
		ZQDBCancelRequest(req_);
		req_ = zqdb::Msg();

		if (logout) {
			auto DepartmentID = GetValue(_("DepartmentID"));
			auto UserID = GetValue(_("UserID"));
			if (UserID.empty()) {
				return;
			}

			userid_ = wxString2utf(UserID);
			{
				zqdb::Msg msg(ZQDB_MSG_REQUEST_DO);
				msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_USER_LOGOUT);
				msg.SetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, BROKER), wxString2utf(DepartmentID).c_str());
				msg.SetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, USER), userid_.c_str());
				ZQDBRequestEx(*g_module, msg, nullptr, 0);
			}
		}
	}
}

void MyTORALoginView::OnResponse(ZQDB_MSG* rsp)
{
	zqdb::Msg msg(rsp, zqdb::Msg::Ref);
	zqdb::GetApp().Post([this, msg] {
		if (cb_) {
			cb_(msg);
			if (msg.IsContinue()) {

			}
			else {
				cb_ = nullptr;
				req_ = zqdb::Msg();
			}
		}
	});
}

// -- MyTORAUserInfoView -- 

MyTORAUserInfoView::MyTORAUserInfoView(wxWindow *parent, const char* xml, size_t xmlflag) :Base(parent, xml, xmlflag)
{
	ctrl_user_list_ = new wxDataViewCtrl(this, wxID_ANY, wxDefaultPosition,
		FromDIP(wxSize(240, 160)));
	ctrl_user_list_model_ = new zqdb::HZQDBModel(*g_module, "./mytora/userinfo.json", XUtil::XML_FLAG_JSON_FILE);
	ctrl_user_list_->AssociateModel(ctrl_user_list_model_.get());
	ctrl_user_list_->AppendTextColumn(wxT("名称"), 0, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE);
	ctrl_user_list_->AppendTextColumn(wxT("数值"), 1, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE);

	ctrl_account_list_ = new wxDataViewCtrl(this, wxID_ANY, wxDefaultPosition,
		FromDIP(wxSize(240, 80)));
	ctrl_account_list_model_ = new zqdb::HZQDBListModel(*g_module, "./mytora/accountlist.json", XUtil::XML_FLAG_JSON_FILE);
	ctrl_account_list_->AssociateModel(ctrl_account_list_model_.get());
	auto& ctrl_account_col_infos = ctrl_account_list_model_->GetColInfo();
	for (size_t i = 0, j = ctrl_account_col_infos.size(); i < j; i++)
	{
		auto& col_info = ctrl_account_col_infos[i];
		ctrl_account_list_->AppendTextColumn(col_info.name, i, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE);
	}

	ctrl_investor_list_ = new wxDataViewCtrl(this, wxID_ANY, wxDefaultPosition,
		FromDIP(wxSize(240, 80)));
	ctrl_investor_list_model_ = new zqdb::HZQDBListModel(*g_module, "./mytora/investorlist.json", XUtil::XML_FLAG_JSON_FILE);
	ctrl_investor_list_->AssociateModel(ctrl_investor_list_model_.get());
	auto& ctrl_investor_col_infos = ctrl_investor_list_model_->GetColInfo();
	for (size_t i = 0, j = ctrl_investor_col_infos.size(); i < j; i++)
	{
		auto& col_info = ctrl_investor_col_infos[i];
		ctrl_investor_list_->AppendTextColumn(col_info.name, i, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE);
	}

	wxSizer* topSizer = new wxBoxSizer(wxHORIZONTAL);

	topSizer->Add(ctrl_user_list_, 0, wxEXPAND);

	wxSizer* sizer_user_v = new wxBoxSizer(wxVERTICAL);
	sizer_user_v->Add(ctrl_account_list_, 0, wxEXPAND);
	sizer_user_v->Add(ctrl_investor_list_, 0, wxEXPAND);
	topSizer->Add(sizer_user_v, 1, wxEXPAND);

	SetSizer(topSizer);
}

void MyTORAUserInfoView::OnSkinInfoChanged()
{
	Base::OnSkinInfoChanged();
}

void MyTORAUserInfoView::OnUserChanged() 
{
	ctrl_user_list_model_->SetHandle(user_);
	zqdb::AllUserInvestor allinvestor(user_, *g_module);
	ctrl_investor_list_model_->Show(allinvestor);
	zqdb::AllUserAccount allaccount(user_, *g_module);
	ctrl_account_list_model_->Show(allaccount);
}

// -- MyTORAUserOrderView -- 

wxBEGIN_EVENT_TABLE(MyTORAUserOrderView, Base)
EVT_SHOW(MyTORAUserOrderView::OnShowEvent)
wxEND_EVENT_TABLE()

MyTORAUserOrderView::MyTORAUserOrderView(wxWindow *parent, const char* xml, size_t xmlflag) :Base(parent, xml, xmlflag)
{
	ctrl_list_ = new wxDataViewCtrl(this, wxID_ANY);
	ctrl_list_model_ = new zqdb::HZQDBListModel(*g_module, "./mytora/orderlist.json", XUtil::XML_FLAG_JSON_FILE);
	ctrl_list_->AssociateModel(ctrl_list_model_.get());
	auto htb = ZQDBFindTableEx(*g_module, STR_ZQDB_TABLE_ORDER);
	auto& col_infos = ctrl_list_model_->GetColInfo();
	size_t i = 0, j = col_infos.size();
	for (; i < j; i++)
	{
		auto& col_info = col_infos[i];
		ZQDBNormalizeField(htb, const_cast<MDB_FIELD*>(&col_info.field), 1);
		ctrl_list_->AppendTextColumn(col_info.name, i, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE
			, MDBFieldTypeIsNumber(col_info.field.type) ? wxALIGN_RIGHT : wxALIGN_NOT);
	}
	ctrl_list_->AppendTextColumn(wxEmptyString, i, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE);

	wxSizer* topSizer = new wxBoxSizer(wxVERTICAL);

	topSizer->Add(ctrl_list_, 1, wxEXPAND);

	SetSizer(topSizer);

	Bind(wxEVT_DATAVIEW_SELECTION_CHANGED, &MyTORAUserOrderView::OnSelChanged, this);
	Bind(wxEVT_DATAVIEW_ITEM_ACTIVATED, &MyTORAUserOrderView::OnActivated, this);
}

void MyTORAUserOrderView::ShowAll()
{
	wxASSERT(IsDispOk());
	zqdb::AllUserOrder all(user_, *g_module);
	std::reverse(all.begin(), all.end());
	ctrl_list_model_->Show(all);
}

void MyTORAUserOrderView::ClearAll()
{
	ctrl_list_model_->Clear();
}

void MyTORAUserOrderView::RefreshAll()
{
	ctrl_list_->Refresh();
}

void MyTORAUserOrderView::OnSkinInfoChanged()
{
	Base::OnSkinInfoChanged();
	if (IsShown() && IsDispOk()) {
		ShowAll();
	}
}

void MyTORAUserOrderView::OnHandleChanged()
{
	Base::OnHandleChanged();
	if (IsShown() && IsDispOk()) {
		ShowAll();
	}
}

void MyTORAUserOrderView::OnUserChanged()
{
	Base::OnUserChanged();
	if (IsShown()) {
		ShowAll();
	}
}

void MyTORAUserOrderView::OnShowEvent(wxShowEvent& evt)
{
	if (evt.IsShown() && IsDispOk()) {
		ShowAll();
	}
}

void MyTORAUserOrderView::OnSelChanged(wxDataViewEvent &event)
{
	auto h = ctrl_list_model_->GetData(event.GetItem());
	if (h) {
		zqdb::ObjectT<tagOrderInfo> order(h);
		HZQDB hs[3] = { 0 };
		ZQDBGetCodeByTradeCode(*g_module, order->Code, hs, 3);
		if (hs[0]) {
			zqdb::GetApp().Goto(hs[0], wxGetTopLevelParent(this));
		}
	}
}

void MyTORAUserOrderView::OnActivated(wxDataViewEvent &event)
{
	auto h = ctrl_list_model_->GetData(event.GetItem());
	if (h) {
		zqdb::ObjectT<tagOrderInfo> order(h);
		if (!ZQDBOrderIsFinal(order->Status)) {
			if (wxOK == wxMessageBox(wxT("确定撤销委托吗？"), wxT("提示"), wxOK | wxCANCEL | wxCENTRE)) {
				zqdb::GetApp().CancelOrder(user_, h);
			}
		}
		else {
			wxMessageBox(wxT("委托不可撤销"), wxT("提示"), wxOK | wxCENTRE);
		}
	}
}

// -- MyTORAUserTradeView -- 

wxBEGIN_EVENT_TABLE(MyTORAUserTradeView, Base)
EVT_SHOW(MyTORAUserTradeView::OnShowEvent)
wxEND_EVENT_TABLE()

MyTORAUserTradeView::MyTORAUserTradeView(wxWindow *parent, const char* xml, size_t xmlflag) :Base(parent, xml, xmlflag)
{
	ctrl_list_ = new wxDataViewCtrl(this, wxID_ANY);
	ctrl_list_model_ = new zqdb::HZQDBListModel(*g_module, "./mytora/tradelist.json", XUtil::XML_FLAG_JSON_FILE);
	ctrl_list_->AssociateModel(ctrl_list_model_.get());
	auto htb = ZQDBFindTableEx(*g_module, STR_ZQDB_TABLE_TRADE);
	auto& col_infos = ctrl_list_model_->GetColInfo();
	size_t i = 0, j = col_infos.size();
	for (; i < j; i++)
	{
		auto& col_info = col_infos[i];
		ZQDBNormalizeField(htb, const_cast<MDB_FIELD*>(&col_info.field), 1);
		ctrl_list_->AppendTextColumn(col_info.name, i, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE
			, MDBFieldTypeIsNumber(col_info.field.type) ? wxALIGN_RIGHT : wxALIGN_NOT);
	}
	ctrl_list_->AppendTextColumn(wxEmptyString, i, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE);

	wxSizer* topSizer = new wxBoxSizer(wxVERTICAL);

	topSizer->Add(ctrl_list_, 1, wxEXPAND);

	SetSizer(topSizer);

	Bind(wxEVT_DATAVIEW_SELECTION_CHANGED, &MyTORAUserTradeView::OnSelChanged, this);
	Bind(wxEVT_DATAVIEW_ITEM_ACTIVATED, &MyTORAUserTradeView::OnActivated, this);
}

void MyTORAUserTradeView::ShowAll()
{
	wxASSERT(IsDispOk());
	zqdb::AllUserTrade all(user_, *g_module);
	std::reverse(all.begin(), all.end());
	ctrl_list_model_->Show(all);
}

void MyTORAUserTradeView::ClearAll()
{
	ctrl_list_model_->Clear();
}

void MyTORAUserTradeView::RefreshAll()
{
	ctrl_list_->Refresh();
}

void MyTORAUserTradeView::OnShowEvent(wxShowEvent& evt)
{
	if (evt.IsShown() && IsDispOk()) {
		ShowAll();
	}
}

void MyTORAUserTradeView::OnSelChanged(wxDataViewEvent &event)
{
	auto h = ctrl_list_model_->GetData(event.GetItem());
	if (h) {
		zqdb::ObjectT<tagTradeInfo> trade(h);
		HZQDB hs[3] = { 0 };
		ZQDBGetCodeByTradeCode(*g_module, trade->Code, hs, 3);
		if (hs[0]) {
			zqdb::GetApp().Goto(hs[0], wxGetTopLevelParent(this));
		}
	}
}

void MyTORAUserTradeView::OnActivated(wxDataViewEvent &event)
{
	OnSelChanged(event);
}

// -- MyTORAUserPositionView -- 

wxBEGIN_EVENT_TABLE(MyTORAUserPositionView, Base)
EVT_SHOW(MyTORAUserPositionView::OnShowEvent)
wxEND_EVENT_TABLE()

MyTORAUserPositionView::MyTORAUserPositionView(wxWindow *parent, const char* xml, size_t xmlflag):Base(parent, xml, xmlflag)
{
	ctrl_list_ = new wxDataViewCtrl(this, wxID_ANY);
	ctrl_list_model_ = new zqdb::HZQDBListModel(*g_module, "./mytora/positionlist.json", XUtil::XML_FLAG_JSON_FILE);
	ctrl_list_->AssociateModel(ctrl_list_model_.get());
	auto htb = ZQDBFindTableEx(*g_module, STR_ZQDB_TABLE_POSITION);
	auto& col_infos = ctrl_list_model_->GetColInfo();
	size_t i = 0, j = col_infos.size();
	for (; i < j; i++)
	{
		auto& col_info = col_infos[i];
		ZQDBNormalizeField(htb, const_cast<MDB_FIELD*>(&col_info.field), 1);
		ctrl_list_->AppendTextColumn(col_info.name, i, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE
			, MDBFieldTypeIsNumber(col_info.field.type) ? wxALIGN_RIGHT : wxALIGN_NOT);
	}
	ctrl_list_->AppendTextColumn(wxEmptyString, i, wxDATAVIEW_CELL_INERT, wxCOL_WIDTH_AUTOSIZE);

	wxSizer* topSizer = new wxBoxSizer(wxVERTICAL);

	topSizer->Add(ctrl_list_, 1, wxEXPAND);

	SetSizer(topSizer);

	Bind(wxEVT_DATAVIEW_SELECTION_CHANGED, &MyTORAUserPositionView::OnSelChanged, this);
	Bind(wxEVT_DATAVIEW_ITEM_ACTIVATED, &MyTORAUserPositionView::OnActivated, this);
}

void MyTORAUserPositionView::ShowAll()
{
	wxASSERT(IsDispOk());
	zqdb::AllUserPosition all(user_, *g_module);
	ctrl_list_model_->Show(all);
}

void MyTORAUserPositionView::ClearAll()
{
	ctrl_list_model_->Clear();
}

void MyTORAUserPositionView::RefreshAll()
{
	ctrl_list_->Refresh();
}

void MyTORAUserPositionView::OnShowEvent(wxShowEvent& evt)
{
	if (evt.IsShown() && IsDispOk()) {
		ShowAll();
	}
}

void MyTORAUserPositionView::OnSelChanged(wxDataViewEvent &event)
{
	auto h = ctrl_list_model_->GetData(event.GetItem());
	if (h) {
		zqdb::ObjectT<tagPositionInfo> position(h);
		HZQDB hs[3] = { 0 };
		ZQDBGetCodeByTradeCode(*g_module, position->Code, hs, 3);
		if (hs[0]) {
			zqdb::GetApp().Goto(hs[0], wxGetTopLevelParent(this));
		}
	}
}

void MyTORAUserPositionView::OnActivated(wxDataViewEvent &event)
{
	auto h = ctrl_list_model_->GetData(event.GetItem());
	if (h) {
		zqdb::ObjectT<tagPositionInfo> position(h);
		HZQDB hs[3] = { 0 };
		ZQDBGetCodeByTradeCode(*g_module, position->Code, hs, 3);
		if (hs[0]) {
			zqdb::Code code(hs[0]);
			if (wxOK == wxMessageBox(wxT("确定平仓吗？"), wxT("提示"), wxOK | wxCANCEL | wxCENTRE)) {
				zqdb::GetApp().CloseOrder(user_, h, ORDER_LIMIT, position->Volume - position->FrozenVolume, code->Close);
			}
		} else {
			if (wxOK == wxMessageBox(wxT("确定平仓吗？"), wxT("提示"), wxOK | wxCANCEL | wxCENTRE)) {
				zqdb::GetApp().CloseOrder(user_, h, ORDER_MARKET, position->Volume - position->FrozenVolume, 0);
			}
		}
	}
}

// -- MyTORAMiniView -- 

wxBEGIN_EVENT_TABLE(MyTORAMiniView, Base)
EVT_TIMER(wxID_ANY, MyTORAMiniView::OnTimer)
EVT_SHOW(MyTORAMiniView::OnShowEvent)
wxEND_EVENT_TABLE()

// DISPATCH_ON_TYPE() macro is an ugly way to write the "same" code for
// different wxBookCtrlBase-derived classes without duplicating code and
// without using templates, it expands into "before <xxx> after" where "xxx"
// part is control class-specific
#if wxUSE_NOTEBOOK
#define CASE_NOTEBOOK(x) case Type_Notebook: x; break;
#else
#define CASE_NOTEBOOK(x)
#endif

#if wxUSE_LISTBOOK
#define CASE_LISTBOOK(x) case Type_Listbook: x; break;
#else
#define CASE_LISTBOOK(x)
#endif

#if wxUSE_CHOICEBOOK
#define CASE_CHOICEBOOK(x) case Type_Choicebook: x; break;
#else
#define CASE_CHOICEBOOK(x)
#endif

#if wxUSE_TREEBOOK
#define CASE_TREEBOOK(x) case Type_Treebook: x; break;
#else
#define CASE_TREEBOOK(x)
#endif

#if wxUSE_TOOLBOOK
#define CASE_TOOLBOOK(x) case Type_Toolbook: x; break;
#else
#define CASE_TOOLBOOK(x)
#endif

#if wxUSE_AUI
#define CASE_AUINOTEBOOK(x) case Type_AuiNotebook: x; break;
#else
#define CASE_AUINOTEBOOK(x)
#endif

#define CASE_SIMPLEBOOK(x) case Type_Simplebook: x; break;

#define DISPATCH_ON_TYPE(before, nb, lb, cb, tb, toolb, aui, sb, after)       \
    switch ( m_type )                                                         \
    {                                                                         \
        CASE_NOTEBOOK(before nb after)                                        \
        CASE_LISTBOOK(before lb after)                                        \
        CASE_CHOICEBOOK(before cb after)                                      \
        CASE_TREEBOOK(before tb after)                                        \
        CASE_TOOLBOOK(before toolb after)                                     \
        CASE_AUINOTEBOOK(before aui after)                                    \
        CASE_SIMPLEBOOK(before sb after)                                      \
                                                                              \
        default:                                                              \
            wxFAIL_MSG( "unknown book control type" );                   \
    }

MyTORAMiniView::MyTORAMiniView(wxWindow *parent, const char* xml, size_t xmlflag) :Base(parent, xml, xmlflag)
{
#if wxUSE_NOTEBOOK
	m_type = Type_Notebook;
#elif wxUSE_CHOICEBOOK
	m_type = Type_Choicebook;
#elif wxUSE_LISTBOOK
	m_type = Type_Listbook;
#elif wxUSE_TREEBOOK
	m_type = Type_Treebook;
#elif wxUSE_TOOLBOOK
	m_type = Type_Toolbook;
#elif wxUSE_AUI
	m_type = Type_Aui;
#else
	m_type = Type_Simplebook;
#endif
	m_type = Type_Listbook;

	m_orient = ID_ORIENT_DEFAULT;
	m_chkShowImages = true;
	m_fixedWidth = false;
	m_multi = false;
	m_noPageTheme = false;
	m_buttonBar = false;
	m_horzLayout = false;

	m_bookCtrl = nullptr;
	int flags;
	switch (m_orient)
	{
	case ID_ORIENT_TOP:
		flags = wxBK_TOP;
		break;

	case ID_ORIENT_BOTTOM:
		flags = wxBK_BOTTOM;
		break;

	case ID_ORIENT_LEFT:
		flags = wxBK_LEFT;
		break;

	case ID_ORIENT_RIGHT:
		flags = wxBK_RIGHT;
		break;

	default:
		flags = wxBK_DEFAULT;
	}

#if wxUSE_NOTEBOOK
	if (m_fixedWidth && m_type == Type_Notebook)
		flags |= wxNB_FIXEDWIDTH;
	if (m_multi && m_type == Type_Notebook)
		flags |= wxNB_MULTILINE;
	if (m_noPageTheme && m_type == Type_Notebook)
		flags |= wxNB_NOPAGETHEME;
#endif
#if wxUSE_TOOLBOOK
	if (m_buttonBar && m_type == Type_Toolbook)
		flags |= wxTBK_BUTTONBAR;
	if (m_horzLayout && m_type == Type_Toolbook)
		flags |= wxTBK_HORZ_LAYOUT;
#endif

	if (!m_bookCtrl) {
		DISPATCH_ON_TYPE(m_bookCtrl = new,
			wxNotebook,
			wxListbook,
			wxChoicebook,
			wxTreebook,
			wxToolbook,
			wxAuiNotebook,
			wxSimplebook,
			(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, flags));
	}

	// wxToolbook doesn't work without icons so always use them for it.
	/*if (m_chkShowImages || m_type == Type_Toolbook)
	{
		m_bookCtrl->SetImageList(m_imageList);
	}*/

	info_view_ = new MyTORAUserInfoView(m_bookCtrl);
	order_view_ = new MyTORAUserOrderView(m_bookCtrl);
	trade_view_ = new MyTORAUserTradeView(m_bookCtrl);
	position_view_ = new MyTORAUserPositionView(m_bookCtrl);
	m_bookCtrl->AddPage(info_view_, wxT("账户"), false);
	m_bookCtrl->AddPage(order_view_, wxT("委托"), true);
	m_bookCtrl->AddPage(trade_view_, wxT("成交"), false);
	m_bookCtrl->AddPage(position_view_, wxT("持仓"), false);

	wxSizer* topSizer = new wxBoxSizer(wxVERTICAL);

	topSizer->Add(m_bookCtrl, 1, wxEXPAND);

	SetSizer(topSizer);
}

void MyTORAMiniView::OnSkinInfoChanged()
{
	Base::OnSkinInfoChanged();
	if (info_view_) {
		info_view_->SetSkinInfo(GetSkinInfo());
	}
	if (order_view_) {
		order_view_->SetSkinInfo(GetSkinInfo());
	}
	if (trade_view_) {
		trade_view_->SetSkinInfo(GetSkinInfo());
	}
	if (position_view_) {
		position_view_->SetSkinInfo(GetSkinInfo());
	}
}

void MyTORAMiniView::OnHandleChanged()
{
	Base::OnHandleChanged();
	if (info_view_) {
		info_view_->SetHandle(h_);
	}
	if (order_view_) {
		order_view_->SetHandle(h_);
	}
	if (trade_view_) {
		trade_view_->SetHandle(h_);
	}
	if (position_view_) {
		position_view_->SetHandle(h_);
	}
}

void MyTORAMiniView::OnUserChanged()
{
	Base::OnUserChanged();
	if (info_view_) {
		info_view_->SetUser(user_);
	}
	if (order_view_) {
		order_view_->SetUser(user_);
	}
	if (trade_view_) {
		trade_view_->SetUser(user_);
	}
	if (position_view_) {
		position_view_->SetUser(user_);
	}
}

void MyTORAMiniView::OnNotifyStatus(HZQDB h)
{
	switch (h->type)
	{
	case ZQDB_HANDLE_TYPE_TABLE_DATA: {
		if (h->type == ZQDB_HANDLE_TYPE_MODULE && h == *g_module) {
			auto book_page = m_bookCtrl->GetCurrentPage();
			if (book_page == order_view_) {
				if (ZQDBIsDisable(h))
					order_view_->ClearAll();
				else
					order_view_->ShowAll();
			}
			else if (book_page == trade_view_) {
				if (ZQDBIsDisable(h))
					trade_view_->ClearAll();
				else
					trade_view_->ShowAll();
			}
			else if (book_page == position_view_) {
				if (ZQDBIsDisable(h))
					position_view_->ClearAll();
				else
					position_view_->ShowAll();
			}
		}
	} break;
	default:
		break;
	}
}

void MyTORAMiniView::OnNotifyAdd(HZQDB h)
{
	switch (h->type)
	{
	case ZQDB_HANDLE_TYPE_TABLE_DATA: {
		if (ZQDBGetModule(h) != *g_module) {
			break;
		}
		auto book_page = m_bookCtrl->GetCurrentPage();
		if (book_page == order_view_ && ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_ORDER)) {
			order_view_->ShowAll();
		}
		else if (book_page == trade_view_ && ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_TRADE)) {
			trade_view_->ShowAll();
		}
		else if (book_page == position_view_ && ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_POSITION)) {
			position_view_->ShowAll();
		}
	} break;
	default:
		break;
	}
}

void MyTORAMiniView::OnNotifyUpdate(HZQDB h)
{
	switch (h->type)
	{
	case ZQDB_HANDLE_TYPE_TABLE_DATA: {
		if (ZQDBGetModule(h) != *g_module) {
			break;
		}
		auto book_page = m_bookCtrl->GetCurrentPage();
		if (book_page == order_view_ && ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_ORDER)) {
			order_view_->RefreshAll();
		}
		else if (book_page == trade_view_ && ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_TRADE)) {
			trade_view_->RefreshAll();
		}
		else if (book_page == position_view_ && ZQDBIsDataOfTable(h, STR_ZQDB_TABLE_POSITION)) {
			position_view_->RefreshAll();
		}
	} break;
	default:
		break;
	}
}

void MyTORAMiniView::ShowView(wxWindow* page)
{
	//切换显示视图
	auto old_page = m_bookCtrl->GetCurrentPage();
	if (old_page != page) {
		for (size_t i = 0, j = m_bookCtrl->GetPageCount(); i < j; i++)
		{
			if (m_bookCtrl->GetPage(i) == page)
			{
				m_bookCtrl->SetSelection(i);
				break;
			}
		}
	}
}

void MyTORAMiniView::OnTimer(wxTimerEvent& event)
{
}

void MyTORAMiniView::OnShowEvent(wxShowEvent& evt)
{
	
}

// -- MyTORAModule --

MyTORAModule::MyTORAModule(HZQDB h) :Base(h)
{
	g_module = this; 
	RefreshBaseInfo();
}

MyTORAModule::~MyTORAModule()
{
	g_module = nullptr;
}
//
//size_t MyTORAModule::OrderUnitMultiple(HZQDB h)
//{
//	if (!h) {
//		return 1;
//	}
//	switch (h->type)
//	{
//	case ZQDB_HANDLE_TYPE_CODE: {
//		zqdb::Code code(h);
//		switch (code->Type)
//		{
//		case CODE_TYPE_Stock: {
//			return 100;
//		} break;
//		case CODE_TYPE_Bond: {
//			return 10;
//		} break;
//		default:
//			break;
//		}
//	} break;
//	}
//	return 1;
//}

void MyTORAModule::RefreshBaseInfo()
{
	for (auto sz_exchange : sz_exchanges)
	{
		zqdb::Exchange exchange(sz_exchange);
		if (exchange) {
			auto tmax_time_point = ZQDBGetMaxTimePointCount(exchange, CYC_1MIN);
			if (tmax_time_point > max_time_point_) {
				max_time_point_ = tmax_time_point;
			}
		}
	}
}

void MyTORAModule::ClearBaseInfo()
{
	max_time_point_ = 0;
}

uint32_t MyTORAModule::GetTradeTime(uint32_t* date, uint32_t* tradeday)
{
	for (auto sz_exchange : sz_exchanges)
	{
		zqdb::Exchange exchange(sz_exchange);
		if (exchange) {
			auto time = ZQDBGetNowTime(exchange, date, tradeday);
			if (time) {
				return time;
			}
		}
	}
	return 0;
}

wxString MyTORAModule::GetUserInfo(HZQDB huser)
{
	if (!huser) {
		return wxEmptyString;
	}
	zqdb::ObjectT<tagTORAUserInfo> user(huser);
	double Static = 0;//PreDeposit - Withdraw + Deposit;
	double Dynamic = 0;//CloseProfit + PositionProfit - Commission;
	double Available = 0;//;
	double FetchLimit = 0;//;
	zqdb::AllUserAccount allaccount(huser, *g_module);
	for (auto haccount : allaccount)
	{
		zqdb::ObjectT<tagTORAAccountInfo> account(haccount);
		if (strcmp(account->User, user->User) == 0 && strcmp(account->Broker, user->Broker) == 0) {
			auto tBalance = account->Balance;
			auto tAvailable = account->UsefulMoney;
			auto tFetchLimit = account->FetchLimit;
			Static += account->PreBalance - account->Withdraw + account->Deposit;
			Dynamic += tBalance; //Static + account->CloseProfit + account->PositionProfit - account->Commission;
			Available += tAvailable;
			FetchLimit += tFetchLimit;
			break;
		}
	}
	return wxString::Format(wxT("总资产:%.2f 可用:%.2f 可取:%.2f 持仓盈亏:%.2f"), Dynamic, Available, FetchLimit, Dynamic - Static);
}

MyLoginView* MyTORAModule::NewLoginView(wxWindow* parent, const char* xml, size_t xmlflag)
{
	return new MyTORALoginView(parent, xml, xmlflag);
}

MyMiniView* MyTORAModule::NewMiniView(wxWindow* parent, const char* xml, size_t xmlflag) 
{
	/*if (view == wxT("账户")) {
		return new MyTORAUserInfoView(parent, xml, xmlflag);
	}
	else if (view == wxT("持仓")) {
		return new MyTORAUserPositionView(parent, xml, xmlflag);
	}*/
	return new MyTORAMiniView(parent, xml, xmlflag);
}

HZQDB MyTORAModule::ReqTestUser(double amount)
{
	HNMSG hrsp = nullptr;
	zqdb::Msg msg(ZQDB_MSG_REQUEST_DO);
	msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_USER_LOGIN);
	msg.SetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, BROKER), ZQDB_USER_BROKER_TEST);
	msg.SetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, USER), (ZQDB_USER_BROKER_TEST + std::to_string(req_test_user_id_++)).c_str());
	msg.SetParamAsDouble("Amount", amount);
	ZQDBRequestEx(h_, msg, &hrsp, 3000);
	if (hrsp) {
		zqdb::Msg rsp(hrsp, zqdb::Msg::AutoDelete);
		//auto errorcode = rsp.GetParamAsInt(STR_ZQDB_MSG_ERROR_CODE, 0);
		//auto errormsg = utf2wxString(rsp.GetParam(STR_ZQDB_MSG_ERROR_MESSAGE, ""));
		return (HZQDB)(void*)rsp.GetParamAsUInt("HZQDB");
	}
	return nullptr;
}

void MyTORAModule::ReqTestUserUpdate(HZQDB huser, uint32_t date, uint32_t time)
{
	zqdb::ObjectT<tagUserInfo> user(huser);
	zqdb::Msg msg(ZQDB_MSG_REQUEST_DO);
	msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_USER_LOGIN);
	msg.SetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, BROKER), user->Broker);
	msg.SetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, USER), user->User);
	msg.SetParamAsUInt("Date", date);
	msg.SetParamAsUInt("Time", time);
	ZQDBRequestEx(h_, msg, nullptr, 0);
}

//
//int MyTORAModule::ReqModifyPassword(HZQDB huser, const char* old_pwd, const char* new_pwd, HNMSG* rsp, size_t timeout, size_t flags)
//{
//	zqdb::ObjectT<tagUserInfo> user(huser);
//	zqdb::Msg msg(ZQDB_MSG_REQUEST_DO);
//	msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_USER_MODIFY_PASSWORD);
//	com::zqdb::proto::msg::ReqModifyPassword req;
//	req.set_broker(user->Broker);
//	//req.set_investor(user->)
//	req.set_user(user->User);
//	req.set_oldpassword(old_pwd);
//	req.set_newpassword(new_pwd);
//	auto str = req.SerializeAsString();
//	msg.SetData(str.data(), str.size());
//	return ZQDBRequestEx(user, msg, rsp, timeout);
//}
//
//int MyTORAModule::ReqModifyAccountPassword(HZQDB haccount, const char* old_pwd, const char* new_pwd, HNMSG* rsp, size_t timeout, size_t flags)
//{
//	zqdb::ObjectT<tagTORAAccountInfo> account(haccount);
//	zqdb::Msg msg(ZQDB_MSG_REQUEST_DO);
//	msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_USER_MODIFY_ACCOUNT_PASSWORD);
//	com::zqdb::proto::msg::ReqModifyAccountPassword req;
//	req.set_broker(account->Broker);
//	//req.set_investor(user->)
//	req.set_user(account->User);
//	req.set_account(account->Account);
//	req.set_currency(account->CurrencyID);
//	req.set_oldpassword(old_pwd);
//	req.set_newpassword(new_pwd);
//	auto str = req.SerializeAsString();
//	msg.SetData(str.data(), str.size());
//	return ZQDBRequestEx(account, msg, rsp, timeout);
//}

int MyTORAModule::OrderSend(HZQDB huser, HZQDB hcode, char direction, char offset, char type, double volume, double price, HNMSG* rsp, size_t timeout, size_t flags)
{
	zqdb::Code code(hcode);
	zqdb::ObjectT<tagUserInfo> user(huser);
	zqdb::Msg msg(ZQDB_MSG_REQUEST_DO);
	msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_ORDER_INSERT);
	//msg.SetParam("str", "1234567");
	//msg.SetParam("i64", "1234567");
	//msg.SetParam("f", "1234567.89");
	com::zqdb::proto::msg::ReqOrderInsert req;
	req.set_broker(user->Broker);
	//req.set_investor(user->)
	req.set_user(user->User);
	req.set_exchange(code->Exchange);
	req.set_code(code->TradeCode);
	req.set_price(price);
	req.set_volume(volume);
	req.set_direction(direction); //DIRECTION_LONG
	req.set_offset(offset); //OFFSET_OPEN
	req.set_type(type); //ORDER_LIMIT
	auto str = req.SerializeAsString();
	msg.SetData(str.data(), str.size());
	return ZQDBRequestEx(user, msg, rsp, timeout);
	/*if (rsp) {
		zqdb::Msg rsp_msg(rsp,zqdb::Msg::AutoDelete);
		auto errorcode = msg.GetParamAsInt(STR_ZQDB_MSG_ERROR_CODE, 0);
		auto errormsg = msg.GetParam(STR_ZQDB_MSG_ERROR_MESSAGE,"");
		if (!errorcode) {
			strcpy(orderid, rsp_msg.GetParam(STR_MDB_FIELD_INDEX(ZQDB, ORDER, ORDER), ""));
		}
		return 0;
	}*/
#ifdef _DEBUG
	com::zqdb::proto::msg::TestMessage test_msg;
	test_msg.set_msg(ZQDB_MSG_REQUEST_DO);
	test_msg.set_type(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_ORDER_INSERT);
	test_msg.set_id(ZQDBReqID());
	(*test_msg.mutable_param())["str"] = "1234567";
	(*test_msg.mutable_param())["u32"] = "1234567";
	(*test_msg.mutable_param())["i32"] = "1234567";
	(*test_msg.mutable_param())["u64"] = "1234567";
	(*test_msg.mutable_param())["i64"] = "1234567";
	(*test_msg.mutable_param())["f32"] = "1234567.89";
	(*test_msg.mutable_param())["f64"] = "1234567.89";
	auto test_msg_str = test_msg.SerializeAsString();
	double test_f64 = std::stod((*test_msg.mutable_param())["f64"]);

	com::zqdb::proto::msg::VarMessage var_msg;
	var_msg.set_msg(ZQDB_MSG_REQUEST_DO);
	var_msg.set_type(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_ORDER_INSERT);
	var_msg.set_id(ZQDBReqID());
	com::zqdb::proto::msg::Variant var_val;
	var_val.set_str("1234567");
	(*var_msg.mutable_param())["str"] = var_val;
	var_val.set_u32(1234567);
	(*var_msg.mutable_param())["u32"] = var_val;
	var_val.set_i32(1234567);
	(*var_msg.mutable_param())["i32"] = var_val;
	var_val.set_u64(1234567);
	(*var_msg.mutable_param())["u64"] = var_val;
	var_val.set_i64(1234567);
	(*var_msg.mutable_param())["i64"] = var_val;
	var_val.set_f32(1234567.89);
	(*var_msg.mutable_param())["f32"] = var_val;
	var_val.set_f64(1234567.89);
	(*var_msg.mutable_param())["f64"] = var_val;
	auto var_msg_str = var_msg.SerializeAsString();
	double var_f64 = (*var_msg.mutable_param())["f64"].f64();
#endif//
	return 0;
}

int MyTORAModule::OrderCancel(HZQDB huser, HZQDB horder, HNMSG* rsp, size_t timeout, size_t flags)
{
	zqdb::ObjectT<tagOrderInfo> order(horder);
	zqdb::Msg msg(ZQDB_MSG_REQUEST_DO);
	msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_ORDER_CANCEL);
	com::zqdb::proto::msg::ReqOrderCancel req;
	req.set_broker(order->Broker);
	req.set_user(order->User);
	req.set_exchange(order->Exchange);
	req.set_code(order->Code);
	req.set_orderid(order->Order);
	auto str = req.SerializeAsString();
	msg.SetData(str.data(), str.size());
	return ZQDBRequestEx(huser, msg, rsp, timeout);
}

int MyTORAModule::OrderClose(HZQDB huser, HZQDB hposition, char type, double volume, double price, HNMSG* rsp, size_t timeout, size_t flags)
{
	zqdb::ObjectT<tagPositionInfo> position(hposition);
	zqdb::ObjectT<tagUserInfo> user(huser);
	zqdb::Msg msg(ZQDB_MSG_REQUEST_DO);
	msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_DO_TD_ORDER_INSERT);
	com::zqdb::proto::msg::ReqOrderInsert req;
	req.set_broker(user->Broker);
	//req.set_investor(user->)
	req.set_user(user->User);
	req.set_exchange(position->Exchange);
	req.set_code(position->Code);
	req.set_price(price);
	req.set_volume(volume);
	switch (position->Direction)
	{
	case DIRECTION_LONG: {
		req.set_direction(DIRECTION_SHORT);
	} break;
	case DIRECTION_SHORT: {
		req.set_direction(DIRECTION_LONG);
	} break;
	default: {
		ASSERT(0);
	} break;
	}
	req.set_offset(OFFSET_CLOSE); //OFFSET_OPEN
	req.set_type(type); //ORDER_LIMIT
	auto str = req.SerializeAsString();
	msg.SetData(str.data(), str.size());
	return ZQDBRequestEx(huser, msg, rsp, timeout);
}

int MyTORAModule::ReqQryMaxOrderVolume(HZQDB huser, HZQDB hcode, char direction, char offset, char type, HNMSG* rsp, size_t timeout, size_t flags) 
{ 
	zqdb::Code code(hcode);
	zqdb::ObjectT<tagUserInfo> user(huser);
	zqdb::Msg msg(ZQDB_MSG_REQUEST_QUERY);
	msg.SetReqID(com::zqdb::proto::msg::MSG_REQUEST_QUERY_TD_ORDER_MAX_ORDER_VOLUME);
	com::zqdb::proto::msg::ReqOrderInsert req;
	req.set_broker(user->Broker);
	//req.set_investor(user->)
	req.set_user(user->User);
	req.set_exchange(code->Exchange);
	req.set_code(code->TradeCode);
	req.set_direction(direction); //DIRECTION_LONG
	req.set_offset(offset); //OFFSET_OPEN
	req.set_type(type); //ORDER_LIMIT
	auto str = req.SerializeAsString();
	msg.SetData(str.data(), str.size());
	return ZQDBRequestEx(user, msg, rsp, timeout);
}

void MyTORAModule::OnTimer()
{
}

void MyTORAModule::OnNotifyStatus(HZQDB h)
{
	if (h == this->h_) {
		if (ZQDBIsDisable(h))
			ClearBaseInfo();
		else
			RefreshBaseInfo();
	}
}

void MyTORAModule::OnNotifyAdd(HZQDB h)
{

}

void MyTORAModule::OnNotifyUpdate(HZQDB h)
{

}

int MyTORAModule::OnNetMsg(zqdb::Msg& msg)
{
	int ret = 0;
	auto type = msg.GetMsgType();
	auto reqtype = msg.GetReqType();
	switch (type)
	{
	case ZQDB_MSG_NOTIFY: {
		switch (reqtype)
		{
		case com::zqdb::proto::msg::MSG_NOTIFY_TD_USER_LOGIN: {
			//收到登录通知，登录成功还是失败参看errorcode，提示参看errormsg
			auto user = msg.GetParam(STR_MDB_FIELD_INDEX(ZQDB, USER, USER));
			auto errorcode = msg.GetErrorCode();
			auto errormsg = msg.GetErrorMsg();
			if (errorcode) {
				//wxLogError(wxT("%s 登录失败:[%zd]%s"), user, errorcode, errormsg);
			}
			else {
				//wxLogInfo(wxT("%s 登录成功"), user);
			}
			ret = 1;
		} break;
		}
	} break;
	case ZQDB_MSG_REQUEST_DO: {
		//
	} break;
	case ZQDB_MSG_REQUEST_QUERY: {
		//
	} break;
	case ZQDB_MSG_RESPONSE: {
		switch (reqtype)
		{
		case com::zqdb::proto::msg::MSG_REQUEST_DO_TD_USER_LOGIN: {
			//发送登录请求成功
			ret = 1;
		} break;
		}
	} break;
	default:
		break;
	}
	return ret;
	return 0;
}
