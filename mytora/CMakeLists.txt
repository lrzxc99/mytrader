cmake_minimum_required(VERSION 3.4.1)

MESSAGE(STATUS "This is mytora PROJECT BINARY dir " ${PROJECT_BINARY_DIR})
MESSAGE(STATUS "This is mytora CMAKE BINARY dir " ${CMAKE_BINARY_DIR})
MESSAGE(STATUS "This is mytora PROJECT SOURCE dir " ${PROJECT_SOURCE_DIR})
MESSAGE(STATUS "This is mytora CMAKE SOURCE dir " ${CMAKE_SOURCE_DIR})

MESSAGE(STATUS "This is mytora current BINARY dir " ${CMAKE_CURRENT_BINARY_DIR})
MESSAGE(STATUS "This is mytora current SOURCE dir " ${CMAKE_CURRENT_SOURCE_DIR})
MESSAGE(STATUS "This is mytora current CMakeList.txt dir " ${CMAKE_CURRENT_LIST_DIR})
MESSAGE(STATUS "This is mytora current CMakeList.txt line " ${CMAKE_CURRENT_LIST_LINE})

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
MESSAGE(STATUS "CMAKE_C_FLAGS: ${CMAKE_C_FLAGS}")
MESSAGE(STATUS "CMAKE_CXX_FLAGS: ${CMAKE_CXX_FLAGS}")

IF(CMAKE_BUILD_TYPE STREQUAL Debug)
add_definitions(-D_DEBUG)
ENDIF()

if (MSVC)
    set(CompilerFlags
        CMAKE_CXX_FLAGS
        CMAKE_CXX_FLAGS_DEBUG
        CMAKE_CXX_FLAGS_MINSIZEREL
        CMAKE_CXX_FLAGS_RELEASE
        CMAKE_CXX_FLAGS_RELWITHDEBINFO
        CMAKE_C_FLAGS
        CMAKE_C_FLAGS_DEBUG
        CMAKE_C_FLAGS_MINSIZEREL
        CMAKE_C_FLAGS_RELEASE
        CMAKE_C_FLAGS_RELWITHDEBINFO
        )
    foreach(CompilerFlag ${CompilerFlags})
        string(REPLACE "/MD" "/MT" ${CompilerFlag} "${${CompilerFlag}}")
    endforeach()
endif(MSVC)

# add location of platform.hpp for Windows builds
if(WIN32)
  add_definitions(-D_WINSOCK_DEPRECATED_NO_WARNINGS)
  add_definitions(-DWIN32 -D_WINDOWS)
  add_definitions(-D_CRT_SECURE_NO_DEPRECATE=1)
  add_definitions(-D_CRT_NON_CONFORMING_SWPRINTFS=1)
  add_definitions(-D_SCL_SECURE_NO_WARNINGS=1)
  add_definitions(-D__WXMSW__)
  add_definitions(-DNOPCH)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /bigobj")
  # Same name on 64bit systems
  link_libraries(ws2_32.lib)
  SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /subsystem:windows") 
endif()

IF(WIN32)
	#需要兼容XP时,定义_WIN32_WINNT 0x0501
	ADD_DEFINITIONS(-D_WIN32_WINNT=0x0602)
	ADD_DEFINITIONS(-DLIB_ZQDB_API)
	#SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
	#SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
	#SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} /MT")
	#SET(CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} /MT")
ELSE()
	ADD_DEFINITIONS(-fpermissive)
	ADD_DEFINITIONS(-fPIC)
	#-w的意思是关闭编译时的警告，也就是编译后不显示任何warning
	#-Wall选项意思是编译后显示所有警告
	#-W选项类似-Wall，会显示警告，但是只显示编译器认为会出现错误的警告
	#调试信息格式主要有下面几种：stabs，COFF，PE-COFF，OMF，IEEE-695和DWARF
	#其中DWARF在Linux中被普遍使用，dwarf2对dwarf1的改变很大，dwarf3大多是对dwarf2的扩充，可以支持C、C++、JAVA、Fortran等语言
	#使用readelf –w* transfer命令，*是调试节名的第一个字母，如-wi就是查看.debug_info节的内容，-wl就是查看.debug_line节的内容
	#-g、-ggdb、-g3和-ggdb3，-g产生OS native format的debug信息，GDB可以使用之。而-ggdb产生的debug信息更倾向于给GDB使用的
	#如果你用的GDB调试器，那么使用-ggdb选项。如果是其他调试器，则使用-g。3只是debug信息级别，3这个级别可以调试宏。
	#SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -w -gdwarf-2 -ggdb3")
	#SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O2 -Wall -DNODEBUG -gdwarf-2 -ggdb")
	SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -W -g3")
	SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3 -W")
	SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -O2 -W -gdwarf-3 -g")
	SET(CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} -Os -W")
ENDIF()
CMAKE_POLICY(SET CMP0015 NEW)

#SET(BOOST_COMPONENTS log_setup log regex date_time chrono system filesystem thread)
FIND_PACKAGE(Boost 
#REQUIRED COMPONENTS ${BOOST_COMPONENTS}
)
IF(Boost_FOUND)
	MESSAGE(STATUS "boost library status:")
	MESSAGE(STATUS "		 version: ${Boost_VERSION}")
ELSE()
	MESSAGE(STATUS "BOOST library not found")
	if(BOOST_ROOT)
		SET(Boost_INCLUDE_DIRS ${BOOST_ROOT})
		SET(Boost_LIBRARY_DIRS ${BOOST_ROOT}/lib)
	else()
		SET(Boost_INCLUDE_DIRS ../../boost/include)
		SET(Boost_LIBRARY_DIRS ../../boost/lib)
	endif()
ENDIF()
IF(WIN32)
	IF(CMAKE_CL_64)
		SET(Boost_LIBRARY_DIRS "${Boost_INCLUDE_DIRS}/lib64-msvc-14.0")
	ELSE()
		SET(Boost_LIBRARY_DIRS "${Boost_INCLUDE_DIRS}/lib32-msvc-14.0")
	ENDIF()
ENDIF()
MESSAGE(STATUS "		 include path: ${Boost_INCLUDE_DIRS}")
MESSAGE(STATUS "		 library path: ${Boost_LIBRARY_DIRS}")
MESSAGE(STATUS "		 libraries: ${Boost_LIBRARIES}")
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
LINK_DIRECTORIES(${Boost_LIBRARY_DIRS})
SET(EXTRA_LIBS ${EXTRA_LIBS} ${Boost_LIBRARIES})

find_package(Protobuf REQUIRED)
IF(PROTOBUF_FOUND)
	MESSAGE(STATUS "protobuf library status:")
	MESSAGE(STATUS "     version: ${PROTOBUF_VERSION}")
	MESSAGE(STATUS "     include path: ${PROTOBUF_INCLUDE_DIR}")
	MESSAGE(STATUS "     library path: ${PROTOBUF_LIBRARIES}")
	INCLUDE_DIRECTORIES(${PROTOBUF_INCLUDE_DIR})
ELSE()
	MESSAGE(FATAL_ERROR "protobuf library not found")
ENDIF()

find_package(wxWidgets COMPONENTS core base aui ribbon webview adv xml stc scintilla)
if(wxWidgets_FOUND)
	MESSAGE(STATUS "     include path: ${wxWidgets_USE_FILE}")
	MESSAGE(STATUS "     library path: ${wxWidgets_LIBRARIES}")
  include(${wxWidgets_USE_FILE})
  LINK_DIRECTORIES(${wxWidgets_LIBRARIES})
  #wxFreeChart
  INCLUDE_DIRECTORIES(../../zqdb/3rd/wxFreeChart/include)
	IF(WIN32)
		IF(CMAKE_CL_64)
			LINK_DIRECTORIES(../../zqdb/3rd/wxFreeChart/lib/vc_x64_lib)
		ELSE()
			LINK_DIRECTORIES(../../zqdb/3rd/wxFreeChart/lib/vc_lib)
		ENDIF()
	ELSE()
		MESSAGE(FATAL_ERROR "wxFreeChart library not found")
	ENDIF()
ELSE()
	MESSAGE(FATAL_ERROR "wxWidgets library not found")
ENDIF()

if(CEF_ON)
#wxWebViewChromium
add_definitions(-DCEF_ON)
SET(CEF_WEBVIEW_DIR ../../wxWebViewChromium)
SET(CEF_WEBVIEW_CEF_DIR ../../wxWebViewChromium/${CMAKE_SYSTEM_NAME}/${PLATFORM})
INCLUDE_DIRECTORIES(${CEF_WEBVIEW_DIR} ${CEF_WEBVIEW_CEF_DIR})
endif()

IF(WIN32)
	INCLUDE_DIRECTORIES(../../zqdb/3rd ../../mytrader/include ../include ../src)
	# LINK_DIRECTORIES(${CMAKE_CURRENT_LIST_DIR}/../../mytrader/bin/${CMAKE_SYSTEM_NAME}/${PLATFORM})
	SET (EXTRA_LIBS ${EXTRA_LIBS} zqdb)
ELSE()
	INCLUDE_DIRECTORIES(../../zqdb/3rd ../../mytrader/include ../include ../src)
	# LINK_DIRECTORIES(${CMAKE_CURRENT_LIST_DIR}/../../mytrader/bin/${CMAKE_SYSTEM_NAME}/${PLATFORM})
	SET (EXTRA_LIBS ${EXTRA_LIBS} zqdb pthread dl)
ENDIF()

add_library( # Sets the name of the library.
		mytora

        # Sets the library as a shared library.
        STATIC

        # Provides a relative path to your source file(s).
		../../mytrader/src/wxutil.cpp
		../../mytrader/include/zqdbase.pb.cc
		../../mytrader/include/zqdb.pb.cc
		./mytora.cpp
        )
# TARGET_LINK_LIBRARIES(mytora PRIVATE ${EXTRA_LIBS})

SET(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_LIST_DIR}/../../mytrader/bin/${CMAKE_SYSTEM_NAME}/${PLATFORM})




