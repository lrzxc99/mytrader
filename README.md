# mytrader 

#### 介绍

欢迎使用`mytrader`开源量化分析交易平台，我们致力于为量化交易、算法交易、程序化交易以及技术分析爱好者打造最极致的行情分析交易平台。

`mytrader`官网：[www.mytrader.org.cn](https://www.mytrader.org.cn/)，您可以在官网下载最新版安装包安装体验

您也可以在这里下载最新发布的版本以及开发包：https://gitee.com/7thTool/mytrader/releases

![](./assets/mytrader.gif)

#### 教程指引

0. [序言](./book/zh-cn/00.md)
1. [证券DB技术架构](./book/zh-cn/01.md)
2. [证券DB应用开发演示](./book/zh-cn/02.md)
2. [mytrader技术架构](./book/zh-cn/03.md)
4. [mytrader安装和设置](./book/zh-cn/04.md)
5. [mytrader使用入门](./book/zh-cn/05.md)
6. [mytrader量化入门](./book/zh-cn/06.md)

#### 技术特性

##### 可视 行情/分析/交易/算法

`mytrader`强大的数据展示和管理能力让我们变得与众不同。
1.    您可以实时浏览和管理行情快照/Tick/K线/指标/筛选/排序/策略/算法以及交易数据。
2.    您还可以基于我们强大的可视化能力，以可视可信的方式验证您的交易策略。

##### 模块化 全方位定制

`mytrader`高度模块化的设计，让我们可以满足您的全方位定制开发需求。
1.    支持三方接入定制开发。
2.    支持计算模块定制开发。
3.    支持C/C++/Python/Excel/VBA/麦语言等定制开发自有的行情/交易/策略系统。
4.    支持GUI界面定制。

##### 无服务 实时推送/本地存储

`mytrader`全市场实时行情和交易数据推送，让我们可以在本地环境实时计算存储行情和交易数据，无需中间服务器处理。
1.    没有中间环节，您的交易策略总是快人一步。
2.    没有中间环节，您的数据全都存储在本地，这样您还可以收盘后执行全市场级别的复盘和模拟回测。

##### 可编程 C/C++/Python/Excel/VBA/麦语言

`mytrader`强大的计算引擎支持您使用C/C++/Python/Excel/VBA/麦语言等编写和调用指标/筛选/排序/策略/算法。
1.    指标：即您可以编写类似MA、MACD、KDJ等主图、辅图指标。
2.    筛选：即您可以编写选股算法，筛选出心仪的代码。
3.    排序：即您可以编写类似涨跌幅、涨跌速等排序算法。
4.    脚本：即您可以编写快速执行算法，以便人工盯盘时抓住时机。
5.    策略/算法：即您可以编写后台策略/算法，以实现策略/算法交易。

##### 多窗口/多策略

`mytrader`多窗口/多策略设计让您可以同时处理更多的事务。
1.    您可以同时打开多个行情交易界面，并将其拖放到不同的屏幕上，以实现跨屏分析交易。
2.    你可以同时运行多个策略/算法，以实现多策略/算法分析交易。

##### 安全 算法全部本地存储

`mytrader`将您的算法的存储和计算都放在本地，您的算法永远只属于您。

##### 多模式 单机/客户端/服务器

`mytrader`支持多种部署运行模式，您总能找到一个适合您的部署运行模式。
1.    单机：不依赖后台服务，只接入三方API，可以单机托管运行。
2.    服务器：可以使用`mytrader`自建数据源服务。
3.    客户端：可以作为类似其他股票期货软件一样的客户端运行，可以选择连接官方数据源，也可以选择连接自建数据源。

##### 实盘/仿真/回测

`mytrader`支持实盘和仿真行情交易，并且全都支持超级回测。
1.    实盘：`mytrader`通过接入三方实盘API以及提供实盘行情交易数据，为用户提供全市场实时数据。
2.    仿真：`mytrader`通过接入三方仿真API以及提供仿真行情交易数据，为用户提供全市场仿真数据。
3.    超级回测：`mytrader`提供全市场级别的超级回测，可以为用户提供更高级别的回放和回测服务，这完全不同于其他股票期货软件的单品种简单回放。

#### 如何使用

##### 人工盯盘

人工筛选->人工盯盘->人工交易

1.    选择交易范围，通过`mytrader`的筛选功能，锁定交易范围
2.    使用实时排序功能和技术分析画面实时盯盘
3.    手动交易或者自动化脚本交易

##### 自动化交易

人工筛选->策略盯盘->策略交易

1.    选择交易范围，通过`mytrader`的筛选功能，锁定交易范围
2.    通过`mytrader`的公式系统选择策略并运行
3.    策略自动化交易
4.    `mytrader`点击策略可以查看策略实时运行状态

##### 算法编写/管理/回测

编写策略/管理策略/策略回测

1.    编写策略，通过`mytrader`或者三方编辑器编写算法
2.    管理策略，用户编写的Python策略默认存储在`mytrader`的`pycalc/src`目录，`mytrader`启动时会自动加载pycalc下的所有算法，用户可以使用`mytrader`查看算法、修改参数、运行算法等。
3.    策略回测，用户可以使用`mytrader`执行回测验证

##### 数据服务器

自建行情交易数据服务器

1.    数据源，使用`mytrader`的源服务模式，构建自有的行情交易数据源。
2.    中继服务，使用`mytrader`的中继服务模式，构建行情交易数据级联服务，更好的分发数据。

##### 数据接口

`mytrader`支持C/C++/Python/Excel/VBA/麦语言等，C/C++/Python/Excel/VBA/麦语言工程师可以将`mytrader`作为数据接口使用。

1.    获取数据，支持C/C++/Python/Excel/VBA/麦语言直接获取`mytrader`行情交易数据。
2.    整合数据，支持C/C++/Python/Excel/VBA/麦语言通过编写模块/筛选/排序/策略/算法等整合数据。

#### 构建工具

1. Windows下相关依赖库都是基于VS2015下编译构建的，故自行构建需要使用VS2015或者更高版本，toolset=msvc-14.0，VS2015需要Update3版本

#### 构建依赖

1.  [zqdb](https://gitee.com/7thTool/zqdb)
2.  `Python3.7`(mytrader项目bin\Windows\x64目录下已有)，您也可以使用Anaconda
3.  [wxWidgets3.1.5](https://www.wxwidgets.org),wxWidgets需编译成静态库，运行时选择MT/MTD
4.  `protobuf`(zqdb已有)
5.  [boost](https://www.boost.org/)
6.  [XUtil](https://github.com/7thTool/XUtil.git)
7.  [CMake](https://www.cmake.org/)

#### 构建步骤

1.  下载安装CMake、git等基本工具
2.  下载`zqdb`（`zqdb`、`zqview`、`mytrader`下载到同一个目录下，比如c:\work\zqdb、c:\work\zqview和c:\work\mytrader）：`git clone https://gitee.com/7thTool/zqdb`
3.  下载`zqview`（`zqdb`、`zqview`、`mytrader`下载到同一个目录下，比如c:\work\zqdb、c:\work\zqview和c:\work\mytrader）：`git clone https://gitee.com/7thTool/zqview`
4.  下载`mytrader`（`zqdb`、`zqview`、`mytrader`下载到同一个目录下，比如c:\work\zqdb、c:\work\zqview和c:\work\mytrader）：`git clone https://gitee.com/7thTool/mytrader`
5.  下载[mytrader](https://gitee.com/7thTool/mytrader/releases)安装包安装，如安装在`C:\mytrader`目录下，将`C:\mytrader`目录下的`ctp`、`ctp_t`、`mytrader`、`TORA_t`等目录复制到mytrader项目下的`bin\Windows\x64`目录下
6.  下载编译wxWidgets，wxWidgets需编译成静态库，运行时选择MT/MTD
在wxWidgets-3.1.5\build\msw目录选择wx_vc14.sln解决方案，所有工程的Debug选择MTD，Release选择MT
![](./assets/wxWidgets_MTD.png)
![](./assets/wxWidgets_MT.png)
7.  下载boost 1.79版本，这里下载boost编译好的二进制库 https://sourceforge.net/projects/boost/files/boost-binaries/1.79.0/
选择boost_1_79_0-msvc-14.0-64.exe下载安装
![](./assets/boost.png)
这里注意，如果你安装了多个版本的boost库，且配置了BOOST_ROOT环境变量，请确保BOOST_ROOT的环境变量指向的是boost 1.81，或者你直接移除BOOST_ROOT环境变量。

8.  下载XUtil到zqdb\3rd目录下：`git clone https://github.com/7thTool/XUtil.git`

9.  使用CMake gui构建mytrader，增加定义项：`CMAKE_PREFIX_PATH=/path/boost;/path/zqdb/3rd/x64-windows-static;/path/wxWidgets-3.1.5`，即增加三个依赖项（boost，zqdb自带的三方库，wxWidgets）的查找路径
![](./assets/cmake-gui.png)
点击configure按钮，选择构建64位版本
![](./assets/cmake-x64.png)
打开vs工程，选择RelWithDebInfo编译

10.  mytrader程序生成在mytrader项目的bin\Windows\x64\RelWithDebInfo和Debug目录下，双击mytrader.exe即可运行
![](./assets/output.png)
注意：调试运行mytrader需要指定工作目录为TargetDir
![](./assets/debug.png)



#### 使用说明

1.  `mymodule`是支持三方模块的封装
2.  `myctp`是基于`mymodule`的`ctp`模块封装，需要自定义ctp模块的话，可以下载`zqctp`：`git clone https://gitee.com/7thTool/zqctp`，ctp是上期技术开发的期货交易接口，[上SimNow注册仿真交易账号](https://www.simnow.com.cn/)
2.  `mytora`是基于`mymodule`的`tora`模块封装，是华鑫证券开发的股票量化交易接口，[上N视界注册仿真交易账号](https://n-sight.com.cn/)，需要注册成为专业投资者才可以使用哦
3.  `mytrader`目前支持`ctp`、`tora`模块，未来会支持更多三方模块

#### 交流

1.  QQ交流群：

    ![](./assets/qqq.png)


#### 请作者喝杯咖啡

如果你觉得项目对你有帮助,可以请作者喝杯咖啡

1. 支付宝打赏：

    ![](./assets/zfb-sqm.jpg)

2. 微信打赏：

    ![](./assets/wx-sqm.jpg)
