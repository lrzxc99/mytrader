#pragma once
#include <wx/string.h>
#include <XUtil/XXml.hpp>
#include <XUtil/XDateTime.hpp>
#include <zqstr.hpp>

wxString utf2wxString(const char* str);
std::string wxString2utf(const wxString& src);
wxString gbk2wxString(const char* str); 
std::string wxString2gbk(const wxString& src);

	/*inline string wxString2gbk(const wxString& src)
	{
		wxCSConv cvGB2312(wxT("GB2312"));
		return string(cvGB2312.cWX2MB(src.c_str()));
	}
	inline wxString gbk2wxString(const string& src)
	{
		wxCSConv cvGB2312(wxT("GB2312"));
		return wxString(cvGB2312.cMB2WX(src.c_str()));
	}

	inline string wxString2utf(const wxString& src)
	{
		wxCSConv cvutf(wxFONTENCODING_UTF8);
		return string(cvutf.cWX2MB(src.c_str()));
	}
	inline wxString utf2wxString(const string& src)
	{
		wxCSConv cvutf(wxFONTENCODING_UTF8);
		return wxString(cvutf.cMB2WX(src.c_str()));
	}*/
