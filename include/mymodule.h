#pragma once

#include <view.h>

struct AccountStatInfo
{
	time_t Time;
	double Balance;
	double Available;
	double Profit;
};

//// -- MyBaseView -- 
//
//template<class T>
//class MyBaseViewT : public zqdb::BaseViewT<T>
//{
//	typedef zqdb::BaseViewT<T> Base;
//public:
//	using Base::Base;
//
//	virtual void OnSkinInfoChanged() {}
//	virtual void OnHandleChanged() {}
//	virtual void OnNotifyEnable(HZQDB h) { }
//	virtual void OnNotifyDisable(HZQDB h) { }
//	virtual void OnNotifyAppend(HZQDB h) { }
//	virtual void OnNotifyRemove(HZQDB h) { }
//	virtual void OnNotifyUpdate(HZQDB h) { }
//};

// -- MyLoginView -- 

class MyLoginView : public zqdb::BaseViewT<MyLoginView>
{
	typedef zqdb::BaseViewT<MyLoginView> Base;
public:
	using Base::Base;

	//返回用户名 USER.MODULE，空表示不登录该模块
	virtual const char* StartLogin(std::function<void(const zqdb::Msg& rsp)>&& cb) { return nullptr; }
	virtual void CancelLogin(bool logout = true) { }

	virtual void OnSkinInfoChanged() {}
	virtual void OnHandleChanged() {}
	virtual void OnNotifyStatus(HZQDB h) { }
	virtual void OnNotifyAdd(HZQDB h) { }
	virtual void OnNotifyRemove(HZQDB h) { }
	virtual void OnNotifyUpdate(HZQDB h) { }
};

// -- MyUserViewT -- 

template<class T>
class MyUserViewT
	: public zqdb::BaseViewT<T>
	, public zqdb::UserMap<T>
{
	typedef zqdb::BaseViewT<T> Base;
	typedef zqdb::UserMap<T> UserBase;
public:
	using Base::Base;

	bool IsDispOk() const { return Base::IsDispOk() && UserBase::IsOk(); }

	void OnSkinInfoChanged() {}
	/*void OnHandleChanged() {}
	void OnNotifyEnable(HZQDB h) { }
	void OnNotifyDisable(HZQDB h) { }
	void OnNotifyAppend(HZQDB h) { }
	void OnNotifyRemove(HZQDB h) { }
	void OnNotifyUpdate(HZQDB h) { }
	void OnUserChanged() { }*/
};

// -- MyMiniView -- 

class MyMiniView
	: public MyUserViewT<MyMiniView>
{
	typedef MyUserViewT<MyMiniView> Base;
public:
	using Base::Base;

	virtual void OnSkinInfoChanged() {}
	virtual void OnHandleChanged() {}
	virtual void OnNotifyStatus(HZQDB h) { }
	virtual void OnNotifyAdd(HZQDB h) { }
	virtual void OnNotifyRemove(HZQDB h) { }
	virtual void OnNotifyUpdate(HZQDB h) { }
	virtual void OnUserChanged() { }
};

// -- MyMainView -- 

class MyMainView : public zqdb::BaseViewT<MyMainView>
{
	typedef zqdb::BaseViewT<MyMainView> Base;
public:
	using Base::Base;

	virtual void OnSkinInfoChanged() {}
	virtual void OnHandleChanged() {}
	virtual void OnNotifyStatus(HZQDB h) { }
	virtual void OnNotifyAdd(HZQDB h) { }
	virtual void OnNotifyRemove(HZQDB h) { }
	virtual void OnNotifyUpdate(HZQDB h) { }
};

// -- MyModule --

enum {
	ORDER_SEND_FLAG_NONE = 0,
	ORDER_SEND_FLAG_SHOWTIPS = 0X01,
};

class MyModule 
	: public zqdb::Module
{
	typedef zqdb::Module Base;
public:
	//static void AddModule(std::shared_ptr<MyModule> module);
	////static void RemoveModule(std::shared_ptr<MyModule> module);
	//static void RemoveAllModule();
	//static std::shared_ptr<MyModule> GetByHandle(HZQDB h);
	//static void Broadcast(const std::function<bool(std::shared_ptr<MyModule> module)>& f);
public:
	using Base::Base;

	virtual MyLoginView* NewLoginView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING) { return nullptr; }
	virtual MyMiniView* NewMiniView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING) { return nullptr; }
	//virtual wxRibbonPage* NewMainPage(wxRibbonBar* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING) { return nullptr; }
	virtual MyMainView* NewMainView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING) { return nullptr; }

	virtual void OnTimer() {}
};
